Wind und Wunden
===============

\Beitext{Salın}

Salın trug seit vier Tagen das gleiche Joggingkleid und stank
bestialisch. Sie hatte es stets gerade so auf die Toilette
geschafft, oder die paar Schritte zum Lebensmitteldrucker und
zurück zum Bettsofa, um sich grundzuversorgen. Mehr war nicht
drin gewesen. Das war manchmal so, und sie konnte damit leben,
auch wenn schön etwas anderes war, aber da war nun diese
neue Nachricht auf ihrem Taschenrechner: 'Ich könnte heute Abend
oder morgen vorbeikommen, ich bin in der Nähe von
Niederwiesenbrück. Möchtest du Besuch von mir? -- Jurin.'

Salın besuchte eher andere und ließ nur eine sehr beschränkte Auswahl
an Leuten in ihre Wohnung, und wenn, dann mit mindestens einer Woche
Vorlaufzeit. Wenn sie in einer Phase wie jetzt war, dann sagte sie
üblicherweise alles ab. Aber sie wollte Jurin nicht absagen. Sie
wollte einfach nicht.

Also lag sie hier nun seit einer Stunde in diesem unglücklichen
Mindset herum, das aus der Widersprüchlichkeit bestand, sich
nicht zu bewegen, aber wirklich aufhören zu müssen, hier zu
vergammeln, wenn sie wollte, dass Jurin käme. Sie brach die Aufgabe runter und weiter
runter, bis sie sich als allerersten Schritt mit dem einen Fuß vom anderen die Socke
abstreifte. Und dann mit dem anderen vom einen Fuß die andere
Socke. Das war genug getan für die nächste halbe Stunde. Oder?

Als die Decke nicht mehr auf ihrem Körper lag, um sie zu wärmen, hatte sie
das Gefühl, dass nicht sie es gewesen war, die sie durch den Raum geworfen
hatte. Sie hätte sich das nicht antun können. Geschweige denn die
Energie gehabt, eine Decke zu werfen. Etwas verwundert, dass sie nun dahinten
lag, zwang sich Salın vom Bettsofa und legte sich sofort wieder
auf dem Fußboden ab. Es war vollkommen illusorisch, heute Abend in der
Lage zu einem Treffen zu sein.

Aber sie wollte!

Sie hatte es immerhin bis auf den Fußboden geschafft. Also streckte
sie ihren Arm nach dem Taschenrechner aus, den sie auf dem Sofatisch
ertastete, und schrieb Jurin eine Uhrzeit für heute Abend. Weise, wie sie
inzwischen war, eine Uhrzeit, die gerade so schon als Abend gelten konnte. Sonst
würde sie denken, sie könnte sich ja schonmal fertig machen und hätte dann
noch eine Stunde Zeit, während Fakt war, dass sie die Stunde jetzt
möglichst nicht zu bequem und sich ärgernd auf dem Fußboden verbingen
würde, wenn sie sie einplante.

Mit dem Abschicken der Nachricht fühlte sie eine enorme Überforderung, gepaart
mit noch nicht existenter Freude. Irgendwie würde
ihr Körper es doch fertig bringen können, mit dieser Verabredung im Nacken rechtzeitig
gewaschen und bekleidet zu sein.

Sie zog sich Joggingkleid und Unterwäsche aus und ließ alles einfach auf dem Boden
liegen. Während sie sich ins Bad schleppte, sah sie ihren kleinen Hausroboter Lore heranrollen,
der die Kleidung aufpflücken und in die Waschanlage fahren würde. Lore war auch dafür
verantwortlich, dass der Haushalt in Phasen wie diesen halbwegs aufgeräumt blieb --
zumindest, wenn Salın einfiel, wo die Sachen hinsollten, die noch keinen festen
Platz hatten --, und sauber. Es war ein leiser und unauffälliger Roboter. Salın
hatte Lore sehr gern, vielleicht sogar genauso lieb wie die Pferdis.

Salın hatte eine von diesen moderneren Duscheinrichtungen, die ihr einige Behinderte
in ihrem Umfeld empfohlen hatten: Sie konnte darin halb sitzen und musste lediglich
einen Knopf drücken, um direkt mit Seifenwasser besprüht zu werden. Nur für ihr Haar
hatte sie sich nicht um den Massuraufsatz gekümmert. Er wäre vielleicht praktisch,
aber wenn Salın in so einer Phase wie dieser war, hasste sie jegliche Berührung, die
sich auch nur entfernt anfühlte wie fremde Finger, die sie pflegten. Sie
versuchte, sich gerade nicht daran zu erinnern, dass ihre Ex sich eine ganze
Weile um sie gekümmert und dabei die Abhängigkeit so sehr genossen hatte.

Stattdessen massierte Salın sich selbst mit geschlossenen Augen das Seifenwasser
in ihr kurzes Haar. Richtig, Jurin würde sie ja dann ohne intensiv hergerichtete
Frisur sehen. Nun, Jurin würde es aushalten.

Als sie das Bad wieder verließ, war sie vollkommen erschöpft, aber irgendwie
schon sehr erleichtert, sauber zu sein. Sie wählte sich eine Bluse aus,
deren Spitzenbesatz ihr gerade nicht übertrieben vorkam, und einen orangenen,
knielangen Rock, der ein umsticktes Lochmuster hatte, durch das die etwas dunklere
Lage darunter sichtbar war. Sie entschied sich für Rockträger in grau, passend
zu ihrer Hautfarbe, und zu ein paar simplen orangenen Spangen mit Schmetterlingen,
zwei rechts und eine links. Sie war erstaunt, dass ihr Haar beim Trocknen doch von ihren
Ohren hin abfallend einige niedliche Locken aufwies.

Als sie wieder zu ihrem Taschenrechner zurückkehrte, hoffte sie, eine
Nachricht von Jurin vorzufinden, dass sie ihre vorgeschlagene Uhrzeit nicht
schaffen würde, sodass sich Salın noch eine Stunde oder so an ihren sauberen Zustand
gewöhnen oder sich Themen ausdenken hätte können, mit denen sie ein Gespräch
eröffnen könnte. Aber Jurin hatte ihr stattdessen nur ein Wort geschrieben: 'Passt!'

Es klingelte. Salın hatte nicht einmal Zeit gehabt, in Ruhe nervös zu werden. Stattdessen
rauschte das Gefühl in ihren darauf nicht unvorbereiteten Körper. Sie stieß sich
die Zehen am Schrank, als sie zur Tür eilte und den Hörer für die Gegensprechanlage
in die Hand nahm. "Vierter Stock."

"Bis gleich!"

Salın hätte nicht damit gerechnet, dass in ihr drin beim Klang dieser
Stimme alles flattern würde. Sie öffnete die Wohnungstür und wartete. Sie
hätte bei Jurins Kondition vermuten müssen, dass Jurin zwei, vielleicht
sogar drei Stufen auf einmal nahm und um so viel schneller vor ihr stand
als jeglicher Besuch sonst.

"Moin!", sagte Jurin in ihrem breiten nordwestmaerdhischen Akzent.

Salın registrierte belustigt, dass es nur dieses eine Wort brauchte, um
den Akzent sicher rauszuhören. "Moin!" Ihr Gruß kam viel zaghafter
aus ihr heraus. Nun ja, sie hatte auch seit mindestens drei Tagen ihre
Stimmbänder höchstens für kurze Selbstgespräche genutzt.

"Vielleicht ist das unhöflich von mir, aber ich muss ziemlich dringend
aufs Klo. Darf ich dein Bad nutzen?", bat Jurin.

Salın kicherte und zeigte ihr das Bad. Es war noch nicht einmal völlig
entwasserdampft.

Nervös blieb Salın im Flur zurück. Sie hatte zwei Zimmer, und hätte sich
in eines begeben, aber Jurin kannte sich doch noch nicht aus und
Salın wusste auch nicht, ob sie eher auf ein gemütliches Sofa wollten oder
lieber in das Zimmer mit der Küche darin zwecks Versorgung. Zum Glück musste
sie sich nicht lange awkward fühlen. Jurin war schnell.

Und dann blieb diese unbeschreibliche Person einfach im Flur stehen und
war mit einem Mal die Ruhe selbst.

Jurins Blick wanderte über die Wände und den Schrank, an denen selbst
entwickelte Fotografien mit Stecknadeln oder Haftstreifen angeheftet
waren. Sie stützte sich mit einer Hand an der Wand ab, als sie, den
Blick auf eines der Bilder geheftet, auf die Salın besonders stolz
war, sich ihrer Schuhe entledigte. Sie trug eine funktionierende Kreuzung
aus Sportschuh, Sandale und Kurzstiefel in Schwarz mit etwas Glitzer.

Salın war hin und weg. "Schön, dass du da bist", brachte sie hervor.

Jurin nickte abgelenkt. "Hast du da deine Kamera im Fenster gespiegelt
selbst fotografiert?"

"Ja." Es war komplizierter als das, aber Salın hatte Bedenken,
dass, wenn sie mit Erklären anfinge, sie Jurin komplett zunerden würde. Auf
der anderen Seite: Es war Jurin und Jurin war bekannt dafür, selbst Nerd
zu sein.

"Aber da sind Sterne durchs Fenster sichtbar, die dabei ein gutes Stück gewandert
sind." Jurin fuhr, ohne das Foto anzufassen (Salın war dankbar darum), die kurzen
Bögen nach. "Also eine Langzeitaufnahme."

Salın schnaubte.

"Was? Erzähle ich Unfug?"

"Nein!" Salın näherte sich Jurin einen Schritt. "Ich hatte überlegt, dir
zu erklären, wie das Foto entstanden ist, aber hatte Angst, ich könnte zu
nerdig für dich werden. Aber nun findest du alles selbst raus!"

"Du, zu nerdig, für mich?" Jurin blickte sie so ungläubig an, wie
Salın es erwartet hätte, -- und sah sie damit überhaupt das erste Mal richtig
an. "Es muss sehr dunkel gewesen sein, als du das Bild aufgenommen hast."

"Nacht. Das Licht war aus", bestätigte Salın.

"Selbstauslöser?", fragte Jurin.

Salın schüttelte den Kopf. "Wollte ich eigentlich, hätte Sinn ergeben. Aber
ich hab's vergessen. Die Belichtungszeit war lang genug, dass die ersten
Momente keine Rolle spielen."

Jurin grinste. Und fragte dann in einem völlig anderen Ton als zuvor: "Wie
geht es dir?" Und als Salın nicht gleich antwortete: "Wollen wir die Frage
mit einem Tee angehen?"

"Muss ich überhaupt?", fragte Salın.

"Nein, natürlich nicht!" Jurins hob für einen Moment die Braue. "Ich
vergesse so oft, dass zu fragen bei vielen so einen Druck auslöst, antworten
zu müssen."

"Aber Teetrinken klingt gut. Hast du bestimmte Wünsche an Tee, oder
soll ich irgendeinen zusammenmixen?"

"Zusammenmixen?" Jurins Blick wanderte die Fotos entlang und blieb an
einer Jukebox haften. "Retro ist dein Thema, ja? Stellst du per Hand
Teeblattmischung zusammen?"

Salın nickte. "Wenn du nicht Tee aus dem Drucker bevorzugst, würde ich
welchen zusammenstellen. Nicht nur aus Teeblättern, sondern auch
getrockneten Früchten oder Gewürzen, wenn du magst."

"Mag ich", sagte Jurin ohne weiteres Zögern. "Darf ich die Jukebox
benutzen?"

"Ja, aber wenn sie hakt, geh lieb mit ihr um oder ruf mich", erlaubte
Salın.

Während Salın zur Küchenzeile ging, blieb Jurin auf dem Weg zur Jukebox
wohl noch an ein paar Bildern hängen. Salın mochte schon, wenn ihre
Kunst bewundert wurde, aber sie war neidisch auf eine nur in ihrer Vorstellung
existierenden Entität, die dabei war und die Bewunderung beobachten konnte.

Dann aber erklang ein sanfter Walzer von Schundmund durch die
Wohnung, samt dem feinen Knistern von Staub oder feinen Kratzern
auf der Platte, was Salın eine Gänsehaut über den Körper rieseln
ließen. Sie fühlte sich auf einmal zurückversetzt auf ein Konzert,
auf dem sie mit 13 gewesen war. Sie war
davongelaufen, um dieses eine letzte Konzert von Schundmund zu
hören, bevor sich die Band aufgelöst und zur Ruhe gesetzt hatte. Die
Mitglieder waren damals alle schon knapp zweihundert Jahre alt gewesen
und nur noch jährlich aufgetreten.

Sie hatte mit ihren Eltern ein Picknick in der Nähe des Konzertgeländes gemacht. Es
war Salıns Geburtstag gewesen. Sie hatte sich das Picknick mit dem Hintergedanken
gewünscht, vielleicht das Konzert hören zu können, vielleicht nur aus
der Ferne, aber jene Hintergedanken für sich behalten. Sie war heute
fasziniert von sich, dass sie damals sich selbst vorgemacht hatte, dass
sie die Hintergedanken gar nicht gehabt hätte, damit sie sie nicht
verraten müsste.

Sie hatte Marişa in einem Moment, in dem Lona kurz nicht anwesend
gewesen war, gefragt, ob sie einen Spaziergang machen dürfe. Und dann
hatte sie sich aufs Konzertgelände gestohlen und das Konzert gehört.

Salın hatte auf dem Konzert keine Angst gehabt. Sie wusste nicht, warum,
aber die Menge hatte einfach geschunkelt und lieb gewirkt. Die alten,
knarzigen Stimmen, die eigentlich harten Rock gewohnt
waren, hatten einfach zu Klimparre diesen Walzer gesungen, und
Salın hatte vor Freude geweint. Und gleichzeitig gewusst, dass es
der letzte SneakPeak in die Freiheit für ihre restliche Jugend sein
würde.

Sie fühlte sich unwirklich, als Jurin den Raum mit der Küchenzeile
betrat und Salıns Equipment in Augenschein nahm. Hier lagerten ihre
Stative, die Kameras, die sie in ihrem Leben so benutzt hatte,
ein Haufen Kuscheltiere, ein Bücherregal mit Fachliteratur
und wirklich alten Schätzen, und mit sehr dicken Tüchern
abgehängt die Ecke, in der sie Bilder entwickelte.

Normalerweise kam Salın damit klar, wenn eine neue Person ihre
Wohnung und all ihr Gedöns bewunderte. Aber nach ihrem Zustand der Handlungsunfähigkeit,
der sie mehrere Tage besuchsuntauglich gemacht hatte, und nun halb
in der Erinnerung versunken, fühlte es sich so an, als würde Jurin
ebenso halb mit in ihrer Vergangenheit sein und die ganze Bedeutung
erfassen können, die es für sie hatte. Salın versuchte vergeblich,
sich daran zu erinnern, dass Jurin keine Gedanken lesen konnte, und
fühlte sich roh und offen vor ihren Stiefeln ausgebreitet.

Dabei war Real-Jurin barfüßig.

Salın hängte das befüllte Teesieb in eine Kanne und ließ heißes
Wasser hinein. Jurin trat mit eben jenen baren Füßen an sie heran
und betrachtete sie. Salıns Blick klebte an jenen Füßen, wie
sie dort so vulnerabel auf ihrem Fußboden standen. Salın hätte
bloß mit dem Tee darüber plempern müssen, dann hätte sie Jurin
verbrannt.

Schnell verdrängte sie den Gedanken und blickte Jurin ins Gesicht. Ohne
Schuhe waren sie etwa gleich groß. Sie streckte die Hand aus, die
verwirrt war, ob sie noch kalt, oder von der Teekanne bereits ein wenig
warm sein sollte, und berührte Jurin an der Wange. Sie wollte
sich ihr nähern, um sie zu küssen, aber Jurin griff nach ihrer Hand und
streifte sie sanft von sich.

"Ich mag gerade nicht", sagte sie. "Später vielleicht."

Salın nickte einfach und versuchte, nicht enttäuscht auszusehen.

Sie war nicht enttäuscht. Sie hatte Angst. Sie wusste nicht wovor, aber
durch die viele Therapie, mit der sie ihre Gefühle zu benennen
gelernt hatte, konnte sie erkennen, dass eine tiefe Angst sie durchflutete
und fast lähmte. "Wollen wir drüben im Sofazimmer Tee trinken?", fragte
sie vorsichtig.

"Gerne." Jurin lächelte. "Kann ich Tassen tragen oder sowas?"

"Such dir welche aus dem Schrank aus!" Sie deutete auf einen rosa
lackierten Schrank, an dem bisher nur ein Foto haftete. Es zeigte
ihr Geschwister und war das erste Foto, das sie je auf Film geknipst
hatte.

An manchen Tagen merkte Salın gar nicht, was es alles bedeutete, was
hier hing. Aber wenn sie sich vorstellte, was andere denken mochten, die
es betrachteten, flutete sie die Bedeutung so sehr, dass sie sich selbst
dadurch manchmal nicht ganz erfassen konnte.

Das Lied auf der Jukebox war längst verklungen, als sie das Zimmer
wechselten, aber Salın stellte es im Vorbeigehen noch einmal ein. Es
war eine schlechte Idee. Sie wusste nicht, was sie dazu bewegt
hatte. Jurin nahm ihr die Teekanne ab, als Salın irgendwie im Zimmer stehend
nicht so genau wusste, was sie musste, um sich zu sortieren.

"Brauchst du etwas?", fragte Jurin.

Auf einmal stand sie vor ihr. Salın hatte das Gefühl, ihr fehlte ein
kurzer Moment in der Wahrnehmung, denn auch die Teekanne, die gerade
noch in Jurins Händen gewesen war, stand nun auf dem Sofatisch. Oder fehlte
ein längerer Moment? Da war wieder
die Angst. Jurin stand vor ihr, könnte sie auffangen. Oder gehen. Salın
hatte Angst, dass sie ginge. Dass gleich alles vorbei wäre.

Aber Jurin würde nicht gehen. Es war nicht ihre Ex. Es war Jurin, und Jurin
wusste nicht, was alles passiert war. Jurin konnte keine Gedanken lesen, und
sie war zugleich eine geduldige Person. Wenn Salın einen Fehler machte
oder nicht so ganz aufmerksam wäre, würde sich das mit Jurin schon
klären lassen.

Sie bemerkte erst im Nachhinein und als wäre sie in einem anderen Leben, dass
sie den oberen Knopf ihrer Bluse öffnete. War es Hunger in Jurins Blick? Wie
mechanisch trat Salın einen Schitt auf Jurin zu.

Einen Moment gefror die Situation und die Zeit stand gefühlt still. Mit dem
nächsten Einatmen trat Jurin einen Schritt zurück, kehrte auf dem Absatz um, nahm ihre
Sachen und ging.

Die Tür schlug sanft ins Schloss, viel sanfter, als Salın es verdient
hätte. Sie hörte es noch wie aus einem anderen Universum. Dann kippte
sie in dieses zurück und brennende Feuerflammenfluten von Selbsthass
und Selbstabscheu fraßen sich durch ihren ganzen Körper.

Während sich ein Teil von ihr wünschte, dass Jurin hier irgendeine Schuld
trüge, wusste sie doch, eben gerade weil Jurin keine Gedanken lesen konnte,
dass Jurins Gehen völlig zurecht gewesen war. Ihre Erinnerung, gerade
noch ein unfreiwilliges Hirnmodul auseinandergerissen in drei Welten, spülte
ihr nacheinander Jurins Sicht ins Gehirn, erst Jurins Abgrenzung, nicht
zärtlich werden zu wollen, dann Salıns Aufknöpfen der Bluse auf Jurins Frage hin, ob sie
etwas brauche.

Hass auf sich loderte so entsetzlich scharf in ihr, dass sie nicht wusste, wie
sie das aushalten sollte.

Sie versuchte, zu sortieren, was zu tun wäre. Einen Moment schaffte sie es, die
schlimmen Gefühle gegen eine Klarheit einzutauschen, die zwar bar jeder Rücksicht
auf sich selbst war, aber in der sie für sich klärte, was nun von Wichtigkeit war: Brauchte
Jurin akut etwas von ihr für ihre Sicherheit? Eine Versicherung, dass es in
Ordnung war, dass sie gegangen war? Eine Versicherung, dass Salın ihr nicht
hinterherlaufen oder hinterhertelefonieren oder sonst etwas würde? (Würde sie
nicht? Also, nicht pausenlos, natürlich, aber vielleicht wäre eine Entschuldigung,
wenn sie emotional soweit wäre, okay.) Aber was Salın sich auch ausmalte, es
fühlte sich an, als würde es eine hohe Chance haben, Druck auf Jurin aufzubauen,
Salın doch nicht fallen lassen zu dürfen. Sie formulierte ein paar Nachrichten, aber verwarf
alle und ließ sich schließlich aufs Bett fallen, um zu weinen.

Es gab, stellte sie fest, nichts in ihrem Leben, wofür sie sich so sehr schämte
wie für diese Sache jetzt. Nichts, was so grundlegend falsch war, und gleichzeitig
diesmal gegen eine Person, die sie nie in eine Lage gebracht hatte, in der sie keine
ethisch sauber vertretbare Wahl gehabt hatte.

Ein kurzer Moment, ein geöffneter Knopf, und es bedeutete so viel. Je nach
Jurins Vergangenheit konnte sie damit irgendwas Schlimmes getriggert oder
eben 'einfach' (in Gänsefüßchen, einfach war es wahrlich nicht) eine
kommunizierte Grenze mit einem riesigen Schritt eingerissen haben.

Sie brauchte eine Weile, bis sie sich erlaubte, zu denken, dass sie
damit auch für sich eine der besten Freundschaften ruiniert hatte, die
ihr das Leben zuspielen würde. Es war ein egoistischer Gedanke und sollte
eigentlich eine Nebenrolle für sie spielen. Aber sie gestand sich langsam
ein, dass es mehr als das war. Sie wollte diese Freundschaft. Sie brauchte
so eine. Wobei niemand das Anrecht an ihrendwen hatte, Freundschaft zur
Verfügung zu stellen. Jurin bot ihr etwas Wichtiges wie Umarmungen, nur
in psychischer, kaum greifbarer Form, etwas, was Salın das Gefühl gab,
sich ohne Rücksicht auf die Gefühle von grenzverletzendenen Leuten wehren
zu dürfen. Genau das, was Jurin jetzt mit ihr getan hatte. Es war
gut, dass sie das getan hatte. Aber es war mies, so entsetzlich schrecklich,
dass sie das hatte tun müssen, und das Salın eine Person war, bei der
das notwendig gewesen war.

Sie brauchte ganze zwei Tage, um sich emotional zu sortieren, die sich wie
Wochen anfühlten, und kam dabei zum Schluss, dass es auf der einen Seite
Jurins Recht wäre, sich von ihr vollständig abzugrenzen, aber auf der
anderen Seite sie sich auch ehrlich vorstellen konnte, dass ein klärendes
Gespräch ihnen beiden mindestens Frieden bringen könnte. Also traute sie
sich, die Nachricht, die schon seit einem halben Tag vorgeschrieben in einem
Editor lungerte und die sie immer wieder leicht überarbeitet hatte, abzuschicken:

'An Jurin:'

Salın überlegte, retro wie sie war, nicht diese heute übliche Botschaftseröffnung
zu nutzen, sondern eine von damals wie 'Liebe Jurin', aber sie mochte nicht, dass
'Liebe' dem Namen ein Genus zuschrieb, und jeglicher andere Anredevorsatz hatte, nun
ja, keine Liebe. 'An' hatte immer noch etwas mehr Hingabe als 'Guten Tag' oder
ähnliches. Vielleicht war 'An' auch ganz gut, weil es weniger Potenzial hatte,
zu vermitteln, dass Salın doch schon sehr an Jurin hing.

'Was ich getan habe, war sehr schlimm und ich könnte absolut verstehen, wenn
dir das für einen Kontaktabbruch ausreicht. Es könnte gut sein, dass ich
in solch einer Situation selbst so entschieden hätte.'

Sie machte einen Absatz.

'Es tut mir leid.'

Und noch einen.

'Wenn du dich damit okay fühlst, und nur dann, fände ich schön, mit dir
ein (gegebenenfalls abschließendes) Gespräch zu führen. Ich kann dir erzählen,
wie es zu meinem Verhalten gekommen ist, oder dir zuhören, was du an mich loswerden
möchtest, -- was auch immer uns ein unter den Umständen möglichst gutes
Closure-Gefühl geben kann. Wenn für dich aber kein Kontakt das Beste ist,
werde ich das einfach akptieren und wünsche dir nur Gutes! Von Salın'

Die Antwort ließ ein paar Stunden auf sich warten, was Salın zwar schlimm lang
vorkam, weil sie solche Angst hatte, aber objektiv betrachtet fand sie es eigentlich
recht zügig.

'Ich komme in elf Tagen spätestens auf Salt an, wo ich vermutlich eine Woche
in der Sandburg (ein größeres Anwesen und meines Wissens gar nicht aus Sand) unterkommen
werde. Wenn dir das in den Kram passt, komm vorbei. -- Jurin'

Salt war eine der vier Atla-Inseln und die Heimat der Atla-Pferde. Piannas und Klavin
waren wohl nie dort gewesen, aber ihre Vorfahren, oder eher Ahnen, weit in die Vergangenheit
zurückreichend. Die Atla-Inseln waren eigentlich nicht so weit von Niederwiesenbrück entfernt, zumindest
verglichen mit der Distanz zu anderen Inseln. Aber Salın war nie dort gewesen. Vielleicht
gerade wegen der Verbindung zu den Pferden, auch wenn sie nicht so genau verstand,
warum.

Elf Tage waren jedenfalls eine ausreichende Zeit, vorher noch einmal nach Minzter zu
fahren, um sich um die Pferdis zu kümmern, und anschließend mit der Flexibilität,
ein paar Tage dort zu bleiben, den Atla-Inseln einen Besuch abzustatten. Salın war
froh, dass sie inzwischen wieder in einer energetischeren Phase gelandet war.

---

Die Sandburg war tatsächlich nicht aus Sand. Aber es war eine alte Burg, die einfach in
Benutzung geblieben, immer weiter gepflegt und genutzt und inzwischen zu einer
Ferienstätte hergerichtet worden war, und sie stand auf einer Wiese, die ohne klare
Grenze in einen riesigen Sandstrand überging.

Jurin erwartete Salın auf einer Mauer
sitzend. Sie hatte bis eben gelesen und mit den Beinen gebaumelt, aber stand auf,
als sie Salın erblickte. "Ich würde gern mit dir auf diesen Hügel dort gehen. Da
war die letzten Tage über niemand. Die Leute sind eher am Strand oder weiter oben. Aber die Aussicht
ist trotzdem schön."

Salın nickte einfach.

Sie wanderten still nebeneinander her und es war fast ein bisschen wie früher, dabei
hatten sie noch gar nicht so viel Früher, auf das sie zurückblicken konnten. Der Weg
war nicht sehr lang. Jurin ließ sich lässig auf eine Bank nieder als sie ankamen, den einen Fuß auf
der Latte knapp unterhalb der Sitzfläche abgestellt, die genau zu diesem Zweck dort
angebracht war: Beine anziehen, ohne Füße auf Sitzflächen abzustellen.

Salın setzte sich fast scheu daneben und blickte über das Meer. Es war ein eigenartiges
Gefühl, sich auf einer Insel zu befinden, um die herum sich das Meer bis zum Horizont
ausbreitete. In ihrem Rücken war die Sicht allerdings von mehr Hügeln verdeckt. Salt war
keine besonders flache Insel.

"Ich bin nicht so der Typ für lange Einleitungen", teilte Jurin mit. "Du wolltest
erzählen, warum. Leg los, wenn du willst."

Salın widerstand dem Drang, sich klein zusammenzufalten, weil sie das merkwürdig
empfunden hätte. Aber so saß sie auch nicht gut. Zu offen. Es würde besser werden, wenn
sie anfinge, also tat sie es. "Erst einmal vorweg: Was ich erzähle, soll keine
Entschuldigung dafür sein, was ich getan habe. Ich kenne nur, dass mir manchmal
bei so etwas hilft, zu wissen, wo es herkommt, damit ich mich weniger schlimm
fühle, weil, naja ..."

Jurin nutzte ihre Nachdenklücke, um sie zu unterbrechen: "Es ist mir klar,
dass das keine Entschuldigung ist. Und darüber hinaus geht's mich vielleicht
nichtmal was an. Aber wenn du schon so frei anbietest, über das Warum zu
reden, werd ich das annehmen. Für mich leitet sich daraus ab, ob ich weiter
mit dir was zu tun haben will oder nicht." Jurin platzierte den Salın abgewandten
Ellbogen auf der Lehne und es wirkte nicht halb so lässig, wie es das in
einer anderen Situation vielleicht getan hätte. "Das klingt vielleicht
fies", ergänzte sie. "Aber wenn du mir jetzt erzähltest, dass du nicht
anders konntest, weil du so verliebt in mich bist, wär die Sache
halt für mich gelaufen."

Salın konnte nicht vermeiden, einen Moment zu lächeln. "Romantic repulsed", flüsterte
sie.

Jurin wandte ihr den Blick zu. Er war nicht so wutverzerrt, wie Salın ihn
sich vorgestellt hätte, fast eher sachlich. "Fängst du bald an? Mache ich Druck, wenn ich
das frage?"

Salın schüttelte den Kopf und versuchte, wieder ernster zu werden. Es gelang ihr
mit einem Schlag, als sie sich die ersten Sätze vorformulierte. "Als ich gerade
von zu Hause ausgezogen war, habe ich quasi direkt eine romantische Beziehung angefangen, die
nicht gut für mich war. Lona fand sie gut und hat sie mir quasi ausgesucht, aber
das war mir damals nicht klar. Wenn du in einem Elternhaus großwirst, in dem bestimmte
Formen von Gewalt normal sind, dann merkst du nicht unbedingt, dass du dieser oder
etwas anderer Gewalt wieder ausgesetzt bist, weil du sie ja für normal hältst, weißt
du?"

Jurin schüttelte den Kopf. Aber ihr Gesichtsausdruck wurde noch weicher. "Ich
habe keine Ahnung, ich habe da in meinem Leben einfach enormes Glück gehabt", sagte
sie. "Aber es klingt für mich sofort logisch, falls das hilft."

Salın atmete erleichtert aus. "Ja, das hilft", flüsterte sie und blickte
aufs Meer hinaus, um weiterzuerzählen. "Es war eine 20/7-9^[Das Equivalent zu 24/7 in einer
Welt, in der Tage 20 Stunden und Wochen 7-9 Tage haben.] BDSM-Beziehung." Sie
merkte, wie sie zu zittern anfing. Sie wusste nicht einmal, ob sichtbar oder
nur innerlich.

"Wolltest du das?", fragte Jurin.

Salın nickte. "Ich wollte."

"Wussten deine Eltern eigentlich davon?", fragte Jurin. "Entschuldige,
ich lenke ab. Es spielt keine Rolle, glaube ich."

"Sie wussten nichts davon." Statt die Beine nun anzuziehen, rieb sich
Salın über den Rock. Es war derselbe, den sie angezogen hatte, als Jurin
sie besucht hatte, fiel ihr auf. "Lona findet die Idee widerlich, sie
könnte sich einer Person unterordnen, oder jemand würde sich ihr unterordnen."

Jurin lachte auf, aber riss sich sofort wieder zusammen. "'Tschuldige. Es
ist bloß, ich halte Lona für kaum fähig, eine Beziehung ohne Machtgefälle
zu führen. Aber es muss ein Unausgesprochenes sein, weil, sonst
kann sie sich ja nicht vormachen, es wäre alles unproblematisch
und normal."

"Ja, das habe ich auch in den letzten Jahren manchmal
gedacht", gab Salın zu. "Damals noch nicht. Aber es war vielleicht
trotzdem gerade deshalb aufregend, das hinter ihrem Rücken auszuleben. Ich
saß manchmal mit ferngesteuertem Vibrator in der Hose am
Mittagstisch meiner Eltern und habe mir nichts anmerken lassen. Und
ich habe gewusst, sie fand meine Ex super, aber wenn sie
das rausgefunden hätte, hätte sie meine
Ex aus dem Haus verbannt und wochenlang mit mir gestritten."

Nach einem kurzen Schweigen, vielleicht, weil es schwierig war,
gut zu reagieren, sagte Jurin: "Interessant."

"Was meinst du?" Salın wagte es, sie anzusehen. Jedesmal, wenn
sie das tat, wurden all ihre Gefühle viel stärker, deshalb tat sie es
nur einen Moment.

"Mein erster Gedanke war: Questionable Consent", gab Jurin zu. "Mein zweiter:
dass ich mir bei deinen Eltern dazu nicht so viele Gedanken machen würde. Und
mein dritter war die Frage, ob das vielleicht gerade der Grund ist. Also, dass
du zum einen vielleicht damals kein Gefühl für Questionable Consent hattest und
zum anderen, dass es dich in eine weniger untergeordnete Position deinen Eltern
gegenüber gebracht hat, die du eben überhaupt erreichen konntest."

Salın brach in leicht verzweifeltes Kichern aus. "Du bist schon ganz
schön erstaunlich! Ich denke, das trifft es sehr gut."

"Das tut mir leid", sagte Jurin.

Salın beschwerte sich nicht darüber. Sie dachte erst, Jurin sollte nicht
leidtun, etwas gut erklären zu können, aber das war vermutlich nichtmal
gemeint. "Jedenfalls war die Vereinbarung mit ihr, -- also mit meiner Ex,
meine ich --, dass ich jederzeit für sich zur Verfügung stehen musste. Wenn
sie mit mir Sex wollte, zum Beispiel. Und wir hatten safe Words, aber ich habe sie
nie benutzt."

Jurin wandte ihr den Blick zu und betrachtete sie mit gerunzelter Stirn.

Mit einem Mal kam Salın der Gedanke, dass sie vielleicht vor den
Inhalten ihrer Geschichte hätte warnen sollen. Das war Material, das
triggern konnte. Aber Jurin hatte gemeint, dass sie keine solchen
Erfahrungen hatte. Aber man konnte trotzdem entsprechende Trigger
haben.

"Wolltest du das?", fragte Jurin, bevor sie etwas formulieren konnte. "Mir
fiel das schwer, zu fragen, weil du schon sagtest, dass du das wolltest. Aber
irgendwie, ich weiß nicht. Ergibt Sinn, dass ich es nochmal frage?"

Salın nickte. "Ergibt es", sagte sie. "Und die Antwort ist immer
noch 'ja, unbedingt!'. Aber das ist gleichzeitig nicht ganz richtig." Sie
seufzte. "Es macht mich immer noch an, nur darüber zu reden, einer Person
einfach jederzeit sexuell zur Verfügung stehen zu müssen."

Sie brach ab, weil dieses Aussprechen das Gefühl umso mehr verstärkte. Sie
warf Jurin einen Blick zu und ein Teil in ihr, den sie niederringen wollte,
wünschte sich, dass Jurin sie begierig ansehen würde. Sie hasste sich
ein wenig dafür, dass der Fetisch so stark war und einfach ansprang, auch
wenn sie nicht wollte. Aber Jurin betrachtete sie weiter mit gerunzelter
Stirn und dieser Ruhe, die sich auf Salın übertrug.

"Und auf einer weniger bewussten Ebene wollte ich das nicht", sagte Salın
und wandte den Blick wieder ab. Dieses Mal beobachtete sie ihre Finger, wie
sie auf den eingestickten Löchern im Rock entlang rieben. "Wenn sie
so ein bisschen säuerlich oder knapp war, oder gestresst, oder wenn schon
sexuelle Aufladung in der Luft lag, habe ich mich ihr, naja, so angenähert
wie dir, als du da warst, damit ich wenigstens Kontrolle über das Wann
zurückbekommen kann. Es war keine bewusste Entscheidung. Ich habe es mir
quasi unterbewusst antrainiert. Besonders dann, wenn ich am liebsten die
darauffolgende Nacht einfach kuschelnd und ohne Erregung für mich
brauchte." Salıns Stimme brach und sie zerfloss einfach in Tränen. Sie
hatte nicht weinen gewollt. Sie hatte sich vorgestellt, gefasst zu bleiben,
sich kontrollieren zu können. Es ging hier eigentlich um Jurin, nicht
um sie.

Sie versuchte, durch kontrollierten Atem wieder ruhiger zu werden, aber Jurin
breitete fragend einen Arm aus und sie fiel von ganz allen gegen
Jurins starken Körper. Sie zitterte erbärmlich, nicht nur gefühlt, sondern sie merkte,
wie er durchs Schluchzen gegen Jurins Umarmung bebte. Und Jurin hielt sie einfach.

Salın presste ihr Gesicht an Jurins Schulter, und weil die Lage so
verkrampft und verbogen war, bettete Jurin sie beide in eine liegende
Position auf die Bank um, als wäre das erlaubt. Salın spürte Jurins weiche
Brust, die kräftigen, festen Arme, den langen Zopf, der an ihrer Schulter
entlangstreichelte. "Ich wollte das nicht", flüsterte sie. "Ich wollte
für dich da sein und nicht, dass du mich auffangen musst. Ich habe nicht
mit dem Ausbruch gerechnet. Ich wusste nicht, dass es nach all den Jahren
auf einmal so schlimm ist."

Jurin drückte sie fester. "Es ist schlimm!", antwortete
sie. "Da kann man auch mal den Zusammenhalt verlieren und weinen
müssen."

"Aber ich habe mit meiner sexuellen Annäherung deine Grenze verletzt!", betonte Salın.

"Ja", gab Jurin zu und seufzte, als würde sie das gerade fast nerven. "Und
es tut dir leid, und ich verzeihe es dir. Es war mies, aber es war einfach
ein ganz anderes, viel kleineres Kaliber von mies, weißt du? Das ist was,
was ich im Zweifel wegstecken kann. Das bei dir ist nichts zum Wegstecken. Und
es ist sehr okay für mich, wenn das jetzt erstmal dran ist."

Und so lagen sie da. Salın heulte, Jurin hielt sie. Manchmal sagte Salın so
etwas wie "Ich schnoddere deine Kleidung voll!" und Jurin sagte irgendetwas
Beruhigendes. Bis ihre Tränen irgendwann versiegten und sie fühlte, wie
der Wind sie trocknete.

"Hast du mit Mauk eigentlich eine 20/7-9 BDSM-Beziehung?", fragte Salın.

Jurin gluckste vorsichtig. "So oft sehen wir uns nicht, dass wesen es
so nennen könnte", erwiderte sie. "Aber ja, wenn wir uns treffen, verfallen
wir irgendwie immer in ein Spiel. Hast du Bedenken, dass es Mauk so gehen
könnte wie dir damals?"

"Eigentlich nicht", überlegte Salın. "Ich habe mich wohl gefragt, warum
ich da eher keine Bedenken habe. Wodurch sich das unterscheidet."

"Am Ende können wir eine ganze Menge überspielen und es gibt keinen
100% Verlass, so sehr sich das viele wünschen. Ich finde wichtiger,
sich das immer präsent zu halten, als der Illusion nachzugehen,
Konsens könnte absolut sichergestellt werden", antwortete Jurin. "Aber
ich denke, ein gutes Indiz bei Mauk ist, dass Mauk in der Vergangenheit
ohne irgendein Zögern oder Entschuldigen klar 'Nein' zu Dingen gesagt
hat, die sie nicht wollte. Und bei dir wusste ich von Anfang an, ich
muss in gewissen Dimensionen mit aufpassen. Du hast gleich in den ersten
Momenten unserer Bekanntschaft gefeiert, dass du eine Grenze gezogen
hast. Und das bedeutet doch, dass du das nicht immer gut hinkriegst,
oder?"

Sie hatte doch gerade aufgehört zu weinen. "Warum hast du schon wieder
recht?", schniefte sie und vergrub ihren Kopf enger an Jurins Schulter.

Jurin zuckte mit eben jenen Schultern, was sich überraschend angenehm
anfühlte. "Erfahrung, würde ich sagen. Also in diesem Fall."

"Ich fand richtig gut, dass du gegangen bist", flüsterte Salın. "Also,
ich habe mich in dem Moment furchtbar gehasst, ja. Weil ich was
gemacht habe, was mir schwerfällt, mir zu verzeihen." Sie versuchte
sich zu sortieren. "Entschuldige, ich wollte gerade das eigentlich
nicht bei dir loswerden, weil es um dich und deine Gefühle dabei gehen sollte."

"Jetzt würde ich aber schon gern wissen, warum du das richtig
gut fandest", sagte Jurin in ihr Haar.

"Es gibt mir die Versicherung, dass ich das auch darf", flüsterte
Salın. "Ich kämpfe immer noch manchmal mit meinem Gewissen, dass ich
die BDSM-Beziehung mit meiner Ex gekappt habe, und ihr aber nicht vermittelt bekommen
habe, was eigentlich das Problem war. Und dann ist sie gegangen und hat mich
geghostet, und hat mir ein halbes Jahr später nochmal geschrieben, dass sie
mir damit zeigen wollte, wie es sich anfühlt, wenn plötzlich was
wegfällt, ohne dass es dafür einen wirklichen Grund gibt."

Jurins Körper verkrampfte sich. Zwischen zusammengebissenen Zähnen
hindurch zischte sie. "Sowas braucht nie einen Grund, ein 'nein, ich
will nicht' reicht! Ob ich sie dafür unangespitzt in den Boden ..." Sie
atmete tief durch. "Ich habe Gewaltfantasien."

Salın verfiel unpassnderweise ins Grinsen. "Du bist sadistisch, oder? Da
solltest du eine gute Auswahl an Fantasien haben."

Jurin schnaubte und schüttelte den Kopf. "Ich habe Spaß an Sadismus, wenn
mein Sub dabei genießt. Das ist notwendig dafür, dass Sadismus für mich
funktioniert." Sie strich Salın eine Strähne aus dem Gesicht. "In
diesem Fall fühle ich mich nur schwer in der Lage, einer Person sowas
durchgehen zu lassen, ohne sie persönlich aufzusuchen und ihr
Leben in ein Inferno zu verwandeln."

Salın blickte auf in Jurins Gesicht. Sie wollte so gern küssen. Aber
konnte sie das jetzt fragen? Ja, konnte sie, überlegte sie. Es
war eine Frage, und Jurin könnte 'nein' sagten. Aber sie könnte auch,
hm, eben dies fragen: "Könntest du immer einfach 'nein' sagen, wenn
ich dich fragte, ob du mich küsst?"

Jurin nickte. "Könnte ich."

"Würdest du es jetzt tun?", flüsterte Salın.

Jurin haderte. Salın überlegte, ob es dann besser wäre, direkt einen
Rückzieher zu machen, aber Jurin hatte doch gerade gesagt, dass sie
könnte. "So albern das ist, magst du direkt fragen?"

Salın atmete tief ein und aus und spürte dabei, wie ihr Bauch sich gegen
Jurins schmiegte. "Küsst du mich jetzt? Magst du?"

"Gleich", sagte Jurin. "Ich war gerade verheddert und fand das als Antwort
auf eine direkte Frage einfacher." Sie strich Salın übers Haar und lächelte. "Ich
würde gern vorher selbst noch loswerden: Ob es nun für dich okay war oder nicht, es tut
mir leid, dass ich dich so unter Druck gesetzt habe, mir deine Vergangenheit
anzuvertrauen. Und ich kann es dir begründen, warum ich das gemacht habe, wenn
du willst, aber ich kann es auch einfach so stehen lassen."

"Doch bitte, wenn du magst?", bat Salın.

"Es verlieben sich sehr oft Personen in mich." Jurin seufzte. "Und es
ist in der Vergangenheit öfter vorgekommen, dass darunter welche waren, die
ich mochte, und die mich bewusst versucht haben, heiß auf sie zu machen, auch
wenn ich eigentlich 'nein' gesagt habe und nicht wollte. Manchmal hat
es funktioniert. Das fühlt sich im Nachhinien mies an und ich möchte
mit keiner Person näher zu tun haben, die so etwas mit mir probiert,
verstehst du?"

Salın nickte sofort und breitete ihre obere Hand auf Jurins Schulter
aus, in dem Versuch, sie zu halten, aber auch nicht zu sehr an sich
zu ziehen. "Ich glaube, das ist gar nicht so sehr zum Wegstecken, wie
du behauptet hast."

"Kann sein", gab Jurin zu. Sie senkte den Blick. "Danke, dass du
verstehst."

Salın hatte keine Vorstellung gehabt, dass dieser immer stabile
Körper mal an Spannkraft verlieren könnte. Sie sortierte sich
so um, dass Jurins Kopf an ihrer Schulter lag. Jurin weinte nicht
und zitterte nicht wie Salın, aber es lag trotzdem so eine
Vulnerabilität in ihrer Haltung, dass Salın wusste, dass Jurin
eben auch nicht nur stark war. "Ich finde sowas ganz schlimm", sagte
sie leise in Jurins Haar. "Da spielen Leute Instinkte und körperliche
Bedürfnisse von dir an, damit du deine Grenzen überschreitest und
es hinterher heißt, aber du wolltest doch."

"Ja, das", flüsterte Jurin. "Wobei ich glaube, dass das mehr
Reflexe als Kalkühle waren."

"Ich finde nicht, dass es das besser macht", hielt Salın fest. "Es
bleibt eklig. Richtig eklig!"

Sie lagen still beieinander. Die Abenddämmerung gab erste Anzeichen,
hereinbrechen zu wollen, aber noch war der Wind warm genug, dass sie nur
einander und keine Decke brauchten. Salın streichelte Jurin sanft über
den Rücken und dachte überhaupt nicht mehr ans Küssen, aber es war
sehr in Ordnung, wie es war.

Irgendwann, da schuhute schon die erste Ule, hob Jurin den Kopf. "Ich
öffne mich anderen selten auf diese Art. Wenn du möchtest, wäre ich
für ein wenig Commitment zu haben, denke ich."

"Wie meinst du das?" Und auf einmal waren all die Schmetterlinge wieder
lebendig. Egal, ganz egal, was Jurin wollte, Salın fühlte sich innerlich
so gestreichelt dadurch, dass diese unbeschreibliche Person sie
auf irgendeine Art in ihrem Leben wollte.

"Freundschaft", antwortete Jurin. Sie grinste etwas verlegen. "Die
Zusage, dass es in Ordnung ist, dass wir uns einander so anvertrauen
wie heute. Wenn du magst." Sie kicherte auf die selbe Art verlegen wie
ihr Grinsen. "Ich habe sowas noch nie abgeklärt. Fühlt sich witzig an."

"Das klingt wunderschön." Salın berührte Jurins Stirn mit
der Nasenspitze. "Aber ich mache keine Zusagen für für immer. Ist das
okay?"

"Klar ist es das!" Jurin lächelte breit. "Sowas könnte ich auch nicht
zusagen. Es geht mir nicht um eine zeitliche Dauer, sondern darum
festzuhalten, was sich im Moment die Offenheit richtig anfühlt."

"Es fühlt sich sehr richtig an", sagte Salın. Sie schloss die
Augen. "Ich hab wirklich starke Verliebtheitsgefühle für dich, und
ich weiß nicht, ob das für dich im Weg ist. Immer noch welche, die
nur da sind, wenn ich dich angucke, wenn ich bei dir bin. Die wären
sonst halt nicht da, und das bleibt auch so. Wie ist das für dich?"

Jurin strich ihr mit der Hand über die Wange. "Ungewohnt, aber
ich komme immer noch drauf klar. Eben weil es Momentgefühle sind."

"Küsst du mich jetzt?", fragte Salın.

"Oder du mich?", fragte Jurin, vielleicht eine Spur schelmisch. "Du
liegst gerade oben!"

Salın kicherte. Normalerweise lag sie nicht gern oben. Aber gerade
war das irgendwie sehr okay. Es war auch kein richtiges Obenliegen. Sie
lagen eigentlich nebeneinander, nur lag ihr Kopf oberhalb von Jurins.

Sie rutschte wieder herab, damit Jurin den Kopf nicht so sehr
in den Nacken legen musste, und küsste Jurin auf den Mund. Nach all der Energie die
sie in Gefühlsausbrüche verbraten hatten, war Salın überrascht, wieviel
Feuer sie übrig hatten. Abwchselnd fühlte sie nichts anderes als Jurins
feste Umarmung, Jurins Streicheln, Jurins Lippen auf ihrem, Jurins
Zunge, die zugleich sanft und drängend ihre Lippen erforschte und ihre eigene Zunge
suchte, und dann im Kontrastprogramm die harte, zu schmale Bank, die
sie sich magisch gegen ein Bett eingetauscht wünschte.

Sie fühlte Jurins Hände auf ihrem Rock, auf ihrer Bluse, auf ihrem Rücken
und ihren inneren Rippenbögen. Sie fühlte sich gewollt und wertvoll. Salın
hielt sich einige Male davon ab, Jurins Hand unter ihren Rock zu sortieren. Das
war nicht die Art, wie sie dieses mal fragen wollte, das war die Art, wie sie
es früher gemacht hatte. Dieses Mal wollte sie es formulieren.

Sie küssten sich nicht mehr und Salın war fast überrascht davon. Sie
hatte aufgehört und Jurin hatte sie gelassen, dabei hatte sie nicht
damit gerechnet, dass sie auffällig genug aufgehört hätte, dass es
registriert hätte werden können. Nun lagen sie da, atemlos, die Gesichter
berührungslos, aber dicht beieinander.

"Willst du deine Hand unter meinen Rock ..." -- das war kein guter
Satzanfang. Wie perplex war sie eigentlich?? -- "... tun?" Ihr wurde
heiß, als sie das fragte.

"Und dann?", raunte Jurin, so dicht vor ihrem Mund.

"Sex?", fragte Salın.

"Haben wir nicht schon Sex?" Jurin unterbrach ihr vollkommenes Verharren, indem
sie lediglich langsam mit einer Hand Salıns Wirbelsäule herabfuhr. "Also, man kann es
vielleicht so und so sehen. Vielleicht ist es auch keiner."

"Für mich ist es noch keiner", flüsterte Salın. "Weil es sich noch safe anfühlt."

Jurins Hand verharrte an Ort und Stelle. Einen Moment hielt damit auch
die Zeit an.

"Es ist mir so rausgerutscht", flüsterte Salın. Sie merkte, wie die Tränen
zurückkommen wollten. Sie wollte das nicht.

"Aber war es so eine Wahrheit, die dir unwillkührlich rausgerutscht ist, oder
hatte es eigentlich eine ganz andere Bedeutung, und du hast dich einfach voll
in den Worten verwählt?", fragte Jurin.

Es war die richtige Frage, fand Salın. "Ich ..." Sie versuchte sich zu
sortieren. "Ich fühle mich oft, als müsste ich sexuell zur Verfügung
stehen, damit ich wertvoll bin." Die Tränen flossen doch wieder aus
ihr heraus. "Damit ich irgendwie ausgleichen kann, was für eine Last
ich bin, damit es sich für andere lohnt mit mir."

Jurin nahm sie einfach wieder in den Arm, wie vorhin. "Salın, ich
mag dich und du bist wertvoll für mich. Und das hat überhaupt nichts mit
einer Option auf Sex zu tun. Wirklich gar nichts!" Sie lehnte ihre Wange
an Salıns Stirn und hielt sie einfach. "Ich find Sex meistens nichtmal
für sich genommen interessant. Ich verwöhne ganz gern Mal eine Person, oder
binde es in ein kinky Spiel ein, aber ich brauche das nicht für mich. Manchmal
denke ich, ich bin übersättigt davon. Entspann dich einfach bei mir, wenn
du kannst, ja?"

Salın nickte. "Es tut mir leid. Dass ich mich schon wieder so zerlege. Du
bist schon irgendwie wütend, oder? Dass ich so bin."

"Ich bin wütend." Jurin sagte es ganz ruhig. "Das irgendwelche Leute auf diesem
Planeten daherkommen und dir das Gefühl rauben, oder dir die Möglichkeit wegnehmen, das
Gefühl zu haben, wertvoll zu sein."

Irgendwo unter ihnen rauschte das Meer. Salın spürte den Drang, irgendwie zu
verschwinden. Ihr wurde die Situation zu viel. Sie wurde sich selbst zu
viel und konnte sich nicht vorstellen, dass es für Jurin nicht auch so
war. Sie hatten doch gerade erst die Freundschaft beschlossen. Es könnte
so schön sein, von Meer umgeben mit diesem Gefühl im Wind zu kuscheln. Aber
der Fluchtreflex ließ nie lange auf sich warten. Deshalb vertraute Salın sich
normalerweise nie an.

"Ich habe oft das Gefühl, Leute beachten mich kaum, solange ich fröhlich herumhopse,
aber dann, wenn ich irgendwie zeige, dass ich verzweifelt bin, fühlen sie sich
verpflichtet, für mich da zu sein", flüsterte Salın.

Jurin seufzte leicht. "Ich glaube, du hast in deinem Leben so viel Mieses
abbekommen, dass es sich schwer anfühlen muss, dich mit all dem, was du bist, einer
Person sozusagen zuzumuten. Aber du kannst dich mir gern zumuten. Ich
habe die Kraft. Ich bin so privilegiert abled, dass es sich oft unfair anfühlt, und ich
würde gern ... ach ich weiß nicht, ich bin auch ein bisschen überfordert damit,
wie ich dir klar machen kann, dass sich das immer noch richtig für mich anfühlt,
hier mit dir. Und gern auch die nächsten zehn Tage."

"Wärest du, wenn ich mich nicht gemeldet hätte, direkt weitergereist?", fragte Salın.

"Ja, aber ich kann gut mit Planänderungen umgehen", versprach Jurin.

Salın atmete tief ein und aus, um sich zu beruhigen und fühlte Jurins
Körperwärme. "Ich wollte auch immer mal reisen. Ich habe mich gefragt, ob ich
Windschwinge sein könnte, und würde mich da gern mal reinfühlen. Ich bin jetzt
im Moment völlig unvorbereitet, aber so generell, könntest du dir vorstellen,
mich mal mitzunehmen?"

Jurin strich ihr durchs Haar und küsste sie auf die Stirn. "Ja, kann
ich", sagte sie. "Lass uns das gern morgen besprechen. Und je nachdem, wieviel
Vorbereitung du brauchst, kannst du auch direkt mitkommen, wenn du willst. Es
geht mit der Fähre rüber und dann durch die Ampen oder an der Küste entlang -- das
habe ich noch nicht entschieden -- nach Geesthaven."
