Wunschkind
==========

\Beitext{Salın}

"Du, ich kann nächste Woche nach der Show nicht bei dir übernachten", sagte
Zwin. "Ich muss bei einem Umzug helfen und nehme den Nachtzug."

Salın rang die Panik nieder, die in ihr aufstieg. Sie wäre fast gestolpert, weil
die Angst drohte, ihren Körper so sehr zu lähmen, dass sie ihr Bein fast nicht
zum Weitergehen vom Boden gepflückt bekommen hätte. Es dauerte nur den Bruchteil eines
Moments an, der sich in ihr wie Stunden anfühlte, oder komplett
zeitlos. Zwin hatte vermutlich nichts bemerkt. Gut so. "Das
verstehe ich! Wer zieht denn um?"

"Mein Bruder."

Sie spazierten nachts durch Minzter. Zwin hatte sie nach
der BDSM-Party, wie verabredet, abgeholt. Der Weg vom Antikuarium zum Ferienhaus ihrer
Eltern führte am Nachtmeer vorbei. Das Meer trug den Namen, weil Leuchtquallen unter
der Wasseroberfläche ihm den Eindruck eines Sternenhimmels aus dunklem Wasser gaben. Salın
liebte den Anblick, -- eigentlich. Aber seit sicher drei Jahren hatte sie es an kaum
einem Tag geschafft, dieses Gefühl zu fühlen, dass sie damit eigentlich
verband. Atem. Eine tiefe Entspannung. Ein Zuhausegefühl. Irgendwo war es vielleicht
in ihr drin, aber sie konnte es nicht erreichen.

Ihre nackten Füße fühlten den nassen Sand. Die kalte Dünung rauschte ab und
zu darüber, und ihr Füße wurden allmählich taub, aber es war ein gutes Taub, ein
Reales. Früher hatten ihre Füße schon in den ersten Frühlingstagen nicht im Wasser
gefroren. Nun war ihr Kälteempfinden ein anderes. Ihr war ständig kalt. Aber
gerade mochte sie die Kälte, die ihre Beine hinaufkroch. Sie mochte, wie der Wind
an ihrem mehrlagigen Kostüm zerrte, in dem sie so zierlich und zerbrechlich wirkte, wie
sie sich oft fühlte. Und darüber schützte sie Zwins Jacke. Jurins Jacke mochte sie
lieber, was ihr ein Grinsen aufs Gesicht zauberte. Aber Zwins weiche, grüne
Jacke tat es auch.

Salın hätte eine eigene Jacke eingepackt haben wollen, aber bereute nicht, es vergessen
zu haben. Es passierte ihr nicht absichtlich, aber erstaunlich oft, dass sie Jacken
daheim vergaß, wenn sie zur Mittagszeit schon das Haus für den ganzen Abend
verließ. Sie mochte fremde Oberteile tragen. Manchmal wünschte sie sich einen Schrank
voll mit Oberteilen und Jacken von Leuten, die sie mochte, um sich auszusuchen, wer
sie in ihrer Abwesenheit wärmte. (Und berührte. (Unaufdringlich.)) Aber dann wiederum tauschte sie
Freundschaften ebenso aus wie Jacken. Es bestand einfach fast keine Beziehung mit ihr
über zwei Jahre hinaus. Nur mit ihrer Schwester brach sie nie.

"Willst du noch am Strand bleiben?", fragte Zwin, als sie den Pfad erreichten, der sich
zwischen den Dünen hindurch hinaufschlängelte bis in eine kleine Siedlung am Rande
Minzters, wo das Ferienhaus am Waldrand stand.

Salın haderte kurz mit sich, aber schüttelte dann den Kopf. "Du?"

"Ich kann auch morgen noch einen Strandtag machen." Zwin lächelte.

Salın versuchte nicht, aus Zwin rauszukitzeln, ob Zwin die Entscheidung nur für sie
traf.

Es war angenehmer, nicht allzu spät zu Hause zu sein, weil Lona, ihre eine
Mutter, wachbleiben würde, bis sie daheim wäre, und sie Salın spüren lassen
würde, wie sehr sie sich sorgte, wenn ihr Kind nachts allein noch so lange
unterwegs war. Ihr 41-jähriges Kind. Sie würde sagen, dass sie froh
wäre, dass Salın immerhin nicht allein draußen
wäre, aber auch zu zweit könne ja so viel passieren. Die Treppen im Dunkeln, sie
könnten stolpern, die geringe, aber doch nicht völlig unexistente Kriminalrate, oder
Salın könnte einen ihre Anfälle kriegen. So viele Sorgen, die Salın ihr ihrer Meinung nach
nehmen könnte, wenn sie aus Rücksicht auf ihre
alte Mutter einfach nicht so spät wegbleiben würde.

Salın atmete tief durch, um nicht in eine negative Gedankenspirale abzudriften. Es
gab so einiges, was sie Lona gern an den Kopf werfen würde, aber nicht konnte. Lona
war leider überhaupt kein Stück kritikfähig, außer gelegentlich bei einigen Dingen, die ihr
nicht so wichtig waren, so alibi-mäßig, um Beispiele rauszuhauen, wenn ihr vorgeworfen
wurde, nicht kritikfähig zu sein. Wenn Salın mit starker Kritik angefangen hätte,
gäbe es am Ende nur die Möglichkeiten, es entweder alles später wieder zurückzunehmen, oder fortlaufende
Schimpf- und Heul-Tiraden über sich ergehen zu lassen, sowie subtilere
Formen von Gewalt, oder zu gehen. Und gehen konnte sie
nicht, weil hier ihre beiden Pferde lebten. Piannas und Klavin waren Atla-Pferde, die
als junge Fohlen, als sie noch so klein wie ausgewachsene Katzen gewesen waren, ihr
Elter verloren hatten. Salın hatte sie als Kind gefunden und ernährt, nicht
wissend, dass sie die Pferde dadurch auf eine andere Lebensweise umgewöhnen würde. Nun lebten sie
hier in ihrem Garten und in dem der Benachbarten, die ihren dafür freundlicherweise
mit dem von Salıns Familie verbunden hatten und sich auch gelegentlich
um die Pferde kümmerten. Piannas und Klavin waren pflegeleicht und kamen auch gut eine Woche
mit ihrer Futterration zurecht, aber brauchten es schon, dass immer wieder
jemand nach ihnen sah, sie striegelte und Nachschub gab.

Lona behauptete, dass sie an den Wohnort gebunden wären und an einem
anderen Ort wohnend für immer unglücklich sein würden. Salın glaubte das eigentlich nicht, aber
hatte doch Angst davor, dass es eintreten könnte, sollte
sie es probieren. Außerdem meinte Lona, dass es hier mit den Leuten von
nebenan eine Pflegegemeinschaft gäbe, die so kaum auf der Welt zu finden
wäre. Leider war sie außerdem offiziell eine Verantwortungsübernahme über die Pferde
eingegangen, als Salın noch klein gewesen war. Der Gemeinderat hatte ein
Auge darauf, dass sich in Fällen wie diesen, in denen Wesen
nicht mehr allein gelassen werden konnten, jemand offiziell kümmerte. Auch
dagegen war es für Salın schwer, sich gegenzustellen.

Salın liebte Piannas und Klavin, die inzwischen längst ausgewachsen waren und
etwa die Größe von Ziegen hatten. Sie hatte zu ihnen eine Bindung wie zu
keiner anderen Person auf dieser Welt, außer vielleicht zu ihrer Schwester. Sie
konnte ihretwegen diesen Ort nicht für immer verlassen. Und irgendwo
in ihr drin fragte sie sich doch, ob sie die Beziehung zu ihren
Eltern auch der Beziehung willen nicht kappen wollte. Sie hatte sie mal
geliebt. Aber sie erinnerte sich nicht mehr an dieses Gefühl.

Marişa, Salıns andere Mutter, öffnete ihnen, als sie klopften. "Lona hat Migräne", teilte
sie mit. "Bitte seid leise."

Zwin legte einen Finger auf die Lippen, als Zwins aufmerksamer Blick Marişas traf, und
nickte. "Wird getan!", flüsterte er. "Gutes Genesen!"

Zwin zog sich leise die sandigen Sandalen aus, und Salın riss sich zusammen, Zwin
nicht zu sagen, Zwin möge sie draußen abklopfen. 'Gehst du bitte kurz raus
und klopfst deine Schuhe draußen ab? Sonst verteilt sich der Sand im
ganzen Haus. Und wer muss das dann wieder wischen?', klang
Lonas Stimme in ihrem Ohr, leise und geduldig, als
wäre ihr das halt noch nicht so klar in ihrem zarten Alter von 41 Jahren. Es
implizierte außerdem, dass Lona das besagte Wischen übernähme, aber das
war fast nie der Fall. Salın machte ihren Dreck fast immer selber wieder weg,
außer ein paar ausnahmige Male, wenn es ihr selbst zu dreckig ging, und
das waren die Male, die Lona immer als Beispiel heranzog und als *immer*
hinstellte.

Salın zog sich die Jacke aus, obwohl ihr ohne arschkalt war. Sie hatte
aber keine Lust, auf ihr sich veränderndes Kälteempfinden angesprochen zu
werden. Es würde hineininterpretiert werden, dass mit ihr irgendwas
nicht stimmte, und wenn sie nicht geschickt antwortete, würde die
Interpretation irgendwann irgendwie darauf hinauslaufen, dass sie
ihre Eltern nicht mehr mögen würde.

Kaum hatte auch Zwin die eigene Jacke aufgehängt, erschien Lona in der Tür zum
Flur. "Willkommen ihr beiden!", rief sie. "Lasst euch begrüßen!" Sie ging
mit herzlich ausgebreiteten Armen auf Zwin zu und zog Zwin kurz an sich. Anschließend
schritt sie auf Salın zu.

Salın machte erst keine Anstalten, irgendwas zu erwidern, aber als Lona
ebenso keine Anstalten machte, Salıns Salzsäulenzustand als Abwehr
zu interpretieren, machte Salın doch einen Schritt auf sie zu, um die
Umarmung zu ertragen. Sie hatte keine Lust, morgen angerufen und gefragt
zu werden, was denn los gewesen wäre. "Mein liebstes Wunschkind^[Es gibt ein
gleichnamiges Musikstück 'Wunschkind' von der Band 'Oomph!'. Es hat
mir in gewisser Hinsicht gut getan, -- ich schreibe nicht ohne
eigene Erfahrungen über das Thema. Erfahrungen sind allerdings
sehr individuell. Bitte seht davon ab, irgendwelche konkreten
Schlüsse michbezüglich zu ziehen. Mir
ist der Kapiteltitel sowie die Bezeichnung hier vielleicht
deshalb eingefallen, weil das Lied für mich mit diesem Thema auf immer
verbunden sein wird, auch wenn es im Musikstück und
in dieser Geschichte um verschiedene
Arten von Missbrauch geht.]! Ich habe dich vermisst", begrüßte Lona auch sie.

Es stach, wie sie 'liebstes' sagte, als gäbe es noch ein anderes. Und
genauso stach immer noch, wie Lona vor fünf Jahren beschlossen hatte, dass
Kenna nicht mehr ihr Kind wäre. Salın atmete langsam ein, als könnte sie
die zwei Tränen damit wieder in ihre Augen zurücksaugen. "Ich muss nießen."

Lona ließ sie los, damit Salın sich umwenden und nießen konnte. Als
Salın in ihrer Kindheit herausgefunden hatte, dass
sie bewusst entscheiden konnte, zu nießen, hatte
sie gedacht, es wäre eine unsinnige Fähigkeit. Wie sehr sie sich geirrt hatte.

Lona zuckte bei dem lauten Geräusch zusammen. "Die Migräne", flüsterte sie
extra leise. "Ich weiß, du kannst da nichts für, und was raus muss, muss raus."

Salın hatte gemischte Gefühle zu dieser Migräne. Sie wollte anderen Leuten, selbst
Lona, ihre Gefühle und Einordnungen nicht aberkennen. Aber wenn Salın Migräne
hatte, konnte -- *konnte* -- sie das Bett nicht verlassen, und Lona warf
ihr dann stets vor, sich anzustellen. Dieser Kontext machte es ihr
schwer, Mitleid zu haben oder auch nur geradeauszufühlen.

Salın folgte den Eltern mit Zwin ins Wohnzimmer, wo sie noch eine 'Kleinigkeit'
zu essen bekamen. Räucherstäbchen brannten, deren Geruch Salın nicht leiden
konnte. Sie überlegte, es zu sagen. Mit Zwin am Tisch hätte es Chancen, dass es
nicht zu Drama führen würde. Deshalb war Zwin dabei: Damit Lona nur in Grenzen
über Salıns Grenzen ginge, in Grenzen, von denen Lona vermutete, dass Zwin es
als 'ist eben Lona, ist doch nicht so schlimm' abtun würde. Es funktionierte. Zwin
wusste von den Nuancen nur, weil Salın manchmal davon erzählte, wenn sie
so sehr brauchte, dass es jemand sah, dass es die Hemmungen zerbrach, darüber
zu reden.

Sie beschloss, die Räucherstäbchen räuchern zu lassen. Zu riskieren, ob
Zwins Schutz reichte oder nicht, lohnte nicht, weil es wirklich schon spät war und es in sicher
spätestens einer halben Stunde ausreichend höflich war, die Abendrunde mit Zwin
zu verlassen.

---

Beim Zähneputzen war sie noch nervös, aber als sie endlich mit Zwin im Doppelbett
lag, atmete sie auf. Sie waren aneinandergekuschelt, weil das Bett nicht
so furchtbar breit war und weil Salın so fror. Letzteres lag sicher auch daran, dass
sie es gerade brauchte, dass das Fenster schräg über dem Bett weit
offen stand. Es war ein fast bett-breites Fenster, das nach oben hin aufkippbar war, und es befand
sich in einer steilen Dachschräge, sodass es zwar auch ein wenig über dem Bett war, Salın von
hier aus aber trotzdem den Wald sehen konnte. Salın liebte diese
Schlafnische so sehr. Auch die würde sie vermissen, sollte sie es schaffen, das
Haus zu verlassen.

"Ich bin dir so dankbar, dass du da bist", flüsterte Salın.

"Gerne doch", hauchte Zwin ihr ins Ohr.

Salın schob besagtes Ohr gegen Zwins Mund und Zwin nahm es zwischen
die Lippen. "Ich bin erregt", flüsterte Salın.

Sie war selten erregt, wenn sie mit Zwin zusammen in einem Bett lag. Sie
hatten schon zweimal Sex gehabt, ja. Das eine Mal war es neu gewesen, da
hatte Salın sich zwar eigentlich denken können, dass es nicht der
Traum eines Sex-Erlebnisses werden würde, weil sie nicht auf Zwin
stand, aber sie hatte es nicht *gewusst*. Andersherum stand Zwin sehr auf sie
und Salın fiel es schwer, zu widerstehen, wenn jemand sie verführen
wollte. Sie fühlte sich dann so wertvoll, so gewollt.

Das zweite Mal war eine ähnliche Situation wie jetzt gewesen: Sie war
durch eine andere Person im Vorfeld in ein erregtes Mindest
übergegangen, in diesem Fall durch Jurin, und anschließend war sie mit Zwin
genau hier gelandet und brauchte Ablenkung. Und außerdem, außerdem
war sie Zwin so fürchterlich dankbar. Sie wollte Zwin auch etwas bieten. Sie
wusste, dass Zwin auch so irgendwas aus der Beziehung zog, aber Salın verstand
das nicht. Es fühlte sich nie genug an.

Zwin fasste ihre Feststellung als Einladung auf, zog sie mit einem
wohligen Brummen mehr in die Arme und leckte an ihrer Ohrspitze. "War
dein Abend schön?"

"Ich habe sehr gut geküsst", flüsterte Salın. Sie atmete flach und
schnell. Sie hatte Zwin die Sache mit ihren Ohren einst
sehr genau erklärt und sie funktionierte. Mitten
in die Erregung kam ihr, weil sie über das Küssen sprach,
die Erinnerung an Jurins Lippen.

"Hattest du heute schon Sex?" In Zwins raunender Stimme war
ein warmes Grinsen zu hören.

"Wenn heißes Küssen bereits Sex ist, dann ja." Salın kicherte. "Sie
hätte gewollt, soweit ich das verstanden habe, aber ich habe mich
getraut 'nein' zu sagen. Und zwar lange, bevor mir *irgendwas*
unangenehm geworden wäre."

"Kannst du das bei mir?", fragte Zwin.

Was für eine beschissene Frage. Was für eine beschissene, fürchterliche
Frage in diesem Moment. Und Salın hasste sich fast dafür, nicht
lügen zu können. "Nein. Nicht immer."

Nicht unerwartet nahm Zwins Kopf von ihrem Ohr Abstand. "Willst
du jetzt überhaupt Sex mit mir haben?"

"Ja", flüsterte Salın. Die Antwort war wahr genug dafür, dass sie
sie rasch aussprechen konnte. 'Zumindest, wenn *du* willst', fügte
sie in Gedanken hinzu. Sie musste nicht fragen, ob Zwin wollte. Sie
wusste es.

Im Gegensatz zu andersherum wusste sie ziemlich genau, was Zwin
wollte. Im Gegensatz zu ihr hatte Zwin nämlich gewisse
Probleme einfach nicht, die Zwin gehemmt hätten, 'nein' zu
sagen. Zwin wusste, was Zwin wollte und was nicht, und sprach beides auch stets
sehr direkt aus.

Zwin wusste auch, dass Salın nicht auf Zwin stand. Salın versuchte, ehrlich
zu sein und Zwin nicht das Bild einer Zukunft oder einer
Liebe oder Beidseitigkeit vorzumachen, die so nie existieren würde. Aber
manchmal wünschte sie sich, wo doch Zwin in so vielen Situationen
ihr einziges Schutzschild zwischen sich und ihren Eltern gewesen
war, mehr für Zwin sein zu können als bloß ein unerfüllter Traum
und eine halbe Freundschaft. Manchmal wollte sie diesen Traum
erfüllen, zumindest für eine Nacht. Und manchmal, so wie heute, würde
es ihr sogar auf gewisse Art gefallen.

Sie *war* erregt. Sie brauchte den Abstand. Es war eine
Win-Win-Situation.

Sie drehte sich zu Zwin um und küsste Zwin auf den Mund. Es war
nicht im Entferntesten so schön wie mit Jurin. Eigentlich gar nicht. Eigentlich
war nur der Nebeneffekt schon, dass Zwin dadurch schneller
atmete und eine Spur Gier in Zwin weckte.

Zwin hatte kein Feuer. Wie auch
immer Zwin ihren Sex mit Leidenschaft verband, Salın spürte keine
solche. Zwin legte ihr keine Hände in den Nacken, presste sie nicht an
sich, machte immer nur eine Sache gleichzeitig. Zwin küsste ihr Ohr
*oder* drang mit einem Finger in sie ein. Sie war nie ausreichend
überfordert, um loszulassen.

Es war trotzdem schön. Ein bisschen frustrierend, aber ablenkend
und schön.

---

Irgendwann schnarchte Zwin leise neben ihr und sie konnte nicht
schlafen. Sie hatte gehofft, der Sex würde müde machen, aber stattdessen
war sie wacher als die meiste Zeit des Tages. Zwin hatte irgendwann das
Fenster geschlossen. Es war auch kalt draußen, sie verstand das, aber
wenigstens ein Spalt hätte ihr geholfen.

Sie stand auf, als sie es nicht mehr aushielt, zunächst, um zu
den Pferden in den Garten zu schleichen und einen Moment mit ihnen
nur für sich zu haben. Sie schliefen friedlich, halb auf der Seite
liegend, die Hufe angewinkelt. Aber dann
entschied sie sich, einen Mantel überzuwerfen
und mit Kamera und Stativ zum Strand runterzugehen.

Es war kalt und still, abgesehen von Meeresrauschen und Wind. Letzerer
wehte um ihre nackten Fußgelenke, machte
sie frösteln, aber oben herum wärmte Marişas Mantel sehr gut. Die Wärme
reichte.

Als sie das Stativ im Sand feststeckte, dachte sie wieder an Jurin. Einen
Moment dachte sie, dass sie nicht an sie denken sollte, weil es doch
ein Zeichen wäre, dass sie mehr von dieser fabulösen Person wollte. Sie
hatte Jurin so klar vor Augen. Die schwarzen Stiefel mit der Schnürung und
den Schnallen und den Plateausohlen, deren Anblick Salın allein schon in
eine kinky Stimmung versetzte. Sie mochte ihre Gegensätzlichkeit, ihr
Engelkostüm und Jurins darken Goth-Stil. Und was sie vor allem mochte,
war Jurins Gelassenheit und dieses Selbstbewusstsein. Es fühlte sich
an, als könnte Jurin niemand etwas wegnehmen. Wenn
Salın an sie dachte, fühlte sie doch dieses rauschende Gefühl von Hitze in
sich aufsteigen, die sie das Frösteln in ihren Beinen kurz vergessen ließ. Aber
es war nichts Tiefes. Es würde wegflattern wie Schmetterlinge,
wenn sie nichts mehr hielte. Vielleicht war auf so eine Art, wie sie Zwins nie
erfüllter aber angeteaserter Traum war, Jurin derzeit ihr
Platzhalter für ihren Traum. Sie würden sich wiedertreffen und herausfinden,
wie lange es frisch, neu und schön war, und dann, wenn es anfangen
würde, sich nach Bindung anzufühlen, würde eine von ihnen zuerst
weglaufen. Es war überraschend schön, dass es hier schon von vornherein abgesprochen
war. Salın lächelte bei diesem Gedanken. Es machte diese Beziehung für
sie sicherer. Jurin würde sie dann nicht halten wollen, wenn
sie ginge. Es würde keine Sehnsucht von Jurins Seite geben, der
sie widerstehen müsste, wenn sie eigentlich nicht mehr wollte. Solche
Sehnsüchte brachten sie sonst so oft dazu, fürs Gefühl, irgendwie
doch genug oder wenigstens für andere brauchbar zu sein, zu bleiben.

Salın justierte die Kamera auf dem Stativ. Sie war sich nicht sicher, ob
die Kamera etwa hundertfünfzig Jahre alt war oder eine gute Immitation einer
solch alten Kamera. Sie war überwiegend mechanisch, außer einem eingebauten Chip, der
Sonnenlicht in Ströme umwandelte und einen Zeiger beim Durchguck ausschlagen
ließ. Er zeigte dann einen Richtwert für die Belichtungszeit
zur eingestellten Blende an. Gerade allerdings war es zu dunkel, als dass
der Zeiger ausschlug. Salın stellte eine Belichtungszeit von vier Minuten ein. Das
war das letzte Mal ganz gut gewesen, aber sie machte erst seit ein paar
Wochen Erfahrungen mit Nachtfotografie.

Sie mochte das Hobby. Egal wie viel
sie im Vorfeld darüber nachlas und lernte, es war doch etwas, wo sie
eigene Erfahrung sammeln musste, um ein Gefühl dafür zu entwickeln, und solange
entwickelte sie Überraschungsbilder. Sie machte auf jedem Film nur höchstens zehn
der knapp vierzig Bilder bei Nacht, weil sich fürs reine Raten ohne Ergebnisse
zum Lernen der Film ansonsten verschwendet anfühlte. Unter ihren letzten
fünf Nachtfotos waren drei, die ganz schwarz waren, und zwei auf denen der
halbe Mond so hell alles überstrahlte, als wäre es die Sonne. Letzteres war nicht
das erzielte Ergebnis, aber es waren durchaus sehr schöne Fotos.

Sie holte ihren Taschenrechner aus der Tasche, in der sie auch Stativ und Kamera
transportiert hatte, und besah sich ihre Notizen der letzten Aufnahmen. Sie
brauchte eine Weile, um sich ungefähr zu erinnern, was sie mit den Zahlen
gemeint hatte, und beschloss, dieses Mal gründlicher zu notieren.

Es gab das Vorurteil, dass, wer die Natur ständig fotografieren wolle, ihr nicht so
nah wäre wie ohne eine Kamera dazwischen. Aber für Salın war
das Gegenteil der Fall. Wenn sie fotografierte, war
sie bei sich und präsent in der Situation und driftete nicht halb in einen
Headspace oder in ihre Sorgen ab. Sie wusste genau, wo sie war, fühlte den
Wind, hörte die Fluten, roch das Salz und das angeschwemmte Seegras, und sah.

Sie fühlte und hörte genau auf das mechanische Geräusch, als sie den Auslöser betätigte und der
Spiegel hochklappte. Sie löste ihre Finger so vorsichtig wie eben möglich von der Kamera,
sodass das Bild nicht verwackeln würde, und schlich dann, ebenso vorsichtig, vom Stativ
weg, -- nur so weit, dass sie über das Rauschen hinweg das Zurückklappen des Spiegels
hören würde.

Dann wartete sie, beobachtete das dunkle Meer, in dem die Leuchtquallen floreszierten. In
Gedanken tauchte sie darin ab, hörte unter den Wogen das kribbelige, hellere Rauschen der Kieselsteine,
die vom Wasser hin- und hergerollt wurden.

---

Sie hatte längst ihre Ausrüstung eingepackt, aber wollte noch nicht gehen. Sie lag
im Mantel auf dem Sandboden und wurde allmählich müde. Aber sie wusste, wenn sie
aufstehen und zurückgehen würde, könnte sie wieder hellwach werden.

Vielleicht sollte sie hier einfach auf dem Sandboden schlafen. Aber ... Drama. Sie
sollte vor Sonnenaufgang wieder im Bett sein und hoffen, dass niemand bemerkt hatte,
wie lange sie weggewesen war.

Morgen Mittag würde es wieder nach Niederwiesenbrück gehen, wo sie ihre eigene
Wohnung hatte. Darauf freute sie sich.

Aber vielleicht sollte sie irgendwann mal im Freien schlafen. Wieder musste sie
an Jurin denken. Dieses Mal wegen des Tattoos: Es war ein Erkennungszeichen der
Windschwingen. Als Windschwinge konnte sich jede Person bezeichnen, die nicht
an einem Ort fest wohnte, sondern immer auf Wanderschaft war. Wobei, reisen
reichte auch. Jurin hatte unzählige Male im Freien geschlafen, das
wusste Salın.

Manche Windschwingen beschlossen erst irgendwann im Erwachsenenalter,
einen festen Wohnsitz aufzugeben. Manche gehörten zu Traditionsgemeinschaften
beziehungsweise, es gab auch Windschwingen-Völker. Zu ihrem
Wandern gehörte unter anderem eine Spiritualität.

Salın setzte sich ihre weichen, schließenden Kopfhörer auf, die leider
so viel mehr Platz einnahmen als irgendsoein moderner Kram, der aber
nicht zu ihren Bedürfnissen passte, und forderte ihre KI auf, ihr das
Interview mit Jurin noch einmal vorzuspielen.

Jurin erzählte darin, dass sie selbst zu einem alten, spirituellen
Windschwingen-Volk gehörte, den Nikaſ, aber selbst nicht besonders spirituell
war. Ihr war ihr Erbe wichtig, was sich auch im Namen ihrer Spiel-Persona
Ærenik widerspiegelte, aber sie pflegte nicht alle Traditionen mitzumachen
oder an alles so zu glauben wie ein Großteil ihrer Familie. Sie erzählte
auch, dass die Familie -- und damit meinte sie einfach alle Windschwingen und
insbesondere die Nikaſ, nicht ihre Eltern und leiblichen Geschwister -- fein
damit war. Soweit Salın das richtig heraushörte, gehörte zur Kultur der
Nikaſ überhaupt, eine geringere Erwartungshaltung zu haben, aber es konnte
auch sein, dass die Erwartungshaltung nur eine andere war, als Salın sie
kannte, und Salın die Erwartungshaltungen dort, wo sie
dann stattdessen wären, mangels Erfahrung nicht sah. Und
überhaupt war so etwas in einem Volk ja immer höchstens eine Tendenz und
nichts, was auf Individuen zwangsläufig zutraf.

Die Nikaſ waren namensgebend für den Begriff Windschwingen, also für die ganze
Bewegung oder Lebensweise, denn Nik war so etwas wie
das Göttan des Windes in Jurins sowie einigen anderen
alten Windschwingenvölkern. Die Nikaſ waren das größte Volk unter den
traditionellen Windschwingen-Völkern, vielleicht nicht immer gewesen, aber
in den letzten dreihundert Jahren schon.

Ær war ein nordisch-niederelbisches Wort für Luft oder Wind, und so ergab sich
der Name Ærenik aus verschiedenen Worten für Wind.

Salın hatte früher oft davon geträumt, Ærenik, beziehungsweise Jurin, einst
kennenzulernen. Nun war es also unerwarteter Weise dazu gekommen. Salın hatte
vermutet, dass so ein Treffen, dass dann nicht mehr in ihren Tagträumen sondern
real passieren würde, sie enttäuschen würde, also, nicht schlimm enttäuschen, sondern
im Wortsinne 'enttäuschen'. Sie hätte damit gerechnet, dass sie mit der
realen Ærenik, beziehungsweise Jurin, nicht viel anfangen können würde oder
Jurin einfach komplett uninteressiert an ihr sein würde. Sie
war fast nun wiederum enttäuscht, dass es anders war. Eigentlich begrub sie
gern Träume, wenn sie Leute real kennenlernte. Sie grinste kurz und
schüttelte über sich selbst den Kopf. Nein, sie war nicht enttäuscht,
nicht enttäuscht worden zu sein. Sie mochte die Aussicht darauf, was
auf sie zukam, und wenn es auch nur von kurzer Dauer wäre.

Salın setzte die Kopfhörer ab. Das Interview war längst beendet. Sie wäre auch
gern Windschwinge, dachte sie. Aber konnte sie das? Es gab so viele
Probleme an der Umsetzung. Tatsächlich war sie noch nie einfach so in den
Tag gereist. Sie hatte zwei Wohnorte: Das Ferienhaus hier und ihre
Wohnung in Niederwiesenbrück. Ersteres konnte sie nicht ganz loslassen wegen
der Pferde, aber es fühlte sich eh nicht mehr wie ein Zuhause an. Und es
gab durchaus Windschwingen, die einen Ort regelmäßig wieder aufsuchten. Er
war eben nur nicht ein Zuhause, wobei vielleicht auf eine andere Art
nicht ein Zuhause, wie dieses Haus kein Zuhause für Salın war, vielleicht.

Die Wohnung in Niederwiesenbrück war Salın nicht wichtig. Aber sie hatte so
viel Kram dort. So viele Dinge, die für sie von emotionaler Bedeutung
waren. Die konnten nicht alle mitreisen. Es war vertrackt, denn eine Wohnung
nur zu haben, um Dinge zu lagern, war nicht im Sinne von Wohnraum, also
fühlte sie sich auch gedrängt dazu, überwiegend dort zu wohnen. Und wenn
sie wegen der Dinge immer wieder zurückkehrte, dann war es eben doch
ein Zuhause.

Sie hatte überlegt, auszuprobieren, alle naselang mal ein
paar Monate umherzureisen. Sie musste ja nicht Windschwinge sein, um sich
Anteile aus dem Lebensstil zu nehmen, die ihr guttaten. Aber zum
einen fühlte es sich irgendwie falsch an. Und zum anderen fehlte ihr
dafür irgendwie der Anschluss oder der Zugang. Sie war nicht
gern allein.

---

Salın stand auf, schüttelte den Sand vom Mantel ab, so ausgiebig und lange, dass
es vielleicht nicht auffallen würde, dass sie ihn entführt hatte, und schleppte
sich zum Haus zurück. Es war nicht einmal das Haus, in dem ihre Eltern eigentlich
wohnten. Aber sie ließen sich nicht nehmen, fast immer herzureisen, wenn Salın hier
nächtigen würde.

Sie war das Wunschkind. Sie war nicht nur das Wunschkind, weil Kenna die
Familie verlassen hatte und Salın geblieben war, nicht nur, weil Lona sie
überall vorzeigen konnte als ihr gelungenes Präsentierexemplar von Kind. (Ihre Worte.) Sondern
auch, weil Salın ein Kind gewesen war, von dem Lona sich früher hatte alles
wünschen können. Egal was.

Salın zitterte, versuchte, den Gedanken so schnell wie möglich
abzubrechen, aber so richtig bekam sie den Weg ins Bett nicht mehr mit.

Zwin wachte auf, als sie sich neben Zwin legte. "Du bist ja eiskalt!"

Und das war sie. Tief, tief ausgekühlt.

Zwin nahm sie in den Arm, als gäbe es da etwas zu trösten. Und vielleicht
gab es das auch und vielleicht hätte Zwin getröstet. Aber Salın konnte sich
nicht anvertrauen. Es war nicht so, dass sie nicht hätte darüber reden
können. Aber jedes Mal, wenn sie es auf nicht oberflächliche Art
versuchte, hasste sie sich. Sie
hasste sich dafür, einer anderen Person diese Last auch nur ansatzweise
zuzumuten, die sie war. Sie hasste sich schonmal vorauseilend, weil sie dadurch
so vulnerabel wurde, einfach falls ihr auf Basis dessen, was sie preisgab,
wehgetan würde -- und es passierte immer früher oder später --, weil sie
sich selbst die Schuld dafür geben würde. Wer war sie auch, sich anzuvertrauen? Was
hatte sie Besseres verdient?

Sie *hatte* Besseres verdient. Sie wusste das. Sie wusste aber leider auch, in welchen
Situationen sie es zuverlässig vergessen würde.

Zwin schlief längst wieder, den Arm um sie gelegt, der so schwer wog, so
falsch war, als sie sich endlich traute, sich in den Schlaf zu weinen. Leise.
