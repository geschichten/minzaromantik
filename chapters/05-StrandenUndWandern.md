Stranden und wandern
====================

\Beitext{Jurin}

Sie gingen weiter am Strand entlang, aber nun langsamer. Jurin fühlte
sich nicht unbedingt wohl. Sie hatte jedes Wort ernst gemeint, aber
es hatte sich auch angefühlt, als hätte sie sich und ihre Hilfsbereitschaft
in den Vordergrund gedrängt, wo eigentlich Salın sein sollte, und auch,
als hätte sie nicht ausreichend Ahnung, worauf sie sich da einließ.

Sie stellte die Frage, vor der sie Angst hatte, -- und es war eine ganz
andere Angst als das Ungeheure an tiefem Wasser: "Was für Folgen hat
mein Verhalten für dich, wenn du deine Eltern wiedertriffst?"

"Nichts Gutes", antwortete Salın. "Möchtest du wissen, ob ich mir
von dir deshalb anderes Verhalten gewünscht hätte?"

Jurin nickte. "Ja, das würde ich gern wissen."

"Nein", sagte Salın in einer überraschend gelassenen Bestimmtheit. "Ich
habe mich dazu entschieden, mit meinen Eltern ein Mindestmaß von 'gutem'
Verhältnis aufrecht zu erhalten." Sie umrandete das Adjektiv mit mit den
Fingern in die Luft gemalten Gänsefüßchen. "Ich möchte keinesfalls
beschließen, dass irgendeine Person, die ich mit zu ihnen nehme, sich für
mich Lona fügt. Lona wird mit Sicherheit ständig über dich herziehen, aber
das werde ich schon aushalten. Ich finde es irgendwie sogar erleichternd."

"Erleichternd?" Jurin runzelte die Stirn.

"Weil ich mir deswegen a) um dich nicht so viele Gedanken machen muss und b) du
gesehen hast", erklärte Salın. "Wie erkläre ich das? Also, ich meine, mein
Herzwesen, das eigentlich auf heute übernachten wollte, sieht die Probleme
alle nicht."

Jurin senkte eine Augenbraue, sodass nur noch die andere erhoben war. "Es
gibt Herzwesen, die dich dort besuchen, die nichts mitkriegen?"

"Ich glaube, es sieht schon alles viel weniger deutlich aus, wenn das
Kennenlernen damit anfängt, dass du halt eine Umarmung über dich ergehen
lässt, weil sie nicht so schlimm ist. Und solche Sachen", überlegte Salın. "Vielleicht
habe ich bisher auch nicht die aufmerksamsten Leute heimgebracht."

"Ich weiß nicht, ob ich belustigt oder schockiert sein soll", murmelte Jurin
und überlegte, dass ihr verzweifeltes Kicherwimmern, das ihr entfuhr,
vielleicht beides war.

"Es tut mir von meiner Seite aus übrigens auch leid, dich da reingezogen
zu haben", hielt Salın fest. "Vielleicht habe ich nicht damit gerechnet,
dass du das so viel abkriegst, gerade weil üblicherweise sonst
keine Person, die ich mit Nachhause nehme, sieht."

"Mach dir keine Gedanken darüber", beschwichtigte Jurin. "Ich wusste schon,
dass ich mich auf was vielleicht nicht so Gutes einlasse."

Sie wichen einer Gruppe Passierender aus. Der Strand wurde hier voller. Sie
erreichten allmählich wieder den Rand des Zentrums Minzters. Jurin überlegte,
ob sie anbieten wollte, umzukehren für mehr Alleinzeit mit Salın, aber eigentlich
sollte sie sich wirklich bald bei Mauk melden. Sie hatte am Abend zuvor noch eine
knappe Nachricht geschrieben, wo sie schlafen würde.

"Wie lange hast du, bis dein Zug fährt?", fragte Jurin.

"Welcher? Der nach Niederwiesenbrück oder der, mit dem du mich auf ein
Date mit hinnehmen willst?" Salın schmunzelte.

Jurin erwiderte das Lächeln. "Ersterer."

Salın blickte auf ihren Taschenrechner. "In etwa viereinhalb Stunden. Fällt
das Date ins Wasser?"

"Das war mein Plan", erwiderte Jurin. "Ich würde Schwebebahn -- das ist ja im
weitesten Sinne Zug -- mit dir auf die andere Seite von Minzter in eine schöne
Badebucht fahren und dort Zeit mit dir und Mauk verbringen, wenn du magst."

"Ein Date zu dritt?", fragte Salın.

Jurin konnte keine Enttäuschtheit ausmachen und zuckte mit den Schultern. "Ich
habe eigentlich eh kein romantisches Konzept von Date."

"Hm", machte Salın.

Jurin beobachtete sie, aber konnte immer noch nicht ausmachen, was für Gefühle
Salın wohl gerade haben mochte. "Enttäuscht es dich?"

"Nein, nein!", widersprach Salın in einem beschwichtigenden Tonfall. Es wirkte,
als wäre ihr das nichtmal in den Sinn gekommen. "Ich möchte gern einen Strandtag mit
dir und Mauk machen. Ich weiß nicht, wann ich meinen letzten Strandtag hatte ..." Sie
grinste Jurin an. "Ich habe mich gefragt, ob ich ein Konzept von einem romantischen
Date habe. Oder was 'romantisch' für mich heißt. Oder was 'romantisch' für irgendwen
heißt. Bist du sowas wie romantic repulsed?"

Jurin verfiel in lautes Lachen und wusste nicht einmal so genau warum. Es war
so präzise getroffen! "Das Label muss ich mir merken, wenn mir das nächste Mal
jemand einen Minzstrauß schenkt!"

Sie grinsten sich einige Momente an und es war irgendwie vielsagend.

"Habe ich Minze von dir bekommen, weil du einen romantischen Moment
zerstören musstest?" Salıns Augen verschwanden fast hinter Lachfältchen.

In Jurin machte sich kurzzeitig die ernsthaftere Stimmung von Linoschkas
Verletztheit und ihrer Scham breit. Sie nickte. "Es sollte nichtmal von
ihrer Seite einer werden, habe ich im Nachhinein vielleicht verstanden,
aber ich bin, wie du sagst, eben romantic repulsed. Die Abwehr war in
dem Moment zu stark in mir, um Verständnis aufzubringen, dass es vielleicht anders
gemein sein könnte."

Salın nickte. "Ich mag Minze sehr. Wirklich. Es ist ein robustes, immer
frisches Gewächs und sehr verzehrungswürdig, wie ich. Aber ich werde dir nie
Minze schenken. Verstanden!" Sie salutierte.

Jurin kicherte über letzteres. Sie ging nicht darauf ein, dass Salın sich
verzehrungswürdig genannt hatte. Und sie stimmte nachdenklich, dass sie
das Versprechen, von ihr nie Minze geschenkt zu bekommen, nicht erfreute. Es
war trotzdem nichts, was sie gewollt hätte, aber das Versprechen des
Gegenteils eben auch nicht.

---

Mauk wartete auf sie an der Haltestelle, von wo aus der Pfad zwischen
harzigen Sommerweiden und Landpappeln hindurch zum Strand
hinabführte, den Mauk und Jurin am Vortag schon genommen hatten. Der Strand lag dicht
am Windschwingenhaus, in dem heute Nacht nur Mauk übernachtet hatte. Sie half
Jurin und Salın, dabei, das Gepäck in einem Schrankhaus neben der
Haltestelle zu verstauen.

"Hast du im Bett geschlafen?", begrüßte Jurin sie.

Mauk grinste ohne eine Spur Schuldbewusstsein im Gesicht. "Du warst
zu weit weg, um mich zu was anderem zu zwingen." Sie trug ein rotes,
schmales Halsband mit je einem stabilen Ring vorn und hinten, an denen das
Stück Stoff befestigt war, das mit ein wenig Interpretationskunst ihr
Kleid genannt werden mochte. Auf ihrer linken Seite bildete es
einen Ärmelausschnitt, der bis zu ihrer Badehose herabreichte. Auf
der anderen war es mit drei Schleifen zusammengebunden, die einfach
nur aufgezupft hätten werden müssen, um Mauk innerhalb nichtmal
einer Minute komplett zu entblößen.

Salın starrte sie wie verzaubert an. "Du bist Mauk?"

Mauk nickte mit einem breiten Grinsen im Gesicht. "Jap. Und
du Salın." Sie brauchte nicht zu fragen, weil sie Salın auf der
Bühne gesehen hatte, fiel Jurin ein.

"Ziehst du dich gern anzüglich an?", fragte Salın.

"Stellst du gern direkte Fragen?", konterte Mauk.

Salın kicherte. "Stört's dich?"

"Nein." Mauks Ton wurde seidenweich und warm. "Und ja, ich ziehe mich
gern auf diese Weise an. Auch wenn ich es eher vorzüglich statt anzüglich nennen würde. Und
ich frage mich, warum das Wort 'anzüglich' für einen Kleidungsstil gebraucht
wird, der weder aus Anzügen besteht noch besonders angezogen ist."

"Weil er anziehend ist?", riet Salın.

Jurin feierte innerlich das Gespräch der zwei, aber fand, sie könnten das
ruhig auf dem Weg zum Strand hinab tun. Also setzte sie sich in Bewegung und
freute sich, dass die beiden einfach folgten.

"Findest du mich also anziehend?", fragte Mauk.

"Ein wenig." Salın betonte es mehr wie eine Frage.

"Nur ein wenig? Wie kann ich deinen Nerv besser treffen?", erkundigte
Mauk sich.

Salın kicherte, aber blieb um eine Antwort verlegen.

Sie erreichten eine schmale Treppe, die einen längeren Bogen
des offiziellen Wegs abkürzte, und Jurin nahm selbige. Dahinter
erblickte sie das Meer, wie es in der Mittagssonne glitzerte und
zum Baden einludt. Hier waren nicht so viele Leute, und so sehr
Jurin Trubel oft nicht so viel ausmachte, so sehr war sie auch froh,
dass sie hier diesen abgelegeneren Strandabschnitt nahmen.

"Du magst auch BDSM, oder?", erkundigte Mauk sich bei Salın.

Jurin kicherte. "Du denkst auch an nichts anderes."

"Manchmal denke ich auch an Kinks, die nicht so sehr in BDSM
fallen", protestierte Mauk. "Und manchmal denke ich sogar ans
Schlafen, wenn ich überraschend eine Nacht allein verbringe."

"Du Luder!" Jurin drehte sich um und versuchte, sich vor ihr aufzubauen,
was nicht die leichteste aller Übungen war, weil sie zwei der schmalen Stufen
tiefer stand und auf diese Art nur fast so weit hochreichte wie Mauk. So
standen sie sich gegenüber und Jurin überlegte, was sie machen wollte.

"Traust du dich nicht?", provozierte Mauk.

Jurin legte ihr einen Finger aufs Kinn und drückte den Fingernagel
sachte hinein. "Als ob, soweit kommt's noch." Jurin drehte sich
auf dem Absatz um und stieg die Treppen weiter hinab.

"Ihr spielt schön", kommentierte Salın überraschend.

"Also stört dich nicht, wenn wir spielen?", versicherte sich Mauk
mit ungewöhnlich ernstem Tonfall.

"Im Gegenteil", sagte Salın. "Ich fände schön, dabei sein und zuschauen
zu dürfen."

Jurin konnte nicht anders, als breit zu grinsen, was niemand der anderen
sah. "Hast du Grenzen, Salın. Gibt es Dinge, die du eher nicht sehen willst?"

"Och, ich denke, die Öffentlichkeit wird ausschließen, dass ihr gewisse
Dinge macht", überlegte Salın.

Mauk kicherte. "Ich bin mir nicht sicher, was Jurin irgendwie doch
hinbekommt, zu tun, ohne, dass es jemand mitbekommt", warnte sie.

"Ach was, ich würde das Risiko eingehen", sagte Salın. "Also, ich würde nicht
nur, ich tu's. Hab ich ein Safe Word?"

"Wir spielen meistens ohne Safe Words, aber 'nein' heißt 'nein' und so
weiter", teilte Jurin mit. "Wenn du sagst, wir sollen aufhören, hören wir
auf. Wenn du auch noch ein Safe Word möchtest, sag an!"

"Nein, das passt so für mich." Ein Lächeln oder eine Fröhlichkeit sprach
aus Salıns Stimme, und Jurin freute sich irgendwie, dass das nach
diesem Morgen ging.

Sie erreichten die Dünwiesen und gingen die letzten Schritte
zum Strand. Er war wirklich überraschend leer. Nur ein weiteres
Grüppchen Orks hatte sich nah an der Wasserlinie rechts
des Strandzugangs niedergelassen. Ein paar Kinder, die wahrscheinlich
dazugehörten, plaschten im Wasser und spielten Seepferdchen oder so etwas. Zumindest
trug eines der Kinder ein Pferdegeschirr und ein anderes schrie "Hüa!". Jurin
hörte sie nur kaum bis hier über die Entfernung des breiten Strandes und gegen den
Wind. Dann würden sie sich wohl links niederlassen und sich auf diese Art nicht in die
Quere kommen. Jurin stampfte durch den Sand voran und beschloss dann doch, noch
um die Strandnase herumzuwandern, weil sie keine Lust hatte, sich zu sehr
beobachtet zu fühlen.

Mauk schnaufte ein wenig, als Jurin anhielt und sich noch einmal kurz umblickte. Sie
stellte fest, dass sie sich gerade mit offenen Haaren nicht wohlfühlte, also holte
sie ihr Zopfband aus ihrer Rocktasche und band das Haar zusammen. Sie war sich durchaus
bewusst, wie sie aussah, und dass sie einen stabilen Stand im Boden hatte. Es war ein Anblick, den
viele dominant und ansprechend lasen, und der sich auch tief in sie hinein so anfühlte. Sie wandte
sich um und erblickte Mauk, die Hände auf die Knie gestützt und rasch atmend, dahinter
Salın. Wieder fiel es Jurin schwer, Salın zu deuten, aber vielleicht musste sie das
auch gar nicht. Salın trug ein Kleid, das dem Kostüm vom Vortag gar nicht so unähnlich
war, fiel Jurin auf, bloß hatte es weniger Bausch.

Sie fokussierte sich wieder auf Mauk. "Stiefel."

"Du hast Stiefel, das ist richtig." Mauk kicherte.

Jurin nahm mit besagten Stiefeln zwei Schritte auf Mauk zu und stand auf diese
Art direkt vor ihr, -- über ihr. Oft liebte Jurin, dass Mauk eine Glatze trug, aber
gerade hätte sie sie gern an den Haaren hochgezogen. Stattdessen griff sie in den
Ring an ihrem Hals.

Mauks Kopf folgte nicht so freiwillig. "Es sind durchaus schöne Stiefel. Aber
vielleicht lässt du mich noch ein wenig meine eigenen nackten Füße angucken?"

"Ich habe echt nicht so gute Laune", sagte Jurin. "Ich habe kein Problem
damit, sie an dir auszulassen."

"Du kriegst also eh, was du willst", meinte Mauk und grinste. "Entweder, weil ich
direkt tue, was du willst. Oder weil du an mir Wut auslassen kannst. Da nehme ich doch lieber
letzteres."

Jurin zückte ein sehr kleines Taschenmesser und ließ zeitgleich Mauks Halsring los.

Die Wirkung war beachtlich, auch wenn sie gar nichts mit dem Messer tat. Mauk
stützte sich nicht wieder auf den Knien ab, sondern ihr Blick haftete darauf. "Was
hast du vor?"

Jurin zuckte mit den Schultern. "Nicht viel?" Sie legte kurz abschätzig den
Kopf schief. "Wobei ... Ich könnte dir damit eine sehr kleine Verletzung
zufügen." Sie berührte Mauk am Kinn, wo sie sie vorhin schon mit dem Nagel
berührt hatte, aber dieses Mal nur mit der Fingerkuppe. "Dort."

"Damit ich das Blut nicht sehe?", mutmaßte Mauk.

Jurin grinste schief. "Das. Und du kämst auch nicht mit der Zunge dran,
um es zu schmecken."

Mauk streckte die Zunge heraus, um mit ihr Jurins Finger zu erreichen, aber
kam nicht dran. "Wie ich dich kenne, wirst du mir die Hände fesseln?"

Jurin nickte. "Für die nächsten vier Stunden. Es sei denn ..." Sie führte
den Satz nicht zu Ende und machte lediglich eine minimale Geste mit dem Kopf
in Richtung ihrer Stiefel.

Sie öffnete sehr langsam das Taschenmesser, aber sie war nicht einmal halb
damit fertig, als Mauk auf die Knie sank und sich an den Schnallen ihrer
Stiefel zu schaffen machte. "Geht doch", sie klappte das Messer wieder ein
und beachtete Mauk nicht weiter. "Salın! Möchtest du eigentlich heute nur
zugucken?"

Salın nickte.

"In Ordnung", sagte Jurin. "Ich werde aus dir aber auch nicht ganz schlau, muss
ich zugeben. Also, nicht, weil ich erwartet hätte, dass du mitspielst. Ich
wüsste aber gern, auf welche Art du dich so eingebunden ins Miteinander
fühlen könntest, wie du es gern wärest. Hast du Bedürfnisse?" Jurin gab Mauk, ohne
hinzusehen, ein beläufiges Zeichen, dass sie sich aufrechter hinknien möge, damit sie sich auf ihrer
Schulter abstützen konnte, während sie ihr den ersten Stiefel auszog.


Salın trat zaghaft einen Schritt auf Jurin zu und atmete tief ein und
aus. "Okay, dann äußere ich jetzt Bedürfnisse, ja?"

Ah, diese Schwierigkeit, dachte Jurin, und lächelte. "Das wäre exzellent."

"Ich hatte mir unter Strandtag vorgestellt, dass wir im Meer baden, Sandburgen
bauen vielleicht, planschen, rumliegen und so etwas. Ich weiß gar nicht genau,
ich habe sowas so lange nicht mehr gemacht", wiederholte sie. "Ich finde jetzt
auch total spannend, euch zuzusehen und, hm, sozusagen zu lernen. Nicht, weil
ich auch so spielen will, sondern weil ich einfach gern herausfinden mag, was Leute
so gut finden."

Jurin nickte. "Mein Plan war, dieses Luder gefügig zu machen, dann aber einen
Strandtag zu verbringen, der sich kaum von unkinky Strandtagen unterscheidet, außer, dass
ich mir gelegentlich mal einfordern kann,
ein Eis gebracht zu bekommen oder so etwas. Passt das?"

Salın nickte freudig mit so raschen, kurzen Bewegungen, dass Jurin es am liebsten
ein Nickerchen genannt hätte, aber das Wort war schon anderweitig vergeben. "Und ..."
-- sie blickte Jurin direkt in die Augen -- "und, wenn du es auch schön fändest,
dann würde ich dich gern heute noch einmal küssen."

Jurin löste den Blickkontakt nicht, als sie das Gewicht auf den nun nackten
Fuß verlagerte, um Mauk das Ausziehen des anderen Stiefels zu ermöglichen, und hob einen
Mundwinkel. "Das kannst du haben."

Salın trat noch einen Schritt heran, sodass ihre Beine Mauk berührten und zwischen
sich und Jurin einquetschten. "Du magst also gern Brat-Spiele?"

Jurin schnaubte, -- über den Witz und auch darüber, dass Salın gemeint hatte, nicht
in das kinky Spiel verwickelt werden zu wollen und die Grenze dafür mal wieder
anders zu empfinden schien als Jurin. "Sehr." Jurin verfiel unterbewusst in
diesen halb raunenden Tonfall. Sie hätte Salın über Mauk hinweg küssen
können, und es hätte ihr durchaus gefallen, aber sie tat es doch nicht. Sie
steckte in einem Mindset, das, hätte sie Salın nun geküsst, sie als
untergeordnet wahrgenommen hätte. "Auch Brett-Spiele. Wenn du magst, würde
ich dich zu einem unkinky Brett-Spiele-Abend einladen."

Mauk hob den Kopf so schwungvoll, dass er gegen Jurins Knie stieß. "Du meinst
in die Linoschka-WG?"

"Dachte ich, ja. Gibt's da ein Problem?", fragte Jurin.

Mauk grinste frech. "Du musst Salın echt mögen."

Jurin zuckte mit den Schultern. "Na und?"

Mauk kicherte und versuchte die Hände zu heben, aber eine davon landete
dabei irgendwie unter Jurins nacktem Fuß. Sie atmete
zischend ein, als Jurin die Hand mit dem Fußballen in den Sand hinein presste. Zwischen
zusammengepresten Zähnen gab sie dennoch von sich: "Ich sag ja gar nichts dazu."

---

Jurin befahl Mauk die Handtücher auszubreiten, während sie sich
entkleidete.

Salın betrachtete sie dabei nachdenklich, aber zog sich
schließlich ebenfalls aus. Aus ihrem Handgepäck grub sie ein
zartgelbes Badekleid aus, aber bevor sie es anzog, drückte sie Jurin
Sonnencreme in die Hand.

"Erotisch oder, hm, was ist das Gegenteil? Disrotisch?", erkundigte sich
Jurin.

"Egrünisch?" Salın kicherte. "Letzteres jedenfalls, wenn du kannst."

"Kein Problem." Jurin füllte die Hände mit Sonnencreme und schmierte
Salın damit den bereitgehaltenen Rücken ein. Sie war fast selbst von
sich überrascht, dass sie nicht einmal Flashmomente von erotischen
Ideen hatte.

"Glaubt Mauk, dass du dich in mich verliebst?", fragte Salın.

Jurin massierte Salın leicht den Rücken, worauf diese wohlig seufzte. "Sowas
in der Art, denke ich. Aber nur aus Frechheit, um mich zu ärgern."

---

Sie schwammen und planschten, jagten sich im Wasser. Als Salın sich auf Jurins
Rücken schwang, konnte Jurin sie kaum abschütteln, und das überraschte sie
fast ein bisschen. Salın war wirklich viel kräftiger, als sie aussah. Und
auf Jurins Rücken stellte Salın ein ausreichend großes Handicap dar, dass
es für Jurin kein Leichtes mehr war, Mauk zu fangen und unterzutauchen.

Jurin überlegte, Salın loszuschütteln, indem sie länger tauchte, als
Salın die Luft anhalten konnte. Das funktionierte schließlich. So standen
sie sich im hüfttiefen Wasser gegenüber und rangen beide nach Atem. Nah
standen sie sich. Salın schloss den Abstand zwischen ihnen, sodass Jurins
nackter Körper gegen ihr Badekleid drückte. Der weite, kurze Rock schwamm
in den Wogen auf, was sehr hübsch aussah.

Jurin legte die Hände an Salıns Hüften. Sie fühlte so gern Körper
durch dünne Schichten nassen Stoffes hindurch. Vielleicht sogar viel lieber
noch als nackte Haut. "Soll ich dich küssen, bevor Mauk uns umwirft?"

Salın nickte auf die selbe aufgeregte Art, wie vorhin, was Jurin immer noch
gern Nickerchen genannt hätte. Sie fuhr mit ihren Händen über den nassen
Stoff, bis sie Salın eng an sich pressen und halten konnte und küsste sie auf
den salzigen Mund. Sie leckte das Meerwasser aus ihren Lippen, merkte,
den Moment, in dem Salın in diesen anderen Aggregatszustand kippte, den
Jurin so gerne auslöste. Sie hielt sie immer noch eng umschlungen, als
Mauk ihnen die Beine wegzog und sie beide im Wasser landeten.

---

Später lagen sie zu dritt auf ihren Handtüchern nebeneinander, -- Mauk
mit dem Kopf zu Jurins und Salıns Füßen, um sie zu massieren. Die Sonne wärmte
ihre Haut, ohne sie zu verbrennen. Jurin war eigentlich gar nicht so der
Typ dafür, am Strand herumzuliegen und zu entspannen, aber gerade war
es so schön, dass sie es am liebsten nicht schon in einer halben Stunde
aufgehört hätte. Vielleicht wurde sie alt. Sie grinste innerlich
über diesen Gedanken.

"Ich würde mich gern zu einem Brett-Spiele-Abend einladen lassen", sagte
Salın. "Und es ist genauso in Ordnung, wenn du dich umentscheidest und
lieber doch nicht willst."

Jurin lächelte. "So sehr ich auch keinen Rückzieher machen möchte, entlastet
das doch. Und vielleicht ist das überhaupt der Grund dafür, dass ich dich
einladen möchte."

---

Jurin hatte eigentlich vorgehabt, nachdem sie erst Salın und dann Mauk in den
Zug gesetzt hatte, selbst ein Stück in den Norden zu fahren und dann vier
Wochen in den Ampen herumzuwandern, um am Ende in Geesthaven zu landen. Aber sie
brauchte dringend den Workout jetzt schon und zudem eine andere Art von Bewegung als das
Wandern in einem Gebirge: Sie suchte spät am Abend den nächstbesten Fahrradladen auf, wo
überraschend doch noch eine Person anwesend war, die ihr beim Auswählen eines
Rads mit Rat zur Seite stand. Jurin brauchte das eigentlich nicht, aber ein zweites
Urteil gab ihr doch mehr Sicherheit. Sie packte ihr Gepäck in Satteltaschen
um, die sie im gleichen Laden gegen ihren Rucksack eintauschte. Sie waren windschnittiger
als die letzten, die sie gehabt hatte, und das merkte sie schon auf den ersten
Kilometern, die sie noch nicht so schnell radeln konnte, weil die Stadtstraßen
sich nicht dafür eigneten.

Sobald die Enge der Stadt hinter ihr lag und der Fahrradweg sich breit an
einem Fluss entlang bis in die Ferne ausstreckte, wo sie beleuchteten Gegenverkehr
sehen würde, Minuten bevor sie reagieren müsste, erhöhte sie das Tempo, bis
sie die Grenzen ihrer Muskelkraft spürte, und raste durch die Nacht. Im Boden
des Radwegs waren blauen Steine eingelassen, die Tageslicht tankten und in der
Nacht leuchteten. Desweiteren bot dieser Boden ein Notfall-EM-Feld, das bei
entsprechender Ausrüstung, die ein Gegenfeld aufbauen könnte, einen Unfall
abfedern könnte. Sie trug entsprechende Unterwäsche mit dem dafür gemachten
feinen Gedräht darin, das sie sich ebenfalls aus dem Laden mitgenommen hatte.

Jurin atmete Fahrtwind, spürte ihn in den zusammengebundenen Haaren und unter
ihr Oberteil greifen. Sie roch die Sommerblüte. Sie spürte die Wärme, die die
Böschung gespeichert hatte und nun ihre nackten Beine entlangstreifte. Und sie
genoss, dass die Nachtkälte hereinfiel.

Salıns Familie, eigentlich Salıns ganze Situation hatte sie hilflos wütend
gemacht. Sie gestand sich ein, dass sie auch deshalb beschlossen hatte,
ab Minzter aufzubrechen, weil dann Niederwiesenbrück noch vor ihrem Ziel
und nicht dahinter liegen würde, sodass sie vielleicht einen Abstecher bei
Salın machen könnte, um zu sehen, wie es ihr ginge. Zudem war sie bisher noch
nie in den Elsterwiesen geradelt. Es war ein weites Flachland mit mittelmaerdhischer
Vegetation. Dazu gehörten Kräuter wie Rosmarin oder Timbarin, allerlei
Obstbäume, Wurzelgemüse und Mehlgurken. Es sollte hier
in der Gegend überhaupt kein Problem sein, sich für eine Mahlzeit aus der
Natur zu bedienen. Aber Jurin hatte es noch nie probiert.

Wenn sie über Niederwiesenbrück fahren würde, dann gehörte dazu eine Fährüberfahrt
über das Atlameer, überlegte Jurin, sonst wäre es ein Umweg. Aber auch das schien
ihr ein gutes Abenteuer.

Noch war sie sich nicht sicher, ob sie überhaupt einen Abstecher nach Niederwiesenbrück
machen wollte. Sie wollte es sich bloß offen halten.

Sie hatte eigentlich wenige Stunden vor Morgengrauen eine Herberge in einem Zehn-Häuser-Dorf
erreichen wollen, um
dort zu schlafen, aber sie merkte vorher, dass sie erschöpft und müde war, und statt
nach einer Trinkpause weiterzuradeln, schlief sie einfach an Ort und Stelle auf
dem weichen Boden neben der Radstrecke. Dafür war sie allerdings zum Sonnenaufgang
wieder wach und radelte weiter, als die Ruhelosigkeit von ihr Besitz ergriff.

Sie liebte das Reisen und die Geschwindigkeit. Sie liebte es, durch eigene Körperkraft
ganze Kulturregionen zu durchqueren. Es kam ihr mit diesen modernen Rädern fast ein
wenig geschummelt vor, und doch war es einfach etwas ganz anderes, als die selben
Strecken mit einem Zug zurückzulegen. Die Aufmerksamkeit war eine andere.

Gegen frühen Nachmittag fühlte sie sich in ihrem Körper endlich wieder Zuhause. Er
war schweißüberströmt, innerlich gut durchgeheizt, aber widersprüchlicherweise
kühl auf der glühenden Haut. Jurin kannte, wenn sie eine Stunde durchschwamm, dass
ihr Gesicht selbst im kalten Wasser wie fiebrig glühte, nur, dass es sich nicht
krank sondern durch und durch lebendig anfühlte.

Sie verweilte eine gute Stunde einfach auf dem Rücken im Gras und blickte in
den blauen Himmel, über den dünne, weiße Wolken entlangwanderten, so langsam,
dass Jurin ihre Bewegung nicht ausmachen konnte. Sie richtete sich wieder auf, als eine
der Wolken ihr ungewollten Schatten spendete. Während sie eine ausgestellte Karte studierte
-- mehr aus einer Nostalgie für Karten als aus Notwendigkeit heraus --, trank sie
ihre Flasche leer und bemerkte, dass das wohl hieß, dass sie entweder
eine gute Stunde ohne Wasser weiterradeln müssen würde oder eine Viertelstunde
zurück, weil der Weg dort den Fluss verlassen hatte und erst dann einen neuen
Fluss erreichen würde.

Sie hatte sich gerade für das Risiko der längeren Strecke entschieden, als
sie jemand beim Namen rief.

"Jurin! Warte!"

Jurin hatte keine Lust, zu schreien, also machte sie deutlich, dass sie vom
Rad wieder abstieg.

Es näherte sich ihr ein schwer beladenes Fahrrad mit Anhänger, das auf diese
Weise eher langsam vorankam, aber sie natürlich doch recht zügig erreicht
hatte. Die Person war ja schließlich schon in Hörweite gewesen.

Jurin betrachtete den Elben. Ähnlich groß wie sie, aber braunes Haar in dichten
Wellen, ein wenig spröde. "Fluse?"

Die Person lachte und stieg vom Rad. "Julipp", korrigierte der Elb. "'Er, sein' und so
weiter. Bin nicht sicher, ob ich noch Fluse genannt werden will."

"Julipp also!" Jurin bestätigte den Namen und unterdrückte, eine versichernde
Frage danach zu stellen, ob er aber doch die gleiche Person war, die sie zuletzt
vor wievielen Jahren gesehen hatte? "Wie lange ist das her?"

"Ich war das letzte Mal, als wir uns gesehen haben, 22, und davor 15, glaube
ich", überlegte Julipp. Er überlegte überraschend wenig lang.

Jurin kicherte. "Und als du 15 warst, wolltest du mich heiraten."

Julipp lachte herzlich. "Ich fand dich halt cool!"

"Ich war 20! Oder?" Jurin versuchte zu rechnen.

"21", korrigierte Julipp.

"Du bist echt gut mit Zahlen!" Jurin kicherte. "Oder du stellst einfach
wirklich überzeugte Behauptungen auf."

"Du warst mein Jugend-Crush, da weiß wesen sowas." Julipp hatte Lachfältchen, die
ähnlich wie bei Salın fast die ganzen Augen dahinter verschwinden ließen, wa aber
sicher auch am Gegenlicht lag.

"Kommst du inzwischen damit klar, dass da mit mir nichts läuft?", erkundigte
Jurin sich vorsichtshalber.

Julipp nickte, zwar mit einem Grinsen im Gesicht, aber doch unverkennbar
ernst. "Ich habe keine Crush-Gefühle mehr für dich. Es war eine gute
Zeit, auch, wenn es vollständig unerwidert von dir war, was bestimmt auch
gut so war, aber die ist vorbei."

"Für mich war es etwas skurril", erwiderte Jurin.

Vielleicht, weil das Thema erschöpft war, suchte Julipp den Moment aus, um
sich aus seinem Gepäck eine Trinkflasche zu entnehmen und zu trinken. Er
hatte auf der anderen Seite der Satteltaschen eine zweite Flasche. Jurin
versuchte, nicht zu auffällig dorthin zu linsen.

"Hättest du Lust, das Fræy-Fest heute Abend mit mir zu begehen?", fragte
Julipp, und dieses Mal lag eine Ernsthaftigkeit in seinem Anliegen, dass
da zuvor noch nicht gewesen war.

Es wäre nicht das erste Mal, dass sie das Fræy-Fest miteinander begehen
würden. Sie waren, als Jurin selbst noch jugendlich gewesen war, häufiger
ein Stück zusammengereist. Vermutlich, als Julipp so um die acht gewesen
war, waren es mehrere aufeinanderfolgende Sommer gewesen. Julipp und Jurin
waren irgendwie wohl sogar blutsverwandt über ein paar Ecken. Jurin wusste
es nicht genau. Aber sie waren in zwei damals eng verbandelten Familien
großgeworden.

Jurin erinnerte sich an damals, als sie an Feuern zusammengesessen hatten. Es
war eine schöne Zeit gewesen, die sie sich manchmal zurückwünschte. Aber
sie schloss so etwas wie Kind- oder Jugendlichsein mit ein, und das war
sie nicht mehr. Und das wiederum war eigentlich auch sehr gut.

"Ja, gern", sagte Jurin. Sie hätte lieber überzeugter oder begeisterter
geklungen. Aber irgendetwas stimmte sie an diesem Plan eben auch
nostalgisch und traurig.

"Ich bin wahrscheinlich viel langsamer als du. Wolltest du heute
noch ein Ziel erreichen?", erkundigte Julipp sich.

Den Gedanken hatte Jurin auch schon gehabt. "Eigentlich schon, aber das
wird dann halt jetzt durch einen neuen Plan ersetzt."

Julipp lachte. "So flexibel wäre ich auch gern im Kopf."

---

Nachdem Jurin doch von Julipps Wasser getrunken hatte, machten sie
einen groben Plan, wo sie heute Nacht ihr Lager aufschlagen würden. Es
war wirklich kein elaborierter Plan: Julipp hatte im Anhänger ein großes
Zelt dabei. Sie könnten einfach irgendwo halten, wo es besonders schön
wäre.

Jurin war froh, dass sie sich bis hierher körperlich schon so ausgelastet
hatte, denn neben Julipp herzuradeln fühlte sich eigentlich kaum nach
Bewegung an. Es war trotzdem schön. Jurin mochte die Sonne auf der Haut und
das sanften Dahinrollen im Freien.

Sie unterhielten sich über Kindheitserinnerungen, über ihre Eltern, über
das Spiel und über die Vereinbarkeit des Lebens als Windschwinge mit
der Teilhabe an sozial sesshafteren Gruppen. Sie kamen zum Schluss,
dass es einen wandelnden Hackspace geben sollte, -- eine Gruppe, die
sich für Technik und Programmieren begeisterte, die aber umherreiste,
damit die Sesshaften mal in das gleiche Problem rennen würden wie
Windschwingen.

Bei Abenddämmerung bauten sie das Zelt auf und richteten davor eine
sichere, kleine Feuerstelle her. Sie hatten auf dem Weg schon ein paar
herabgefallene Zweige eingesammelt, nicht nur fürs Feuer, sondern auch
für das Ritual.

Das Fræy-Fest war das Fest des Zurücklassens und Verbrennens. Dabei
wurden in Holz Sätze eingeritzt, die wesen unangenehm verfolgten. Vielleicht
etwas, was jemand Schlimmes gesagt hatte, oder eine
Diskriminierungserfahrung. Die Gefühle dazu wurden in den Raum gestellt und
mit den anderen, die das Fest begingen, geteilt, und dann zusammen mit
dem Stück Holz verbrannt, um sich davon zu befreien und es zurückzulassen.

Jurin wusste, dass ein Stock der zwei, die sie zu verbrennen gedachte, um
Salıns Mutter gehen würde, und sie fand es ein starkes Stück, dass seit
dem Zeitraum, dass sie das Ritual das letzte Mal begangen hatte, eins
der Ereignisse, das sie verbrennen würde, erst so wenige Tage her
war. Sie hätte es sich dabei leicht machen und Lonas Spruch verbrennen
können, ob sie Salın auch ja nicht entführen würde. Es wäre etwas, was
Julipp einfach verstanden hätte. Aber es war nicht, was Jurin am meisten
erzürnte. Stattdessen ritzte sie 'Mein Haus, meine Regeln' in den
Stock.

In den zweiten, entrindeten Stock ritzte sie eine einzelne Rose. Sie
hatte vor zwei Jahren eine Freundschaft angefangen, die sie einfach als
schön empfunden hatte. Und dann, wie aus dem Nichts, hatte ihr das Herzwesen
eine Rose geschenkt als Zeichen ihrer romantischen Beziehung, -- Jurin
könne ihr da nichts vormachen. Es hatte keine Möglichkeit gegeben, das
in einer Weise zu klären, die für Jurin den Konflikt aufgelöst hätte, worauf Jurin schließlich
den Kontakt abgebrochen hatte. Interessanterweise trauerte sie der
Freundschaft nicht nach. Aber verletzt hatte es doch.

Als sie fertig waren, zündeten sie das Feuer an und sprachen über ihre
Sprüche. Julipp hatte einen, der besagte: 'Du benutzt deine chronische Erschöpfung
als Ausrede, uns nicht so oft sehen zu müssen.'

"Oh, ich kann mir vorstellen, dass das weh tut", murmelte Jurin.

Julipp blickte sie lange an, fast finster.

Jurin atmete tief durch. "War ich das?"

Julipp lachte fast, aber schüttelte den Kopf. "Du hast nur nichts dagegen
gesagt damals. Du warst dabei."

"Es tut mir leid", sagte Jurin ernst. "Ich war damals noch nicht so
sensibel."

Zu Jurins Überraschung lehnte Julipp seinen Kopf auf ihre Schulter und sie
legte den Arm um ihn. "Ich nehme die Entschuldigung an. Es erleichtert nach
all den Jahren so, weißt du?"

Jurin drückte ihn kurz enger an sich. "Ich weiß es nicht", gab sie
zu. "Aber es ist schön." Sie seufzte leise. "Ich habe in den letzten Jahren
eine Menge über meinen verinnerlichten Ableismus gelernt und ihn hoffentlich
zu großen Teilen entlernt, aber manchmal überrollt mich doch glatt die Scham, da
weiß ich auch nicht. Es tut mir wirklich leid."

"Die chronische Erschöpfung kommt bei mir durch eine Neurodivergenz, durch
die ich Reize ganz schlecht filtern kann. Ich mag Soziales, aber es ist mir
gleichzeitig eigentlich immer zu laut. Nicht nur auditiv", erklärte Julipp.

"Oh, da kenne ich inzwischen ein paar, denen es so ähnlich geht", fiel Jurin
ein.

"Ich weiß", murmelte Julipp. "Im Spiel-Kontext, oder? Du machst jetzt Spiel-Orga
und legst darauf wert, dass das Spiel sogar offen für Leute wie mich sein könnte, wenn
ich das richtig mitkriege."

"Ich versuch's", bestätigte Jurin.

Julipp löste sich wieder von ihr und griff nach einem ihrer Stöcke. "Was
hat es mit der Rose auf sich?", erkundigte er sich.

Jurin blickte in die Funken, die dicht vor ihnen in den Abendhimmel stoben, während
sie erzählte.

"Du bist also aro!" Julipp lachte. "Das wird der einzige Grund sein, warum du mich damals
nicht wolltest. Der einzige!"

Jurin kicherte. Ihr war klar, dass er scherzte.

"Als wir uns das letzte Mal getroffen haben, hast du davon erzählt, dass du
viel mit anderen rummachst", erinnerte Julipp sich.

"Willst du was in Frage stellen hier?" Jurin konnte nicht vermeiden, ein bisschen
verärgert zu klingen.

"Nein!", beeilte sich Julipp zu sagen. "Der Hintergrund ist, dass ich gern
verstehen würde. Ich bin sehr sicher romantisch, und interessanterweise
monoamor. Aber mein Liebscht ist sich komplett unsicher, ob as vielleicht
aro ist."

Jurin entspannte sich, streckte den Rücken durch und blickte in den Sternenhimmel. "Ich
sollte mir endlich mal angewöhnen, nicht alles sofort als möglichen Angriff zu
verstehen. Dass es darum geht, dass du selbst eine Einordnung willst, wäre ebenso
naheliegend gewesen. Es ist für manche echt schwierig, sowas über sich rauszufinden."

"Für dich nicht?", fragte Julipp. "Oh, und du musst natürlich gar nicht
drüber reden."

Jurin zuckte mit den Schultern. "Ich hab kein Problem damit." Ein unwillkührliches
Lächeln trat in ihr Gesicht, als sie einen Anfang für das Gespräch fand. "Aktuell
denke ich viel über Salın nach, und es gibt schon ein paar in meinem Umfeld, die
mich deshalb für verknallt halten."

"Und das nervt?", riet Julipp.

"Würde es." Jurin zuckte abermals mit den Schultern. "Aber von Mauk
kann ich das ab." Sie holte tief Luft, um ernster weiterzusprechen. "Ich habe
aber eben auch bestimmt einen Monat sehr intensiv über Fee nachgedacht. Wir haben
zwei Wochen oder so miteinander getanzt, nachdem Fee mich einmal
kräftig zur Schnecke gemacht hat. Zurecht. Ich habe damals mein Weltbild umkrempeln müssen."

Julipp kicherte. "Oh ja, dass berechtigte Kritik samt Messenger lange beschäftigt,
glaube ich."

"Aus ähnlichen Gründen habe ich mal eine Weile Rosa Pride-Away nicht aus
dem Kopf kriegen können", fuhr Jurin fort. "Wie das so ist, wenn jemand
dich öffentlich auseinandernimmt, und du über Jahre die Schultern zuckst
und denkst, lass die Leute reden, und dann feststellst, scheiße, sie
hat Recht."

Julipp brach dieses Mal in lautes Gelächter aus. "Gibt es auch Leute,
die dich nicht loslassen, die dich nicht zusammenfalten?"

Jurin blickte ihm grinsend ins Gesicht und nickte. "Nurek zum Beispiel. Ich
mag, wie ungefiltert sie ist. Ich hatte mich bis dato für recht ungefiltert
gehalten und habe versucht, sie mir eine Weile zum Vorbild zu machen. Sie
ist sanfter dabei."

Julipp nickte langsam. "Was für eine Art Kontakt hattest du zu ihr?"

"Freundschaft?" Jurin sortierte ihren Zopf über die Schulter nach
vorn. "Also, damals kannten wir uns noch kaum, aber wenn du wissen willst, ob
ich mit ihr rumgemacht hätte: Nein. Sie war und ist in einer monoromantischen
Beziehung und findet Rummachen im Allgemeinen eklig. Ich habe nie auch nur
einen Gedanken daran verschwendet. Mein Interesse, rumzumachen, geht erst
mit Konsens wirklich los."

"Und wenn Konsens da ist, machst du mit wem auch immer rum?", erkundigte Julipp
sich.

"Diese Formulierung, rummachen, ey! Ich habe die so ewig nicht mehr
verwendet!" Jurin lachte etwas peinlich berührt und schüttelte den
Kopf. "Ja, also, wenn es verspricht, irgendwie interessant zu
werden, schon. Bei manchen fühlt es sich besser an, dann länger. Bei anderen
fühlt es sich langweiliger an, da mache ich es vielleicht nur einmal. Mit
manchen fühle ich mich einfach nicht so wohl. Zum Beispiel dann,
wenn es für sie nicht ohne Romantik als Ergänzungsmittel geht. Es ist aber auch
oft sehr random und ändert sich zudem schnell."

"Würdest du mich küssen?", fragte Julipp.

"Ja", antwortete Jurin und betonte es eher als Frage. "Jetzt, wo du keinen
Crush mehr hast, schon, denke ich. Ist das eine Bitte?"

"Nein, nein!" Julipp wich lachend ein Stück zurück. "Mehr Neugierde.
Entschuldige!"

Jurin zuckte mit den Schultern. "Kein Ding. Dann hab ich auch kein
Interesse."

"Aber wenn du magst, würde ich heute Abend im Zelt gern kuscheln
gegen die Kälte. Ohne irgendwas Heißes. Also, einfach wie früher." Julipp klang
fast verlegen.

Jurin nickte. "Sowas hatte ich mir auch vorgestellt", sagte
sie weich. "Wie früher eben. Einfach aneinandergekuschelt bis zum
Einschlafen erzählen, was es zu erzählen gibt."

"Jaaa!" Julipp lehnte sich wieder an Jurin an. "Das wäre schön."

Auch wenn Jurin ihn über so viele Jahre nicht gesehen hatte, liebte sie
Julipp, das wusste sie. Julipp war Familie. Ein Gefühl für Familie
hatte Jurin. Aber wann immer jemand ihr von romantischer Bindung
genauer erzählt hatte, war es ihr zu eng gewesen, als könnte sie nicht
atmen. Sie hatte nie das Bedürfnis gehabt, sich auf eine Person für
immer festzulegen. Im Gegenteil, es sträubte sich in ihr alles dagegen.

"Mit Salın ist das tatsächlich ein bisschen ähnlich wie mit Nurek", überlegte
Jurin. "Salın ist ungefiltert und trägt eine ständige lebensfrohe Lebendigkeit mit sich
herum, als müsste sie damit ausgleichen, dass ein Teil von ihr innerlich im Moment
tot sein muss."

"Das klingt dramatisch." Julipp griff nach Jurins Hand und sie schloss
ihre um seine.

"Das ist es." Jurin seufzte und war froh um die Hand. "Salın ist in einer
missbräuchlichen Beziehung."

"Scheiße", flüsterte Julipp ohne Zögern.

Jurin spürte die Wahrheit dieser Worte erst jetzt so richtig, als sie sie
aussprach. "Grauenvoll", flüsterte sie. "Und das dritte, was mich an
Salın nicht loslässt, ist ein Kontrastprogramm: Sie küsst richtig gut. Also,
zu meinen Kussbedürfnissen passend, meine ich."

Julipp lachte und Jurin lachte mit. "Ich kann voll verstehen", sagte
Julipp, "dass
du ständig an sie denken musst. Und dass romantische Gefühle
dafür überhaupt nicht gebraucht werden."

Jurins andere Hand strich über das erste ihrer Stöckchen. "Lass uns
die Vergangenheit verbrennen. Und dann wie früher kuscheln."
