Abbruch
=======

\Beitext{Salın}

"Wem gehören denn die Stiefel?" Lonas Stimme klang überraschend beiläufig
dafür, dass sie bis ins obere Stockwerk hinaufdrang und Salın weckte.

Salın seufzte innerlich, verhielt sich aber ganz ruhig. Der Himmel hatte erst
einen blassen Morgenton angenommen und sie wollte Jurin nicht so früh
wecken. Wahrscheinlich bestand da aber keine Chance, weil Lonas Absicht
das Gegenteil war.

"Das sind aber nicht Zwins?"

"Ich weiß es doch auch nicht", war nun Marişas angestrengt leise
Stimme gerade so zu hören.

Salın stellte sich vor, wie Lona an den Stiefeln roch und sie anfasste. Aber
vielleicht reichte der Schuhgrößenunterschied fürs Erkennen, dass es nicht
Zwins waren, auch aus.

"Wer hat denn hier schon wieder den ganzen Strand reingetragen?" Diesmal
war Lonas Stimme abgewandt, leiser, aber Salın konnte sie trotzdem
klar verstehen.

Jurin strich ihr mit den Fingern über die Wange.

Salın lächelte unweigerlich. "Davon wäre ich lieber aufgewacht", flüsterte
sie.

"Das sind deine Eltern, ja?", fragte Jurin leise.

"Hmhm", bestätigte Salın.

Jurin küsste sie zart auf die Stirn, was Salıns Körper überraschend entflammen
ließ. Sie lagen in Löffelhaltung. Salın spürte Jurins starken Schultergürtel
an ihrem Rücken entlangreiben, als sie sich zu ihr umdrehte, liebte diese
Kreuzung aus Muskeln und Zartheit. Sie mochte Jurins Körper dafür, dass
er beschützendstark und zugleich auf diese Art weich und feinporig
war, die sie so sehr anzog. Fast wie in Trance strich sie mit den
Händen über Jurins Arme, ihr Dekoltee, ihre Schlüsselbeine entlang und schließlich
in den Nacken, wo die Haarfluten über ihre Finger glitten. Sie legte ihre
Lippen auf Jurins und Jurin erwiderte den Kuss mit dem Feuer, in dem sie
gerade gern vergangen wäre.

Unten klapperte das Gerät, mit denen ihre Eltern Frühstück bereiteten
und den Tisch bestückten. Lona
schimpfte über Marişa, sie möge nicht so laut sein, als diese versehentlich
einen Eimer umstieß. Salın konnte sich keinen Moment länger aufs Küssen
konzentrieren und brach es, frustrierend unversengt, ab.

Sie hatte seit Jahren keine längerfristigen Wünsche mehr, die Welt zu verlassen. Sie
wusste, dass das gerade nur der Moment war. Sie wusste, ein halber Tag
in Niederwiesenbrück würde ihr den Lebenswillen zurückgeben. Sie sprach
nie mit jemandem darüber, wie es war, wenn er vorübergehend fehlte.

Jurin streichelte ihr übers Haar. "Was wäre dein Traum-Szenario bezüglich
Verhalten von mir?"

"Was meinst du?" Salın flüsterte fast, und trotzdem kam sie sich lauter
vor als Jurin.

"Ich könnte aus dem Fenster einfach verschwinden", schlug Jurin vor. "Oder
zum Frühstück mit runterkommen? Keine Ahnung, ob ich eingeladen wäre. Oder
ich könnte mit runterkommen, aber mich direkt verabschieden."

"Wenn du aus dem Fenster verschwändest, was passiert dann mit den
Stiefeln?", fragte Salın amüsiert.

Jurin zuckte, wie so oft, mit den Schultern, nicht so doll, nur ein
wenig, wie um zu sagen, 'who cares', und das mochte Salın. Es nahm
dem Ganzen oft unnötige Bedeutung raus. "Du hast sie gefunden? Keine
Ahnung. Wir finden schon eine Ausrede, wenn du die Fenstervariante
willst."

Salın kicherte. "Ich mag nur die Vorstellung, weil ich dann eine
heimliche Verehrerin hätte oder so. Wie in so einer romantischen
Komödie. Moment, in unserem Fall, aromantischer Komödie? Ich
kann nicht gut unterscheiden, ehrlich gesagt." Sie seufzte und
hoffte, in Jurin kein unbehagliches Gefühl ausgelöst zu haben. "Es bleibt
trotzdem die Frage, ob du hinterher sicher wieder an deine Stiefel
kämst."

Jurin zuckte abermals mit den Schultern. "Ich wechsel das Schuhwerk eh
demnächst aus. Ich wollte bald wandern, dazu eignen die sich nicht."

"Da wüsste ich gern genauer, wie du das im Normalfall mit
Weitergeben oder Recyclen machst, aber lieber wann anders", sagte
Salın. "Ich glaube, wir sollten nach unten gehen. Und wenn sie
dich nicht gleich rausschicken, wäre mein Wunsch-Szenario, wenn du
mich schon so fragst, dass du zum Frühstück bleibst und wir danach
gehen, sobald das einigermaßen stressfrei machbar
ist." Sie seufzte. "Aber ich möchte
eigentlich nicht, dass du hier für mich herhalten musst, um
eine schwierige Situation zu überstehen."

"Verstanden." Jurin rollte sich aus dem Bett und stand
auf, als hätte sie nicht nur höchstens vier Stunden
geschlafen.

Sie trug ein hellblaues Nachthemd, das sie
sich von Salın geliehen hatte, und der Anblick dieser
sonst immer schwarz bekleideten Person nun, abgesehen
von den Haaren, in so hell reflektierend im Morgenlicht
zu sehen, war so fremd, dass Salın lachen musste. Die
Haut hatte keinen einheitlichen Farbton, an manchen
Stellen rötlich, überwiegend hellbräunlich und
an manchen Stellen sogar fast bläulich oder
grünlich. Es sah nicht ungesund aus, aber für eine
Person, die ständig draußen war, überraschend blass.

---

Als sie die Treppen hinab nahmen, war Jurins Stil wieder
normal, außer, dass sie keine Stiefel trug.

"Salın, mein liebes Wunschkind, bevor du dich setzt, magst
du noch eben das gute Geschirr aus dem Keller holen, damit
sich unser Besuch auch willkommen fühlen kann?", sprach Lona, noch
bevor Salın den Fuß der Treppe erreicht hatte. Lona roch
förmlich, wo Salın sich aufhielt.

Salın merkte erst verspätet, dass sie bereits "Klar, mache ich!"
gesagt hatte, weil sie in diesen unterwürfigen Hirnstatus
gefallen war, in dem sie möglichst alle von Lonas Wünschen
erfüllte. Sie übte während ihrer Therapie, bei sich zu bleiben,
aber sie hielt nicht zum Zweck des sich Besinnens auf der Kellertreppe inne,
sondern auch weil sie Jurins reden hörte.

"Mit Besuch bin ich gemeint?" Der Dialekt und die Dominanz stach für
Salın in diesem Haus irgendwie besonders hervor.

"Natürlich, wer sonst?" Salın konnte innerlich förmlich
sehen, wie Lona die Arme ausbreitete. "Lass dich begrüßen!"

"Wenn es um mich geht, würd ich auf schniekes Geschirr zum Frühstück lieber
verzichten", sagte Jurin.

Salın gefror.

"Es macht uns wirklich keine Umstände", betonte Lona. "Fühl dich
einfach wie zu Hause und lass dich verwöhnen. Ich bin Lona und das
ist Marişa."

"Macht es denn Umstände, darauf zu verzichten? Denn dann würde
ich das tatsächlich gern. Das war keine Rücksichtnahme meinerseits sondern
ein Wunsch", beharrte Jurin.

Stille.

Dann die zurückhaltende Stimme Marişas an Lona: "Wir können auch
darauf verzichten, oder?"

"Natürlich! Dann halten wir es ganz einfach."

Salın fragte sich, ob Jurin die leichte Pikiertheit
in Lonas Stimme auch hören konnte.

Laut rief Lona: "Kind, kommst
du wieder hoch?" Und an Jurin gerichtet: "Lass dich erstmal
in den Arm nehmen und ordentlich begrüßen!"

"Auch darauf verzichte ich", sagte Jurin. "Mir wäre ein schlichtes
'Moin' lieber. Mir ist gerade nicht so touchy zumute."

Salın kam gerade rechtzeitig, um Lonas Froschmund noch
sehen zu können, sowie Jurins Geste mit einer in Abwehr
hochgehaltener Hand.

"Wenn es dir hier nicht gefällt, kannst du auch gleich gehen", sagte
Lona in einem übertrieben freundlichen Tonfall.

"Lona, lasst uns doch erstmal frühstücken und lass unseren Besuch
erstmal richtig ankommen und wach werden", schlug Marişa vor. "Du
bist doch auch vor deinem ersten Morgen-Monua noch grumpy,
das sagst du selbst immer."

Salın hoffte gerade so sehr, dass Jurin Marişas Entschuldigung stehen
lassen würde, auch wenn sie so falsch war, aber konnte sich das
bei Jurin eigentlich nicht vorstellen. Sie wechselten
einen Blick.

Jurin zuckte mit den Schultern und wandte sich an Marişa: "Kann
ich noch irgendwas helfen?"

"Nein", antwortete Lona an Marişas Stelle. "Marişa macht
mir noch den Morgen-Monua, von dem die Rede war, und dann
geht es los. Soll sie dir auch einen machen? Setzt euch ruhig schonmal."

"Zu einem Monua sag ich nicht nein", wandte sich Jurin wieder an
Marişa.

Marişa nickte freundlich. "Heiß oder lieber lauwarm?"

Als wäre gerade nicht eine andere Konversation angefangen
worden, schritt Lona an Marişa vorbei
auf Jurin zu und hob deren Handgelenk an, das
Jurin gerade erst wieder sinken gelassen hatte. "Ist
das ein Windschwingen-Tattoo?"

Jurin entzog ihr die Hand. "Gibt's damit ein Problem?"

"Nein, nein!" Lonas Stimmung wechselte wie warmes
Wachs in den freundlich
einladenden Tonfall zurück, den ihr so viele einfach
abkauften. "Ich freue mich, sei willkommen! Wir mögen
interessanten Besuch. Wir sind kulturell sehr offen und
interessiert. Solange du nicht irgendwann unser
Kind entführst?"

"Entführen?" Jurins Augenbraue betonte die Skepsis.

"Ach, nimm mich doch nicht so ernst! Du weißt wie ich
das meine." Lona lächelte breit. "Als Eltern hat man doch
immer Angst, dass das Kind irgendwann auszieht und nicht
wiederkommt. Auch wenn es nicht alle Winschwingen so
halten, bin ich doch richtig informiert, dass ihr um die
halbe Welt reist, oder?"

"Schon." Jurin wirkte nicht ganz präsent, als sie das einräumte,
und wandte sich dann an Salın: "Wie alt bist du?"

"41."

"Magst du mir noch sagen, wie du den Monua am liebsten hättest?" Marişa hatte
lauernd auf einen Moment gewartet, das zu Ende
abklären zu können, und schob sich nun wohl eine Spur energischer
dazwischen, als es sonst ihre ultrasanfte Art war, um weitere
Fettnäpchen, wie sie es nannte, zu unterbinden.

Während sie und Jurin sich über die gewünschten Eigenschaften
des Monuas unterhielten -- Marişa erfüllte
Wünsche am liebsten sehr genau, damit auch nichts
falsch wäre --, zog Lona Salın an der Hand
ins Wohnzimmer mit.

Salın merkte, wie sie immer kleiner und unscheinbarer geworden war, bis
sie quasi nicht mehr da war, sondern höchstens ein Automatismus, der
ungefährliche Antworten gab, wenn nötig.

"Was für einen Elben hast du denn da
eingesammelt?", raunte Lona. "Ich mag ihn jetzt schon nicht."

Salın rann es unangenehm heiß den Rücken herab. Sie war sich
ziemlich sicher, dass Lona eine Lautstärke getroffen hatte, die
sich zwar nur an Salın bestimmt anhörte, aber doch gerade laut
genug für Jurin zum Mithören oder wenigstens Erahnen war. Aber sie
wusste es nicht. Und wenn es so wäre, wusste sie auch nicht,
ob Lona die Lautstärke absichtlich so wählte, weil sie in
Wirklichkeit wollte, dass ihre Meinung gehört würde, ohne dass
sich die betreffende Person verteidigen köntne, oder ob Lona
davon überzeugt war, dass Leute sie schon nicht hörten, wenn
Lona es nicht wollte.

Sie blieben immerhin nicht lange allein. Jurin folgte, und
Salın sah in ihrem Auftreten und der Art, wie selbstbewusst
sie sich bewegte, allein schon einen ausreichenden Grund, warum
Lona sie nicht ausstehen konnte. Sie erinnerte sich daran, wenn
sie früher dabei gewesen war, sich mit selbstbewussten Kindern
anzufreunden, dass Lona abfällig über sie geredet hatte: 'Das sind
so Leute, die denken, ich komme rein und mir gehört die Welt. Sowas
kann ich nicht leiden.'

"Ich habe versäumt, mich vorzustellen", sagte
Jurin. (Ein Indiz dafür, dass sie es tatsächlich gehört hatte?) "Ich
bin Jurin. Pronomen 'sie/ihr/ihr/sie'. Wo setze ich mich am besten
hin?"

"Wenn du keinen besonderen Extra-Wunsch hast, dann ist
dies der Platz für Besuch." Lona deutete auf einen Stuhl.

Jurin setzte sich ohne erkennbare Abwehr. "Danke für die
Einladung. Sieht gut aus, was es hier gibt!"

Lona freute sich über das Kompliment und tat etwas, was zwar
langweilig, aber ungefährlich war: Sie zählte auf, woraus
das Essen gemacht war, woher die Ingridenzien kamen, dass einige davon hier
im Garten gewachsen waren und wie sie das ganze ohne Spinstrom
zubereitet hatte. Sie monologisierte dann noch eine Weile darüber,
dass eine Welt ohne Spinstrom eine bessere wäre, wie sehr die
Bevölkerung inzwischen davon abhinge und dass sie diese
Abhängigkeit ja nicht für sich oder die ihren wollte.

Jurin hörte ihr einfach zu, begann mit dem Frühstück, als der
Monua geliefert wurde und Lona die Prozedur beiläufig eröffnete,
und kommentierte nichts.

"Aber jetzt rede ich die ganze Zeit von uns! Erzähl uns was von dir!", forderte
Lona Jurin schließlich auf.

Jurin zuckte mit den Schultern. "Ich lebe so vor mich hin. Bald steht wieder
eine Wanderung an. Was willst du wissen?"

"Ich mag nochmal auf unser Missverständnis vorhin zu sprechen kommen." Lonas
Ton war wieder so sanft und freundlich geworden und trotzdem hätte
Salın überall und immer gewusst, dass er hieß, widersprich mir nicht
oder Drama. "Ich verstehe deine Bedürfnisse, aber du musst auch meine
verstehen. In diesem Haus gehört Umarmung zu einer herzlichen Begrüßung
dazu."

"Schlechte Hausregel", kommentierte Jurin trocken.

"Mein Haus, meine Regeln." Dieses Mal war die Kiebigkeit in Lonas Stimme
sicher für niemanden zu überhören, vermutete Salın.

"Mein Körper, meine Regeln", konterte Jurin. "Auch wenn sich mein Körper in
deinem Haus befindet, darfst du ihn nicht einfach anfassen. Nichts gibt
dir das Recht dazu. Ich diskutier das nicht."

"Wenn dir das nicht passt, kannst du ja gehen", fauchte Lona. "Ich lass
mich hier nicht so anpampen."

Jurin betrachtete sie mit erhobener Braue. "Kann mich nicht erinnern,
je bei einer Sex-Party gegangen worden zu sein, weil ich mal nicht
berührt werden wollte. Und bei einer Sex-Party fände ich eine
Policy, dass mit Berührung gelebt werden müsse, auch schon nicht
so galant, aber doch eher verständlich als in einem
Privathaushalt, der sich einlandend oder willkommenheißend nennen will."

Jurin sah nicht aus, als wäre sie fertig, aber Marişa hob die
Hand. "Ich glaube, es handelt sich hier um ein Missverständnis. Hier
treffen verschiedene Generationen ..."

"Nee, tut mir leid, glaub ich nicht.", schnitt
Jurin ihr direkt das Wort ab. "Und wir
haben es eh eilig. Bist du satt genug, Salın? Können wir los?"

Salın bemerkte, dass sie sich bis gerade tatsächlich nicht existent
gefühlt hatte, mehr wie eine Hülle, und
kippte nun in ihren Körper zurück. Leider machte sie das nicht
unbedingt handlungsfähiger. Sie konnte sich einige Momente nicht rühren, saß einfach wie
erstarrt da. Sie hatte noch nie erlebt, wie jemand die Situation mit
ihren Eltern einfach so kompromisslos eskaliert hatte. Es erfüllte sie
innerlich gleichzeitig mit einer riesigen Angst und mit einem
Hauch von Stolz, den sie noch nicht greifen konnte: Ein Teil von
ihr wollte sich generell so ähnlich verhalten wie Jurin. Aber
das würde so nie möglich sein.

Sie spürte allmählich, wie auch die Handlungsfähikgeit
in sie zurückglitt. Was hatte Jurin gesagt? Sie
blickte auf in ihr Gesicht. "Eilig?", fragte sie.

"Los? Wohin?", fragte Lona zeitgleich.

Jurin wandte sich an die Tischrunde. "Salın und ich hatten einen Deal für
unser Date", behauptete sie. "Gestern hat Salın geplant, was
wir tun, heute bin ich dran. Ich habe noch nicht gesagt, was
wir vorhaben, es soll eine Überraschung werden. Aber wir müssen
den Zug um ..." -- Jurin blickte auf die Wanduhr, eine mechanische
mit Pendel und lautem Ticken -- "... um halb bekommen."

"Entschuldige, ich habe das gerade voll verdrängt", sagte Salın. Sie
war eigentlich wirklich nicht gut im Lügen, aber die Vorlage
war ziemlich gut.

"Musst du noch Sachen packen?", fragte Jurin.

"Kommst du vorher noch kurz in die Küche, Kind?", bat Lona. "Es
wäre schade wenn das gute Essen auf den Kompost
müsste. Ich würde gern die Reste vom Frühstück mit dir einpacken."

"Das kann ich auch machen." Jurin stand auf.

"Salın weiß, wo die Dosen sind, das ist unkomplizierter", beharrte
Lona.

Jurin zuckte mit den Schultern. "Dann sollte das auch nach
dem Packen noch möglich sein", sagte sie. "Mir scheint wahrscheinlich,
dass du Allein-Zeit mit ihr willst, und ich hätte meine Allein-Zeit mit ihr,
wenn das für Salın okay ist, gern vorher."

Es herrschte mit einem Mal Stille, die sich für Salın so anfühlte,
als wäre ein Blitz eingeschlagen.

"Salın ist mein Kind!", knurrte Lona zwischen zusammengebissenen
Zähnen hindurch. "Was gibt dir das Recht, dich in unsere Familienangelegenheiten
zu drängen?"

Jurin setzte zu einem Widerspruch an, aber Salın hob kaum merklich
die Hand. "Lass gut sein", meinte sie. "Ich komme gleich nach."

Aus ihr unerfindlichen Gründen fühlte sie gerade eine Energie, die sie
selten in Lonas Gegenwart hatte, und sie wollte wissen, was das mit
ihr machte. Und sie wollte Jurin nicht weiter in die Situation reinziehen
als eh schon.

Sie folgte Lona mit den Frühstücksplatten in die Küche, und befasste
sich sofort damit, Dosen aus den Schränken zu nehmen.

"Nicht die rote. Meinst du nicht, dass die längliche mit dem schwarzen
Deckel am besten geeignet ist?", fragte Lona freundlich.

Es war Salın vollkommen egal. "Wir können die nehmen."

"Du kannst deine Meinung ruhig äußern", motivierte Lona.

Salın verdrängte das Szenario, dass sie tatsächlich eine Meinung zu
den Brotdosen äußern könnte, die von Lonas abwich, Lona das nicht
annehmen konnte, sie mit Argumenten zu überzeugen versuchte und
schließlich doch über sie entschied. Sie kannte
dieses Skript so sehr auswendig, dass es sich nur im Bruchteil einer Sekunde
voll entfaltet hatte, und genauso rasch war es wieder zusammengefaltet
gut verstaut. "Okay, meine Meinung?", fragte Salın. "Ich mag Jurin."

"Sie kommt mir nicht wieder ins Haus", zischte Lona.

"Ich weiß", sagte Salın, vollkommen gefühllos. Jegliches sinnvolle Gefühl
hätte hier auch einfach keinen Raum bekommen.

"Sei vorsichtig mit ihr", warnte Lona, und wählte dafür einen
innigen Tonfall, als wäre Salın ihr wichtig. "Mir ist sehr wohl klar,
dass die Sache mit dem Date und dem Zug eine Lüge war, um dich vorschnell
aus dem Haus zu lotsen."

'Woher weißt du?', dachte Salın, sagte aber nichts.

"War es doch, oder?", bohrte Lona unbarmherzig nach. "Ich bin dir
nicht böse. Das weißt du. Aber ich möchte, dass du ehrlich zu
mir bist."

Salıns Kopf nickte. Salın versuchte, nicht enttäuscht von ihm
zu sein.

Lona lächelte sanft und nahm Salıns Wangen in ihre Hände. "Vielleicht
habe ich vorhin etwas überreagiert. Aber du musst verstehen: Ich
möchte nur nicht, dass dir jemand wehtut", sagte sie eindringlich. "Jurin mag
bestimmt ein paar Seiten an sich haben, die dich faszinieren. Ich
kann aber nicht haben, wie sie sich einfach ohne jegliche
Rücksichtnahme alles einfordert. Wenn
sie was will und du irgendwann im Wege stehst, dann lässt sie dich
fallen wie eine heiße Kartoffel."

Salın wollte das nicht glauben, kein Stück, und konnte doch nicht
vermeiden, dass ein Teil von ihr es doch tat: Jurin ließ sich nicht
so leicht etwas wegnehmen, das stimmte. (Warum auch?) Sie hatte
bereits angekündigt, dass sie keine haltende Beziehung eingehen
würde. (Womit Salın einverstanden war.) Aber was war der Unterschied
zwischen dieser Realität und dem Fallen-gelassen-werden wie eine heiße Kartoffel, wenn es
nicht mehr so passte, abgesehen von der Wortwahl? Salın wusste, dass
da einer war, konnte ihn aber gerade nicht greifen. Wie als wäre
sie nicht ganz da, sondern halb eine Persönlichkeitsanteil von ihr,
der Lona einfach voll vertraute oder fast spiegelte. Es war unheimlich.

"Sie hat gelogen, Salın", betonte Lona.

Wenn Lona eines moralisch
komplett verwerflich und verurteilungswürdig fand, das wusste
Salın, so war es lügen. (Außer, Lona log selbst, aber das
waren dann immer 'Notlügen'.)

"Und was ich noch schlimmer finde:", fuhr Lona
fort, "Sie hat dich mit reingezogen. Sie hat dich in eine
Lage gebracht, wo du deine Mutter anlügen oder Jurin vor mir des
Lügens bezichtigen müsstest. Natürlich hast du sofort mitgespielt. Es
war manipulativ, sie hat dich da mit reingesogen. Das wolltest
du gar nicht. Stimmt's?"

Salın erinnerte sich, dass sie etwas daran tatsächlich angekratzt
hatte. Wieder nickte ihr Kopf reflexartig. Verräterische
Tränen wollten in ihre Augen treten. Es war so unfair: Sie *hatte*
unbehagliche Gefühle Jurin gegenüber gehabt. Sie hatte -- *eigentlich* -- noch
viel schlimmere Lona gegenüber. Aber die Jurin gegenüber waren hier
erlaubt, ja sogar irgendwie produktiv, die Lona gegenüber hätten
alles nur schlimmer gemacht, deshalb wurden sie erst gar nicht
gefühlt. (Heute Abend in Niederwiesenbrück würde sie sie
spätestens fühlen, als kaum sortierbares Gefühlsknäuel.)

"Sie hätte dich wenigstens vorher fragen müssen", sagte Lona. "Und
wenn sie dich gefragt hätte, was hättest du gesagt?"

"Dass ich nicht lügen kann", murmelte Salın.

Lona nahm sie in den Arm. "Ich habe so ein grund-ehrliches Kind
erzogen. Da bin ich richtig stolz drauf", sagte sie. "Lass
dir das niemals nehmen. Von niemandem. Du bist gut so, wie
du bist, mein Präsentierexemplar von einem Kind." Sie ließ
Salın los. "Und nun ab mit dir!", sagte sie freundlich lächelnd. "Pack
deine Sachen. Ich geb dir gleich die Dose mit." Und als Salın schon
fast zur Tür raus war: "Die mit dem schwarzen Deckel, genau wie du
wolltest!"

---

Jurin lehnte gelassen an die glatte Wand neben der Haustür gelehnt,
als Salın endlich das Haus verließ, Lonas 'Pass auf dich auf, mein
Wunschkind!' noch in den Ohren. Ihre Blicke trafen sich und
in Salın löste sich etwas. Es war seltsam, was diese Person allein
durch ihre in sich ruhende Präsenz für einen Einfluss auf Salıns Gefühle hatte. Vielleicht
trug auch einfach das Bild der nackten Schultern im Sonnenschein
dazu bei, das Salın das Gefühl des leichten, noch kühlen
Sommerwindes, der unter ihre eigene Bluse drang, erst
präsent machte. Sie mochte den Sommer.

Tatsächlich war Salın schon oft in eher ungesunden Beziehungen
gelandet. Daher löste es in ihr drin einen Alarm aus, dass
der bloße Anblick Jurins diesen Einfluss auf sie hatte. Sie hatte bei all den
Personen, mit denen sie Freundschaften oder Beziehungen gehabt
hatte, die sich später als ausbeutend oder anderweitig ungut
für sie herausgestellt hatten, starke, positive Gefühle empfunden. Zum
Beispiel eine Verliebtheit wie jetzt. Oder sie hatte den Charme geliebt.

Es gab allerdings einen Unterschied zwischen jenen Beziehungen
und der zu Jurin: Lona hatte all jene bei sich willkommen geheißen. Aber
das musste nichts heißen, überlegte Salın. Lona würde mit sich selbst
überhaupt nicht klarkommen, sie könnte keine Person bei sich
willkommen heißen, die versuchte, dominanter zu sein als sie selbst. Wenn
sie also eine Person wie Lona nach Hause brächte, dann würde
Lona sie hassen und es wäre kein Indiz dafür, dass sie
Salın guttäte.

Jurins Blick schweifte über Salıns Gepäck. "Sieht aus, als hättest
du für eine Woche gepackt. Aber manche Leute brauchen für einen
Tagesausflug auch einen Wanderrucksack. Wieviel Zeit haben wir?"

"Ich habe vor, heute Abend zurück nach Niederwiesenbrück zu
fahren, wo ich eigentlich wohne", erklärte Salın. "Entschuldige,
dass es so lange gebraucht hat. Ich habe den Pferdis noch
Aufwiedersehen gesagt."

In Jurins Gesicht trat ein feines Lächeln, nur für einen
Moment. Ansonsten wirkte ihr Gesicht überraschend finster. "Ich
finde es schön zu hören, dass du hier nicht immer wohnst."

Salın konnte nicht festmachen, was sie an Jurins Verhalten nun
schon wieder störte. Vielleicht war es all der Zweifel, der noch
in ihr bohrte: Ihr Unvertrauen in ihre eigene Charakterkenntnis
und Urteilungsfähigkeit in Beziehungen, gepaart mit dem Unbehagen,
das Lona gesäht hatte.

Jurin stieß sich von der Wand ab und bestimmte so den Aufbruch. Sie
nahm Salin eine Tasche ab, indem sie wortlos mit ausgestrecktem Arm
danach fragte. Sie schwiegen, als sie am Strand entlang gingen. Salın
versuchte mehrfach, sich zu überlegen, wie sie ein Gespräch anfangen
könnte. Was musste Jurin fühlen? Konnte sie einfach nachfragen? Aber
wie? 'Wie geht es dir?', kam ihr nicht nach genau der Frage vor, deren
Antwort sie gerade wollte.

Ohne für sie memorablen Übergang landete sie plötzlich wieder emotional
in Lonas Umarmung und hörte die Weichheit in ihrer Stimme, als sie
sie vor Jurin gewarnt hatte. "Du hast gelogen", murmelte Salın. "Lona
hat mit mir über dich geredet und gesagt, du hättest mich fragen sollen,
bevor du mich in so etwas reinziehst." Im Moment, als sie es fertig
ausgesprochen hatte, durchflutete sie ein scharfes Gefühl von Selbsthass, dass
alles in ihr lähmte. Warum von allen Möglichkeiten der Gesprächseröffnung
hatte sie sich gerade die ausgesucht, in der sie Lonas Kritik auf Jurin
übertrug? (Aber es beschäftigte sie, dass Jurin das getan hatte, sie konnte
es nur nicht von Lonas Gefühlen trennen.) Sie hatte Angst, dass es
gleich mit Jurin vorbei wäre.

"Recht hat sie", sagte Jurin. "Hätte ich tun müssen." Sie seufzte. "War
nicht gut von mir, wenn auch vielleicht aus anderen Gründen, als Lona
denkt."

Salın blickte sie überrascht, fast perplex an. Mit der Wendung hätte sie nicht
gerechnet.

"Es tut mir leid", sagte Jurin.

Salın hatte keine Mühe, ihr zu glauben, dass es eine ehrliche
Entschuldigung war. Aber es verwirrte sie. "Warum?"

"Mir war schon in dem Moment klar, dass ich mich nicht gut
verhalte", gab Jurin zu. "Ich war überfordert. Ich kenne für solche
Situationen keine Skripte. Ich wusste nur, ich kann Gewalt nicht
einfach stehen lassen, wenn sie passiert. Ich kann es nicht. Ich
kann einfach nicht tatenlos dabei sein, wenn gesetzte Grenzen
überschritten werden oder was da alles an Gewalt passiert ist." Sie
atmete einige Male ein und aus, unverkennbar, um Wut zu
regulieren. "Ich habe keinen sinnvoll kontrollierten Umgang
damit gefunden und dann irgendwann auf die nächstbeste Option
zurückgegriffen, die mir einfiel, und die war halt wirklich
nicht gut."

"Ich weiß nicht, Jurin", sagte Salın. Sie war sich nicht sicher,
ob Jurin fertig war oder sie sie unterbrach. Ihr Hirn war aber
auch ohnehin so vernebelt, dass ihr das Zuhören schwerfiel. "Du hast
versucht, mich aus einer Situation zu retten. Ich habe mich in dem konkreten Moment, als
du die Lüge vorgebracht hast, auch mit dir nicht so wohlgefühlt, zugegeben,
aber du kannst da gar nichts für, glaube ich."

"Doch, schon." Jurin sagte zunächst nichts weiter.

Die Brandung hatte wieder zugenommen, weil der Wind von der Seeseite
kam und allmählich auffrischte. Salıns Schuhe, die am Rucksack befestigt
waren, baumelten gegen ihre nackten Beine. Jurin trug die Stiefel sogar am
Strand. Salın fand es immer merkwürdig, wenn Leute den Sand nicht fühlen
wollten. Dabei hatte sie gerade auch kaum die emotionale Kapazität
um darauf zu achten.

In die entstandene Stille, die mit Meeresrauschen gefüllt war, sagte
Jurin: "Ich denke, ich kann greifen, wofür ich mich schäme. Darf
ich?"

Salın nickte.

"Lona hat immer nur über euch geredet. Über dich und, Ma-, Maşi...? Wie
hieß deine andere Mutter?", fragte Jurin.

"Marişa."

"Allein, dass du sie 'andere Mutter' nennst, ist auffällig. war das immer so?"

Salın nickte wieder. "Ja. Ich weiß, was du meinst."

"Es hat auf mich spontan wie eine Servant-Dom-Beziehung gewirkt, und
es hat sich höchstens semi konsensuell oder gesund
angefühlt", sprach Jurin aus.

Salın kicherte. "Ich habe noch nie im BDSM-Kontext über meine Eltern
nachgedacht." Sie grübelte einen Moment. "Vielleicht, weil ich BDSM
immer mit Konsens verbunden habe, auch wenn ich wahrlich nicht
nur gesunde oder durchweg-konsensuelle BDSM-Beziehungen hatte."

"Zurück zum Punkt", sagte Jurin. "Lona hat über dich und Marişa geredet,
teils sogar für euch. Also sie hat zum Beispiel einmal statt Marişa
geantwortet, als ich bewusst Marişa gefragt habe, weil es mir
aufgefallen war, dass sie übergangen wird. Ihr wart immer nur Figuren in ihrer Story, sie hat
euch nicht als eigenständige Personen wahrgenommen, die einen eigenen
Willen haben. Ihr Bild von euch ist weit weg von dem, was ihr seid, aber
sie interagiert so, dass es für euch schwer ist, nicht automatisch in dieses
Bild zu verfallen. Beziehungsweise,
von Marişa weiß ich das nicht, aber bei dir kam's mir so vor."

Salın unterdrückte den ersten Impuls zu nicken und entschied sich im
nächsten Moment bewusst dafür. "Du beobachtest gut."

"Es ist lange her, aber ich hatte Trainings für sowas fürs
Spiel", erklärte Jurin. "Ich bin nicht besonders stolz darauf, aber
ich war mal ganz gut darin, ähnliche Strategien auf gegnerische
Gruppen anzuwenden oder deren manipulatives Spiel zu erkennen und
abzuwehren. Wobei Lona ein Kaliber für sich ist. Dem bin ich
nicht gewachsen."

"Ich auch nicht. Dabei ist das eine Sache, die ich seit Jahren in
der Therapie übe", murmelte Salın.

"Es ist sehr schwer", sagte Jurin. Eine Spur Sanftheit schlich sich
in ihre Stimme. "Vor allem, wenn du damit großgeworden bist und
Marişa da auch keine Hilfe war. Davon gehe ich zumindest gerade aus."

"Sie war keine Hilfe", bestätigte Salın. "Manchmal habe ich das Gefühl, Marişa lebt
schon so lange unter Lonas Einfluss, dass sie gar keine eigene Persönlichkeit
mehr hat."

"Gut möglich", kommentierte Jurin. Ihr Mund verzog sich einen Moment
in ein nicht glückliches Lächeln. "Aber mit mir hat sich Lona
unterhalten."

Salın schüttelte den Kopf. "Glaubst du das? Ich glaube, auch du warst
ihr vollkommen unwichtig."

"Nee, war ich nicht. Ich war ihr nicht positiv wichtig, aber sie hat
sich zumindest anteilig wirklich mit mir befasst, und das hat mich
über Marişa und dich gestellt." Jurin seufzte sehr tief. "Und ich bin
drauf angesprungen und habe dasselbe mit dir gemacht."

Bis gerade hatte Salın widersprechen wollen. Sie glaubte nicht, dass
sich Lona auch nur ein ernsthaftes kleines bisschen für Jurin
interessierte. Aber Jurins Schluss fühlte sich richtig
an. Salın spürte gleichzeitig einen Reflex, es abzustreiten. Sie
hatte diesen steten Impuls, Leuten, die Schuldgefühle hatten, diese
auszureden und für unnötig zu erklären. Wieder trat dieser Nebel in
ihren Kopf.

"Sie wollte nicht, dass ich dich ihr quasi wegnehme, und ich habe
auf eine Art dagegen angekämpft, die dir auch keinen Raum
gibt. Vielleicht wie eine Damsel in Distess. Jedenfalls ohne
dir die Möglichkeit zu geben, zu entscheiden", schloss
Jurin. "Und das tut mir leid."

"Aber dann hat sie dich ja schon in ein Mindset gebracht, dass du nicht
wolltest, oder?", fragte Salın. "Also, sie hat dich in eine
Rolle gedrängt, die du dann angenommen hast."

Jurin nickte. "Genau."

"Aber wie ist das dann anders, als wenn sie Marişa und mich in
eine Rolle drängt, in der wir machen, was sie will?"

Jurin runzelte die Stirn. "Es *ist* anders. Moment, lass mich
kurz denken." Sie versuchte, ihr Haar über die Schulter nach
vorn zu streichen, aber es wehte einfach wieder nach hinten. Sie
verhedderte sich mit ihren Fingern zwischen den Strähnen, bevor
sie irgendwann aufgab. "Hm, nein,
ich weiß es nicht genau. Ich kanne es noch nicht greifen. Ob
sie mich als Gegnerin wahrgenommen hat, aber
eben wenigstens wahrgenommen?"

"Vielleicht wollte sie, dass du dich in einer Weise verhältst, die
verwerflich ist, sodass sie dich hinterher bei mir schlechtreden
kann?", schlug Salın vor.

Jurin grinste. "Ich mag, wie du denkst, aber ich weiß nicht, ob
ich da mitgehen würde", sagte sie. "Ich habe einen Fehler
gemacht, und sie hat sich darauf gestürzt, das glaube ich
schon. Aber ich glaube, sie hätte so oder
so etwas an mir gefunden, was sie schlechtreden kann. Selbstbestimmtes
Grenzensetzen eignet sich allein schon hervorragend für
Tatperson-Opfer-Umkehr."

Salın stöhnte genervt -- was bei ihr stets bloß wie ein zartes, leicht
unglückliches Seufzen klang. "Ja, sie ist da sehr gut drin. Sie
hat auch vorhergesagt, dass du mich fallen lassen wirst, wenn dir
was an mir nicht passt."

"Werde ich nicht", versprach Jurin.

Salın war so überrascht, dass sie stehen blieb. "Wie? Was?"

Jurin stolperte kaum merklich wegen des abrupten Stopps, aber
fing sich routiniert und wandte sich ihr zu. "Ich
möchte gern festhalten: Wenn du da irgendwann ganz rausmusst, wenn
du untertauchen musst oder sonst etwas, und du denkst, ich kann die
Person sein, an die du dich dann wenden würdest, zögere nicht und ruf mich an oder
schick mir eine Nachricht mit hoher Priorität. Ich hole
dich überall ab. Auch mitten in der Nacht. Ich
bringe dich an einen sicheren Ort. An einen, wo du dich nicht selbst um deine
Versorgung kümmern musst, wenn du da Hilfe brauchst. Ich
bleibe bei dir, solange wie du das brauchst. Es ist immer
wichtig genug und es ist auch dann okay, sollten sich unsere
Wege getrennt haben und ich erwarte nichts dafür."
