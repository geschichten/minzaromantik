Widmung
=======

\Beitext{Jurin}

*CN: Wirbellose Tiere: Quallen*

Jurin war zur zweiten Boje rausgeschwommen und wusch aus Spaß die
Flagge daran im Meerwasser aus. Sie genoss das Fitzelchen Angst,
das sie hatte -- Angst, auch ein eher untypisches Gefühl für
sie --, weil um sie herum und bis weit in die Tiefe unter ihr
nur Wasser war. Und Quallen, aber die taten nichts, auch wenn Jurin
sich jedes Mal erschreckte, wenn eine ihren Körper streifte.

Sie würde eine Vierteilstunde brauchen, um die ganze Strecke
zurückzuschwimmen. Sie sollte Mauk mit dem Eis nicht
warten lassen, dass Mauk ihnen holen wollte. Jurin atmete tief durch, schmeckte das
Brackwasser auf ihren Lippen, dessen Salzgehalt hier im Nachtmeer
im Vergleich zu anderen Meeren niedrig war, und
trat den Rückweg an. Trat? Flosste? Obwohl:
Flossen trug sie auch nicht. Aber 'schwamm den Rückweg an' wäre
ein Äquivalent zu 'ging den Rückweg an', wenn es um gehen statt schwimmen
ging, nicht für 'trat den Rückweg an'.

Sie kicherte und verschluckte sich fast. Die Kreise, in denen sie sich
aufhilt, hinterließen eindeutig ihre Spuren. Solche Gedankengänge waren
typisch für die WG, in der Linoschka wohnte.

Jurin genoss die langen Schwimmzüge, das Ausatmen unter Wasser, die Kraft
und Energie, die sie in ihrem Körper dabei fühlte, und stellte sich
dabei vor, dass es um mehr ginge als nur anzukommen.

Manchmal vermisste sie schon, am Spiel teilzunehmen. Sie war seit einigen
Jahren immer mal wieder in einer Spiel-Orga und leitete eine Runde
mit, aber das war etwas völlig anderes, als in einem Abenteuer gegen
andere Gruppen anzutreten, ihnen Fallen zu stellen, wegzurennen, wenn es
etwas zu verlieren gab oder um etwas zu kämpfen. Sie mochte das Hochgefühl, einer
anderen Person den Weg abzuschneiden und sie an den Anfang zurückzuwerfen,
sowie das Adrenalin, selbst zu riskieren, dass so etwas mit ihr
passierte. Sie mochte, in all dem einen ruhigen Kopf zu bewahren und
Strategien zu erarbeiten, von denen zwar ein Großteil nicht aufging, aber
manche eben doch schon, was dann so eine unbeschreibliche Euphorie
bedeutete. Sie bereute nicht, die Entscheidung
getroffen zu haben, nicht mehr zu spielen, aber sie hatte noch nicht
so recht einen Ausgleich dafür gefunden.

---

Mauk saß nur mit einer engen, violetten Badehose bekleidet auf einem Handtuch, das neben Jurins
ausgebreitet war, und las eine EM-Zeitung. Jurin kannte dieses Device-Format
bisher vor allem von Mauk: Es war eine Folie, etwa in der Größe eines
gewöhnlichen Laptops, die sich sehr dünn falten ließ und auf der sich Artikel
platzsparend anordneten, die eine Person lesen und vielleicht vergleichen
wollte. Leute, die eine EM-Zeitung benutzten, wählten zumeist eine kleine
Schrift, und genossen den Vorteil, dass Texte einen Ort hatten, an dem sie blieben,
statt sich durch Scrollen zu bewegen.

Jurin nahm neben Mauk Platz und legte sich ein schwarzes Tuch um die nackten Schultern. Sie
tauschten grinsende Blicke.

"Waldmeister-Minze, mylady", sagte Mauk. "Zitrone-Minze war aus."

Jurin reagierte nicht sofort, überlegte, Mauk für eine Unperfektion zu
strafen, für die Mauk nichts konnte. Aber Mauk würde sich wehren, und
Jurin wollte gerade lieber Eisessen als das Luder zu überwinden. "Wird
genauso taugen."

Mauk reichte ihr den Becher und beschäftigte sich mit dem eigenen. "Ich
hab da nochmal eine Frage zu heute Abend."

"Sag an!" Jurin betrachtete Mauks verspieltes Lächeln und den Blick, der
nun auf ihr Eis gesenkt war. Er wirkte gleichzeitig verträumt und so wach.

"Du hattest dir also vorgestellt, mit mir am Abend ins Antikuarium zu
gehen, um diese Bonbon-Schau anzusehen." Mauks Stimme klang amüsiert. "Und
ich möge zu deinen Füßen liegen und *leise* deine Stiefel putzen."

Jurin nickte. "Das hatte ich mir so ausgedacht." Sie ließ sich einen Löffel
des Eises auf der Zunge zergehen. Nach dem Schwimmen und an diesem warmen
Sonnentag erfüllte es sie mit einem so tiefen Wohlgefühl, diese Kühle
im Mund mit dem erfrischenden Geschmack zu genießen. "Ich wollte Neues
ausprobieren. Fällt dir was Extremeres ein?"

Mauk richtete ihren verschmitzten Blick auf Jurins Gesicht. "Ich fürchte,
daraus wird heute insgesamt nichts", sagte sie. "Ich habe vorhin ein Moosplakat
gesehen, das die BonBon-Show im Antikuariat ankündigt. Es ist keine Kink-Party. Es
ist einfach eine BonBon-Show. Ich glaube nicht, dass das Publikum eine Extra-Show
im Parkett oder den Rängen sehen will, die von der Bühne ablenkt." Mauk
kicherte leise. "Oder wusstest du das und wolltest eine Veranstaltung
zweckentfremden?"

Jurin starrte Mauk einige Momente mit erhobener Augenbraue
an und schüttelte schließlich den Kopf. Eilig holte
sie ihren Faltrechner aus ihrer Gürteltasche, die sie oberhalb ihrer Handtücher
abgelegt hatte, und recherchierte. "Recht hast du! Schiete!" Sie las die
Ankündigung genauer. "Es gibt außerdem bereits so viele Interessierte, dass sie ein
Reservierungsmodell einführen, nach dem die Hälfte der Plätze vorbestellt
werden können und die andere an Leute vor Ort ausgelost werden."

"Wurde für dich vielleicht reserviert?", fragte Mauk.

"Das schaue ich gerade nach." Jurin sendete die Anfrage und erhielt direkt
die Antwort, dass für sie ein Platz in der ersten Reihe reserviert war. Leider
fand sie schnell heraus, dass in der ersten Reihe kein weiterer Platz für
Mauk frei wäre. Die Anfrage, ob jemand zu ihren Füßen sitzen dürfe, war keine,
die eine KI beantwortete, also ließ die Antwort erstmal auf sich warten. "Ich
hoffe, du kommst rein! Also wenn du willst."

Mauk zuckte mit den Schultern. "Ich würde schon gern, aber ich würde auch
nicht bereuen, wenn wir einen schönen kinky Strandtag verbringen und es
das für heute war." Auf Æreniks nachdenklichen Blick hin, fügte sie
hinzu: "Und wenn es nicht kinky wird, ist auch gut."

"Du glaubst, mir würde nichts einfallen?", fragte Jurin.

Mauk schnaubte. "Glaub ich, ja! Etwa doch?"

Dieses Luder einfach! Jurin stellte ihr Eis ab. "Fütter mich! Aber
auf eine Weise, die sich nicht anfühlt, als könnte ich es nicht
selbst."

---

Die Nachricht, dass Mauk zu ihren Füßen sitzen dürfte, kam am frühen Abend, nur etwa
zwei Stunden vor Einlass. Sie machten noch einen gemeinsamen Spaziergang an der
Promenade entlang. Es war viel los auf diesem breiten, gepflasterten Streifen Weg zwischen
Strand und den Läden für Eis, Heißgetränke, Blumen und Hüte. Anders als in der Stadtmitte
waren dies ausschließlich Läden, die Selbstgemachtes darboten.

Jurin betrachtete einen schwarzen Sonnenhut genauer, der ihr gefiel und lauschte
halb auf die Stimme des alten Lobbuds, der detailliert erklärte, woraus der Hut bestand
und welcher Pflege er bedürfe. Als Jurin sich überlegte, aber wirklich genug
Dinge zu haben, schien er beinahe enttäuscht.

Mauk trug zusätzlich zur Badehose einen durchscheinenden, schwarzen
Cardigan, sonst nichts. Sie hatte einen auffälligen Körper. Etwa hatte sie zwei
sehr verschieden große Brüste, eine undurschnittliche Körperfettverteilung und ebenso
untypische Proportionen. Jurin bemerkte die vielen Blicke, die Mauk, wahrscheinlich
deshalb, galten. Sie hätte sich Gedanken gemacht, ob es Mauk missfiele, aber sie wusste
von ihr, dass Mauk diese Blicke manchmal bewusst provozierte. Es war ihre Art, zu
demonstrieren, dass sie genau richtig war, wie sie war.

"Wer ist diese Salın-Person eigentlich?", fragte Mauk. "Was findest du an ihr?"

"Wieso fragst du?" Mauks Interesse kam für Jurin ein wenig aus dem Nichts.

"Ehrlich gesagt ..." Mauk haderte ungewöhnlich lang, aber Jurin ließ ihr
die Zeit. "Die Art, wie du über Salın sprichst, klang für mich irgendwie
so Date-artig. Und das kann ich mir bei dir einerseits nicht vorstellen
und andererseits ... Naja, du musst halt nicht konsistent mit dir selber
sein. Und mich hat's interessiert, ob an meinem Gefühl was dran ist."

"Oh", machte Jurin. "Dann muss ich wohl vielleicht doch bei ihr aufpassen."

"Nein, das meinte ich nicht", unterbrach Mauk Jurins Gedankenkette, die
sich anbahnen wollte. "Ich kann sie durch deine Erzählungen so überhaupt
nicht einschätzen. Aber du, -- du redest darüber auf eine Art, ich weiß
nicht, weniger strategisch als sonst vielleicht?"

"Hm." Jurin beließ es zunächst bei diesem nachdenklichen Laut und strich sich
durchs Haar -- dass sie offen trug, das erste Mal absichtlich seit ihrer
Kindheit. Sie schnaubte über sich selbst. "Da ist wohl was dran."

Mauk lachte. "Ich liebe deinen nordwestmaerdhischen Dialekt
einfach. Besonders bei solchen Sätzen, die eigentlich Verunsicherung
ausdrücken, aber bei dir: so breit und ausgelatscht, als wäre deine
Nachdenklichkeit eine volle Überzeugung!"

"Ich bin voll überzeugt nachdenklich!" Jurin grinste mit.

Sie war nicht interessiert an romantischen Beziehungen, da gab
es nichts dran zu leugnen. Und genauso ließ sich nichts daran
leugnen, dass Mauk recht hatte, dass sich die Show mit Salın wie ein
Date für Jurin anfühlte, und das kein schlechtes Gefühl war. Ein
aromantisches Date?

"Mauk?"

"Ja?" Mauk wackelte mit den Augenbrauen.

"Ich glaube, ich habe dich gefragt, bei der Show mein Stiefelleckluder zu
sein, weil mir die Date-Sache ein bisschen unheimlich ist und ich gehofft
hatte, dass es dem ganzen diese Bedeutungsebene nimmt." Einen
Moment fürchtete Jurin, dass sie Mauk damit verletzt haben könnte.

Aber Mauk grinste einfach weiter. "Halt mich auf dem Laufenden!" Mauk
stupste sie mit dem Finger gegen die Schulter. "Außerdem mag ich die
Erniedrigung, zwecks Ausrede dir selbst gegenüber benutzt zu werden."

Jurins Hand schloss sich reflexartig um Mauks Handgelenk in ihrer
Schultergegend, um es schmerzhaft zu verbiegen, und lachte. "Das
erleichtert mich!"

"Haaaah", machte Mauk, ein Laut zwischen Schmerz und
erbärmlichem Genießen. "Gerne wieder!"

---

Eine Show gewidmet zu bekommen, war ein Erlebnis für sich.

Zwischen vielen anderen in roten oder rosa BonBon-Kostümen tanzte Salın in einem
türkis-weißen. Es hatte ähnliche, aber nicht identische Farben zu dem
von letzter Woche. Sie hatte dieses Mal sogar plüschige Engelsflügel.

Sie tanzten auf diese fröhliche, fast übermütige
Art, die Jurin noch nie zuvor so bewundert hatte: Es steckte so viel
Schwung und Gehopse in dem Tanz, dass es bedeuten musste, dass die Tanzenden
sehr muskulös und trainiert waren, und gleichzeitig wirkte alles
so leicht, als wären sie noch jugendlich und euphorisch am
Spielen. Es war eine sehr interessante Stimmung von Leichtigkeit.

Gleichzeitig waren die Bewegungen, das Hochschwingen der Beine und
Röcke, die Blicke auf Unterwäsche versprachen, die Jurin aber doch
wegen der vielen Unterröcke nie zu sehen bekam, eigentlich wohl
sexuell konnotiert. Aber Jurin wusste es nur, sie nahm das nicht so
wahr. Vielleicht sogar im Gegenteil: Durch die Leichtigkeit
wurde für Jurin dem Ganzen eine solche Bedeutungsebene genommen. Es
war einfach lustigschön und irgendwie selbstbewusst.

Und dann wiederum war es einfach krass, so eine Show von der Person
gewidmet zu bekommen, die heute Hauptrolle tanzte! Jurin hatte
auf diese Art ein ganz anderes Gefühl zur Show. Zugegeben, sie hatte
nicht viele Gefühle, die sie zu Shows bisher gehabt hatte, als Vergleich,
weil sie eigentlich interaktive Kunst lieber mochte. Sie hatte wohl
in ihrer Jugend und auch ein- zweimal später ein Theater oder ein
Konzert gesehen. Es hatte ihr nicht so viel gegeben. Und das lag
vermutlich vor allem daran, dass sie nicht mit den Darstellenden interagiert
hatte und sie ihr fremd waren. Hier aber kannte sie Salın, wenn auch
nur flüchtig. Das änderte alles.

Nach zwei längeren Tanzstücken folgte eine Gesangseinlage, in der
auch Salın sang. Jurin verstand kein Wort, bemerkte aber durchaus, dass
Salın oft an den Bühnenrand schritt, um sie bei einigen Strophen
direkt anzusehen. Jurin fühlte sich dann immer sehr bewusst, und
spürte die eigenen Finger, die dabei sanft über Mauks
Nacken strichen, der an ihren Knien anlehnte.

Dann folgten ein paar Aufführungen, in denen Salın nicht dabei war,
und dann noch ein recht kurzer Tanz, bei dem Salın mehrere Räder schlug,
bei denen ihre Rocklagen erstaunlich wenig verheddert wirkten.

Als der Applaus aufbrandete, sprang Salın von der Bühne und hockte sich
vor aller Augen und vor Jurin hin. "Das war so schön, für dich zu tanzen! Wartest
du hinterher noch auf mich?"

"Klar!" So wenig Höflichkeitsempfinden Jurin auch zugeschrieben wurde, so
sehr wusste sie, dass sie nach sowas nicht einfach gehen sollte. Und
sie wollte es auch nicht. "Wenn du länger brauchst, warte ich an unserem
letzten Treffpunkt."

Salın hauchte ihr Küsschen auf die Wangen. Erstaunlicherweise fühlten sie
sich vollkommen anders an als ihr Küssen letzte Woche. Vielleicht waren
es flüchtige Küsse ihrer Bühnen-Persona.

---

Mauk beschloss, an der Bar auf Jurin zu warten und eine Himbeerlimonade zu trinken,
damit Jurin in Ruhe Salın treffen konnte. Jurin wartete an die Mauer
gelehnt, wie letztes Mal. Es fühlte sich sehr anders an als letzte Woche, aber
vielleicht auch nur deshalb, weil Jurin hier zum Warten stand und nicht
zum Runterkommen.

Auch, als Salın aus der Tür trat, war alles anders. Salıns Körper, sonst
so voll Energie, wirkte nun eher kaum in der Lage zu stehen. Jurin bot
unsicher einen Arm an, aber Salın lehnte sich mit einem Meter Abstand zu ihr
an die Wand, und Jurin ließ den Arm wieder sinken.

"Was ist los?", fragte sie leise. "Also, du musst nicht begründen, warum
du gerade die Nähe nicht willst. Das meine ich nicht."

Salıns Körper entspannte sich bei einem Ausatmen. Das Lächeln, das sie
in ihrem Gesicht geklemmt hatte, löste sich auf. "Ich wollte eigentlich da anknüpfen,
wo wir letzte Woche aufgehört haben. Aber ich kann nicht. Es liegt nicht
an dir. Es liegt am Jetzt." Sie kicherte lautlos und unfröhlich. "Du hast irgendwie so
eine Art an dir, dass ich dir einfach vollkommen ehrlich meine Gefühle
ins Gesicht klatsche!"

Jurin versuchte ein vorsichtiges Lächeln. "Wenn jetzt nicht okay ist, willst
du dann ein Treffen wann anders? Morgen oder so?" Sie überlegte,
dass es rüberkommen könnte, als würde sie mit Salın nur interagieren wollen,
wenn sie so leidenschaftlich küssten wie letzte Woche. "Oder ist Jetzt ein
guter Zeitpunkt, aber für etwas anderes als das Anknüpfen an letzte Woche?"

Salın wischte sich über die Augen und wandte Jurin den Blick zu. Jurin
war nicht sicher, ob sie gerade geweint hatte. Ihre Augen waren
nicht rot. "Würdest du mich nach Hause bringen? Es ist eine gute halbe
Stunde zu Fuß."

"Mauk wartet auf mich." Jurin legte zwei Finger an die gerunzelten
Lippen.

"Verstehe", sagte Salın.

Jurin schüttelte den Kopf. "Tust du nicht. Das heißt halt, dass du noch
kurz warten musst, bis ich das abgeklärt und mich verabschiedet habe. Ich
bring dich Nachhause."

"Musst du wirklich nicht." Der Stress in Salıns Stimme steckte
Jurin fast an.

"Ich weiß." Jurin zuckte mit den Schultern. "Ich will aber."

Salın nickte schließlich. "Ich warte hier."

Weil Salın schon wieder so fröstelig aussah, reichte Jurin ihr ihre Jacke, bevor
sie überzeugt ins Antikuarium schritt und die Sache mit Mauk klärte. Es erleichterte
sie, dass Mauk ihr nicht jetzt noch weitere überraschende Beobachtungen
eröffnete, und auch, dass sie nicht enttäuscht schien. Sie würden sich vielleicht
in der Herberge treffen. Jurin würde ihr eine Nachricht schicken.

---

Bis zum Meer gingen sie schweigend nebeneinander her. Jurin musste fast
schon wieder grinsen, weil sie sich jetzt kaum einem Dating-Gefühl
ferner fühlen könnte. Sie fühlte sich nicht unbedingt wohl. Es wurde
Zeit, das zu kommunizieren, aber es würde noch einige Momente brauchen, bis
sie einen Ansatzpunkt finden würde.

"Es tut mir leid", kam Salın ihr vorweg.

"Keine gute Eröffnung der Sache", kommentierte Jurin.

"Was?!"

"Vielleicht interpretier ich auch zu viel darein: Du entschuldigst dich
bei mir für deine Stimmung?", mutmaßte Jurin.

Salın nickte. "Sollte ich nicht?"

"Nein, solltest du nicht", bestätigte Jurin. "Ich fühle mich gerade
auch nicht wohl, und es hat mit deiner Stimmung zu tun, aber du fühlst
halt einfach erstmal so wie du fühlst. Können wir damit irgendwie produktiv
umgehen?"

Mit einem mal grinste Salın so breit, dass Jurin es in der Dunkelheit
ausmachen konnte. "Du bist echt ... Weißt du? Bisher hat noch nie
eine Person gewagt, mir zu sagen, dass meine Stimmung einen negativen
Einfluss auf sie hätte. Und du bringst das einfach, und gleichzeitig
sagst du, es wäre erstmal grundsätzlich in Ordnung so. Das ist so
erleichternd! Ich habe mich in einem Gespräch darüber noch
nie so okay gefühlt." Salın stolperte über einen größeren Stein, und Jurin
fing sie reflexartig auf, sodass sie nicht hinfiel. "Also, das meintest
du doch, oder? Dass meine Gefühle grundsätzlich in Ordnung sind, wie
sie sind."

"Denk schon." Jurin ließ Salıns Arm nicht los, weil Salın sich von sich aus an
ihre Seite schmiegte. "Willst du ungefähr erzählen, was los ist? Oder
geht's mich nichts an?"

Salın legte den Kopf auf ihrer Schulter ab. "Ich möcht's nicht so genau
erzählen, ja?" Sie holte einen Moment Luft, aber fuhr ohne eine
Bestätigung von Jurin fort: "Ich gehe gerade einen Weg und zu einem Ort wo
negative Erinnerungen dranhängen. Das belastet mich."

"Aber du musst ihn gehen?", fragte Jurin.

Salın nickte, sodass ihre Spangen über Jurins Schultern rieben.

Jurin legte ihr den Arm um die Schulter. "Dann ist das für mich jetzt
genug geklärt."

---

Sie waren eine Weile auf diese Art langsam am Strand entlang gegangen, und
es war einfach schön gewesen: Der Wind, die Sterne und die Wolken, die sich
über den Himmel schoben und einige davon immer wieder verdeckten. Das
sachte Rauschen des Nachtmeers, das heute Nacht ungewöhnlich glatt war, und
die Leuchtquallen, die unter der Oberfläche weiß, rosa und bläulich
schimmerten. Aber nun blieben sie unvermittelt stehen. Salın drehte ihren
Körper vor Jurins, sodass sie wieder aneinander lehnten.

"Küsst du mich?", fragte sie.

Jurin streichelte ihr den Rücken hinauf und legte ihre Hand in Salıns
Nacken ab. Mit der anderen zog sie sie zärtlich noch dichter an sich heran.
Dann küsste sie sehr sanft die Stirn, und dann, nur flüchtig, aber sehr
langsam, die Lippen. Sie genoss das Lodern, das dabei zwischen ihnen
entstand, weil es bewusst viel zu wenig war.

"Sadistin", flüsterte Salın.

Jurin lächelte schmal. "Stört's dich?"

Salın schüttelte kaum merklich den Kopf.

Jurin küsste ihre Wimpern und zupfte ganz vorsichtig
daran.

"Ich wusste nicht, wie quälend sanft du sein kannst", hauchte
Salın.

Jurin schnaubte. "Wie auch! Du weißt eine Menge von mir nicht. Wir
kennen uns kaum."

"Möchtest du mich kennenlernen?", fragte Salın.

Wieder stimmte es Jurin skeptisch, auf einer bewussten Ebene, während
sich ihre emotionale Ebene einfach entspannt fallen ließ. "Ein
Stück."

"Fühlst du dich manchmal gezwungen, über das Stück, das du eigentlich
nur willst, hinauszugehen, weil die andere Seite das so sehr wünscht?", fragte
Salın.

Jurin zuckte wieder mit den Schultern. "Gezwungen nicht." Sie küsste
Salın noch einmal sanft auf die Stirn, weil sie gerade mit ihrem
Mund in der Gegend war und die Spannung nicht verlieren wollte. "Kommt
vor, dass ich Kompromisse eingehe, aber ich möchte eigentlich
keine weiteren, als ich schon habe."

"Ich auch nicht", flüsterte Salın. "Deshalb fühlt es sich gut an, dich
kennenzulernen. Weil du auch keine langfristige Beziehung willst."

Jurin entlud die Erleichterung, die sich in ihr breit machte, in einer
leidenschaftlichen Berührung der Stirn mit ihrem Mund und wanderte
über die linke Schläfe herab zur Wange. Sie hörte Salıns rascheren
Atem, spürte, wie sie beide in ein anderes Universum übergingen, wie
Salıns Körper eine ganz andere Form von Anspannung annahm. Aber
bevor sie Salıns Lippen küsste, hielt sie noch einmal inne. "Willst
du das hier?"

"Ja." Es war ein Hauchen ohne Zögern. "Küss mich! Bitte!"

Und Jurin küsste sie. Vorhin hätte sie nicht mit so einer Szene zwischen
ihnen gerechnet. Sie tauchte ein in all ihre Sinne, den Wind, das Rauschen
im Hier, die Hormone und anderen Botenstoffe in ihrem Körper, die Empfindungen
auf ihrer Haut, dem Atmen und Küssen, das eins war. Sie spürte Salıns zärtlichen,
kalten Hände in ihrem Rücken, wie sie über ihre nackte Haut unter ihrem
Top streichelten, Salıns Zungenspitze, die forsch und doch fragend zwischen ihre Lippen
fuhr.

Irgendwann, als sie genug hatten, lehnte Salın gegen Jurins Körper, den
Kopf mit den Spangen in der Frisur in Jurins Halsbeuge platziert. "Ich
habe ein bisschen Angst, dass du das nicht so unbeschreiblich schön
findest, wie ich", sagte sie. "Also, du musst das natürlich nicht so
toll finden, natürlich sind Gefühle auch einfach verschieden und so. Aber ...
aber ... also, was ich eigentlich wissen möchte: Langweilt es dich fast?"

Jurin lachte ob der obskuren Idee, es könnte sie langweilen. "Es
gehört schon mit zu meinen schönsten Kuss-Erfahrungen! Das kann ich
offen zugeben."

"Wow!", hauchte Salın. Und schließlich: "Ich muss weiter."

Sie waren angehalten, weil sie hier den Strand verließen, lernte
Jurin. Sie nahmen einen Pfad zwischen Dünen und Wald hinauf zu
einer Siedlung. Salın führte sie einige Straßen entlang, die sich auf
typische Art von Stadtstraßen unterschieden. Jurin konnte es nicht
genau festmachen, aber Stadt- und Dorfstraßen gaben ihr einfach sehr
unterschiedliche Gefühle. Schließlich blieben sie vor einem Haus stehen.

"Huch?", machte Salın.

"Hm?"

"Es sieht aus, als wäre niemand da, aber vielleicht irre ich mich."

"Sorgt es dich?", fragte Jurin.

Salın schüttelte den Kopf. "Nein."

"Wer sollte da sein?"

"Meine Eltern." Salın fiel ein klein wenig zurück in die Stimmung
von vorhin.

Einem Impuls folgend fragte Jurin: "Soll ich mit reinkommen?"

Salın blickte sie fast alarmiert an. "Ich habe dich nicht gebeten, mich zu
begleiten, um dich zu verführen und Zuhause mit dir Sex zu haben."

"Dafür wäre ich auch nicht mitgekommen." Jurin zögerte kurz, und
korrigierte sich: "Von mir aus nicht. Wenn du mich dafür gefragt
hättest, wäre ich überrascht gewesen, hätte mich aber schon auch
dafür entschieden. Aber jetzt frage ich dich, weil du auf mich so wirkst,
als könntest du brauchen, nicht allein da drin zu sein, was auch
immer dich da erwartet."

Salın nickte sachte. "Okay", flüsterte sie.

Jurin folgte ihr zur Haustür. Salın öffnete sie so langsam und leise, als würden
sie einbrechen. Es erinnerte Jurin wenig überraschend ans Spiel, in dessen Runden
sie pausenlos irgendwo eingebrochen war. Ein kühler Wind fuhr durchs Haus
und irgendwo weiter hintern schlug eine Tür.

Jurin blickte über Salıns Schulter hinweg in die Dunkelheit und machte eine
überraschend kleine Gestalt aus, -- ein vierbeiniges Tier? Sie fragte
sich, ob sie hätte Angst haben sollen.

"Klavin, was machst du denn hier?", flüsterte Salın.

Das Tier kam auf sie zu, dem Gang nach zu urteilen, ein Huftier, und berührte
Salıns Hand mit einem großen, flexibellippigen Maul.

"Du hast da ein Pferd im Flur", murmelte Jurin verwirrt. "Ein kleines Pferd."

Salın schob sich in die Wohnung und schloss hinter Jurin die Tür. Die Dunkelheit
umschloss sie nur einen Moment, dann zündete Salın eine Dochtlampe an der
Wand an. "Darf ich vorstellen? Das ist Klavin und das ist Jurin!"

Klavin beäugte Jurin skeptisch und gab
ein Schnauben von sich. Jurin verwarf den ersten Gedanken, ebenso mit
einem Schnauben zu antworten und sagte bloß ein leises "Moin".

"Kein Licht, keine Schuhe", murmelte Salın. "Falls meine Eltern doch da sind, darf ich
dich als mein Herzwesen ausgeben?"

"Wir können auch ausmachen, dass ich dein Herzwesen bin. Ich habe da kein
striktes Kriterium, ab wann ich die Bezeichnung verteile", raunte Jurin zurück. "Ansonsten,
ich mache jede Lüge mit, die du über mich behauptest. Ich kann spontanes Rollenspiel."

Salın zündete auf dem Weg durch den Flur in ein Wohn-Esszimmer noch
weitere Lampen an und führte das Pferd in den Garten. Dort kümmerte sie
sich um eine Futterstelle, während Jurin daneben stand. "Manchmal kriegen die
beiden die Hintertür auf, dann hat man mal so ein Pferd im Speisezimmer
oder Flur.", teilte sie mit.

Anschließend machte sie eine vollständige
Hausführung, bei der Jurin sich nicht sicher war, ob sie für sie gedacht war,
dafür, herauszufinden, ob sie wirklich alleine waren -- was ein etwas
unheimlicher Gedanke war, denn wer würde sich im eigenen Haus verstecken --, oder
dazu da war, diese vielen ollen Lampen anzuzünden. Jurin stellte fest,
dass diese Dochtlampen die einzigen Lampen im Haus waren.

"Schreiben deine Eltern dir keine Nachricht, wenn sie nicht kommen?", fragte Jurin. "Ist
es ein besorgniserregendes Zeichen, dass sie nicht da sind und du davon nichts
weißt?"

Salın schüttelte den Kopf. "Lona, das ist meine eine Mutter, verweigert sich Spinstrom und
Technik vollkommen. Deshalb würde sie vielleicht einen Brief per Post schreiben, aber
mir keine EM-Nachricht hinterlassen. Aber es kommt mal, selten, vor, dass sie doch
nicht kommen, ohne es anzukündigen, weil Lona sich festquatscht oder ihnen was
anderes dazwischen kommt."

Jurin hob eine Augenbraue so weit ihr Gesicht das erlaubte. "Nun, ich sollte
mich vielleicht nicht darüber erheben oder so. Ich habe mit Vergnügen die
Phasen im Spiel gespielt, in denen wir quasi Survival gemacht haben und
nichts zur Verfügung hatten. Aber ein Leben lang dem absprechen? Erscheint
mir nach einer künstlichen, massiven Barriere."

Salın nickte. "Willst du noch einen Tee?"

"Mit Kompoststövchen?" Jurin schnaubte. "Mauk, das ist der Mann, dier heute zu meinen Füßen
gehockt hat, ist ohne Lebensmitteldrucker großgeworden. Sie hat Kompoststövchen zum
Teekochen benutzt, daher kenne ich die. Aber das hatte andere Gründe als ein Absagen
von Technik."

"Mit Kompoststövchen, ja", sagte Salın.

Jurin konnte keine Regung bei ihr erkennen, die verraten hätte, wie sie
zu Jurins Kommentaren fühlte. "Tue ich dir gerade weh?"

Salın schüttelte den Kopf. "Ich bin eben damit großgeworden. Ich habe lange
geglaubt, es wäre was Tolles. Und nun ist es ein Gemisch aus Gefühlen. Ich
habe manchmal auch deine und fühle einfach nur die Absurdität des
Ganzen. Aber im Moment fühle ich blanke Wut."

Jurins Lächeln erlosch fast wie eine stromlose Kerze im Wind und sie sah
Salın ernst an. Sie konnte die Wut überhaupt nicht erkennen, aber
sie zögerte keine Sekunde, Salın zu glauben. Sie kannte inzwischen einige
Leute, bei denen sich Emotionen atypisch oder eben auch einfach nicht
zeigten. "Also, L ... wie hieß deine Mutter?"

"Lona."

"Lona hat das nicht nur für sich, sondern auch für dich entschieden?", erkundigte
sich Jurin.

Salın nickte. "Und für meine andere Mutter Marişa."

"Wie bist du unterrichtet worden?", fragte Jurin. "Zuhause? Oder gibt es Schulen, die
ohne jeglichen Spinstrom auskommen?"

"Gibt es. Ungefähr vier in ganz Maerdha." Salın seufzte. "Aber als ich dreizehn war,
war Lona nicht geheuer, wieviele Kinder doch irgendwo heimlich Lern-KIs als zusätzliche
Quelle hatten, hat mich von der Schule genommen und zwei Privatlehrkräfte
für mich gefunden."

"Wow." Jurin ließ sich ernüchtert auf einem Stuhl nieder. "Entschuldige, wenn ich das
so sage, aber was für eine beschissene Situation!"

"Möchtest du Tee?", fragte Salın erneut.

"Du wolltest eigentlich nicht drüber reden?", mutmaßte Jurin.

Salın schnaubte freundlich und schüttelte den Kopf. "Ich würde gerade gern
nicht darüber reden."

"Ich nehme einen Tee, und wenn du dieses Klavier spielen kannst und das Haus schalldicht
genug ist, ein Nachtkonzert." Jurin stellte fest, dass sie sich einen
ganz schön bequemen Lehnstuhl ausgesucht hatte und streckte sich aus. "Wenn's
nicht zu intim ist."

---

"Ich kann dir was vorspielen, aber nicht als ein dir gewidmetes Konzert auf
so eine kinky Weise wie die Show", sagte Salın, als sie mit zwei Teetassen
auf einem Tablett wiederkam.

"In Ordnung." Jurin legte ihre kalten Finger an die dampfende, kleine
Porzellan-Tasse. "Warte kurz."

Salın hatte sich gerade auf den Klavierhocker gesetzt und wandte sich nun doch
wieder Jurin zu.

"Willst du wirklich für mich etwas spielen, oder hast du nur zugesagt, weil
ich gefragt habe?", erkundigte sich Jurin. "Denn, wenn du doch nicht möchtest,
könntest du auch 'nein' sagen und wir holen das Feiern vom letzten Mal
nach."

Salın klappte den Deckel über der Tastatur auf und verteilte ihre
Finger über die Tasten. "Ich probiere, wie ich mich dabei fühle, und
breche vielleicht ab."

"Exzellent!" Jurin hob die Tasse an die Lippen und ein herber
Grüntee-Geruch stieg ihr in die Nase.

---

Eine halbe Stunde später lagen sie im Bett auf dem Dachboden
aneinandergeschmiegt. Jurin verspürte keine Lust, Salın wieder
zu küssen, und das schien auf Gegenseitigkeit zu beruhen. Jurin
streichelte zärtlich durch Salıns nun unbespangtes Haar. Salıns
Kopf lag dabei halb auf ihrer Schulter, halb auf ihrem Oberarm. Ihrer
beider Atem war ruhig.

Salın hatte tatsächlich zwei Klavierstücke gespielt, ein
eingeübtes, das sie aber nicht bis zum Ende spielen konnte,
und ein selbst komponiertes. Ersteres war eher dramatisch
und letzteres auf eine interessante Art zugleich leicht und
gruselig. Jurin hatten beide gefallen und das hatte sie auch
gesagt. "Ich habe wieder alles mit dir sehr genossen heute", teilte
sie mit.

"Bis auf meine Stimmung zwischendurch", korrigierte Salın.

"Jein. Denn ich mochte unseren klärenden Austausch danach, und
das wäre so nicht passiert, wenn du die Stimmung nicht
gehabt hättest", sagte Jurin. "Auch Auflösungen
können schön sein."

Salın gluckste. "Ich genieße dich auch sehr. Du tust mir gut."

"Das hat mir tatsächlich noch nie jemand so gesagt, glaube ich." Jurin
runzelte die Stirn und zuckte schließlich die Schultern. "Schön, denke
ich."

"Mochtest du, eine Show gewidmet zu bekommen?", fragte Salın, und einen
Moment klang dabei ihr euphorischer Bühnen-Übermut wieder durch.

"Sehr!" Jurin stupste mit der Nase gegen ihre Stirn.

Es war ein eigenartiges, aber deshalb nicht schlechtes Gefühl mit Salın hier
zu liegen. Es fühlte sich wie ein intimer Ort an, um sich im Arm zu
haben. Jurin mochte das Spiel auch, weil sie in manchen, nervenkitzeligen
Situationen an Orten schlafen oder zur Ruhe kommen musste, die nicht sicher waren, aber
es war immer noch eine Unsicherheit, die Teil eines Spiels war. Dieser
Ort fühlte sich an, als könnte er jeden Augenblick real in Flammen ausbrechen.

Und hier lag sie, mit Salın im Arm und wartete auf den
Weltuntergang. Wenn das nicht romantisch war ...

Aber das war es eben nicht.
