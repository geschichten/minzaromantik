Ein aromantischer Blumenstrauß
==============================

\Beitext{Jurin}

Jurin lehnte allein an der kühlen Mauer des Antikuariums, rieb mit den Fingern über eines
der Minzblätter und roch daran. Sie mochte den Geruch, das konnte sie nicht
anders sagen. Sie atmete die kühle Sommerluft, versuchte, Abstand zu gewinnen von der
BDSM-Party, die im Innern der Mauern im Gange war.

Linoschka hatte ihr vorhin den Strauß Minze überreicht mit den Worten, es wäre ein
aromantischer Blumenstrauß für Jurin. Es war lieb gemeint gewesen. Vielleicht
hätte Jurin mit mehr Wohlwollen auf die Geste reagieren können. Sie mochte Linoschka
eigentlich. Aber sie hatte echt nicht den Nerv dazu gehabt. Vielleicht auch gerade
deshalb nicht, weil sie etwas anderes von ihr erwartet hätte.

"Das es praktisch ist und ich Minze mag, macht das nicht unromantisch", hatte Jurin
gesagt. Sie hatten so oft darüber gesprochen, dass Jurin aro war und sie solcherlei
Gesten stressten.

"Vielleicht. Aber es ist unromantisch gemeint", hatte Linoschka gemeint.

Und dann hatte Jurin etwas getan, wofür nun Scham durch ihren Körper flutete. Schamempfinden
war eigentlich sehr untypisch für sie. Sie hatte die nächstbeste Person, die an ihnen vorbeihuschte,
kurz aufgehalten. Ein Dunkelelb in einem blasstürkisen und weißen Kostüm aus Spitze und
Tüll. "Magst du einen Stängel Minze haben? Könnte gut zum Kostüm passen."

Die Person war stehen geblieben und hatte den dargebotenen Stängel aus dem Strauß genommen. "Ist
das eine Bitte um ein One Night Stand oder sowas?" Das Gesicht ein verspieltes
Grinsen.

Jurin hatte den Kopf geschüttelt. "Einfach für dich zur Freude, zum
Mitnehmen und Weitergehen."

"Dann ..." -- ein Hadern, ein längerer Blick -- "gehe ich wohl weiter." Aber
vor dem Abtreten, doch noch ein Lächeln und ein verzückter Knicks. "Danke!"

Und als Jurin den Blick Linoschka wieder zugewandt hatte, wusste sie, dass sie
Linoschka verletzt hatte. Linoschka schwieg. Als Jurin explizit fragte, ob es sie
verletzt habe, nickte sie bloß.

Jurin hätte ihr gern mehr Dinge an den Kopf geknallt. Etwa, dass diese Reaktion exakt
der Beweis war, dass die Geste vollkommen romantisch war. Aber sie war sich
nicht sicher genug, ob ihre Gewissheit nicht doch ihrer Wut
entsprang. Vielleicht hatte es für Linoschka was mit Freundschaft zu
tun. Es war Jurin egal. Der Strauß war speziell für Jurin
bestimmt gewesen, als ein bindendes Element zwischen ihnen zweien. Sonst würde es
nicht verletzen, wenn Jurin die Minze weiterverteilte, scheinbar auf so bedeutungslose
Weise. Scheinbar. Jurin fand das alles andere als bedeutungslos. Sie hatte
sich einfach so verhalten, wie sie sich verhalten würde, wenn niemand ihr Verhalten
in irgendwelche Muster einsortieren würde, in denen sie nie Zuhause sein könnte.

Jurin zupfte ein Blatt Minze aus dem Strauß, um darauf herumzukauen. Es war gute Minze,
keine Frage. Minze aus dem Garten der WG, zu der Linoschka gehörte. Jurins Mundwinkel
zogen sich wie automatisch zu einem breiten Lächeln bei den Erinnerungen an die WG. Sollte sie
sich je niederlassen, was nie der Fall sein würde, aber wenn, dann dort.

Sie kehrte zurück ins Jetzthier, wo sich der Abend in keiner Weise so entwickelte,
wie Jurin sich das ausgemalt hatte. Allerdings war es nicht auf jeder Ebene
eine schlechtere Alternativ-Entwicklung. Jurin war hier mit Linoschka verabredet,
aber auch Mauk war hier, das hatte sie vorher gewusst, und hätte damit gerechnet, ihre Aufmerksamkeit
zwischen diesen beiden verschiedenen Personen teilen zu müssen. Das wäre
herausfordernd geworden. Mauk war ein submissives Brat, -- eine Person, die mit ihrer
Art so sehr einforderte, dass Jurin sie unterwarf, dass Jurin es schwergefallen
wäre, dem Sog zu widerstehen.

Linoschka war zurückhaltender und unsicherer. Und Jurin hätte ihr gern einen Abend
auf dieser BDSM-Party ermöglicht, die sie in die Szene einführte und ihr zeigte, was
es so gab.

Jurin hätte nicht damit gerechnet, dass beide aufeinandertreffen und dieses ungleiche
Paar miteinander spielen würde. Eigentlich war auch Linoschka eher auf dem submissiven
Spektrum unterwegs. Das war nicht die beste Voraussetzung für eine Spielbasis zu
zweit. Aber na gut. Nun rauften sie sich vielleicht
ums Untenliegen. Oder sie experimentierten herum oder unterhielten sich einfach.

Jurin war allein zurückgeblieben, und wenn sie ehrlich mit sich war, war das
eine positive Entwicklung des Abends. Sie stellte fest, dass sie gar nicht für sich
auf die Retro-BDSM-Party gegangen war. Ihr gefiel das Antikuarium. Das Gemäuer roch alt
und steinig, und das war es auch. Sie mochte grundsätzlich BDSM und kinky Spiel. Aber
sie fühlte sich eigentlich zu müde dafür, heute irgendwen anzuleiten.

Sie aß ein weiteres Minzblatt, kaute gründlich darauf herum und seufzte tief, weil es
guttat und erleichterte. Den Rest des Straußes steckte sie in ihren Beutel, der zu
ihren Füßen ruhte. Er war schwarz und grob aus stabilem Stoff gewebt und fühlte sich haptisch sehr gut an. Aber
er hatte kein dezidiertes Minzstraußfach, und auf diese Weise würde der Strauß in der Herberge
definitiv nicht mehr schön aussehen. Und das war der erste Gedanke gewesen, als sie
den Strauß überreicht bekommen hatte: Wie soll ich den denn transportieren?

"Heiß, heiß, heiß da drinnen!" Die Stimme
der Person, die gerade durch die Tür ins freie getreten war, klang
fast so hektisch, wie wenn sich jemand verbrannt hätte.

Jurin richtete sich aus der Hocke auf und betrachte den Dunkelelben, der mit kleinen Trippelschritten
barfüßig auf dem Pflaster herumtapste und die Arme im Wind schwang. "Brr, aber hier
ist es dann doch wieder kalt! Es ist nie richtig!"

Jurin holte eine schwarze, taschenreiche Jeansjacke aus dem Beutel und reichte sie
wortlos der fremden Person. Ihre Blicke fanden sich.

"Du warst die Person mit der Minze!" Der Dunkelelb schlüpfte in die Jacke, aber bewegte
sich dabei geschickt auf eine Weise, dass Jurin ihr dabei half. "Soll ich die auch
mitnehmen und gehen?"

Jurin kicherte lauthals. "Die würd ich schon ganz gern wiederhaben."

Sie blickten sich einige Momente einfach grinsend an. Der Dunkelelb strich die Jacke
erfolglos glatt. Es war keine Jacke, die zum Glattsein gemacht war. "Das ist jetzt
ein ziemlich gewagter Stil mit so einer Jacke zu diesem Kostüm."

Jurin wiegte den Kopf und musste schon wieder lachen. "Das kann ich nicht leugnen!", sagte
sie. "Ich fänd sie selbst zu meinem schon gewagt. Deshalb trag ich sie nur
draußen." Sie strich sich über das schwarze,
ärmellose Oberteil. Es war eine Kreuzung aus Korsett und Mieder aus mattem, schwarzem Kunstleder,
auf das mit ein kleinen Perlen und glänzender Beschichtung Muster
aufgebracht waren.

Ihr Gegenüber trug Rüschenstrumpfbänder um die baren Fußgelenke, das türkis-weiß bildete
einen schönen Kontrast zur dunkelgrauen Haut. Der ebenfalls rüschige, mit Schleifen besetzte Spitzenrock
endete Oberschenkelmitte, obwohl der Stoff sicher Raum gehabt hätte, bis zu
den Knien zu reichen. Aber ein bauschiger Unterrock sorgte dafür, dass der darüber abstand. Das Kostüm machte den
Eindruck, wenn Jurin es richtig einordnete, als gehörte er in Shows aus dem Unterhaltungstheater
der Bonbonzeit. "Habe ich dich vorhin auf der Bühne gesehen?", fragte sie.

"Weißt du, ich kann natürlich nicht sagen, was du gesehen hast, aber wenn du einen
Engel auf der Bühne gesehen hast, dann war das wohl sicher ich!" Der Dunkelelb machte
hopsende Tanzschritte, warf abwechselnd die Beine und damit den Rock hoch und verbeugte sich
schließlich.

Jurin lachte. "Schnieker Tanzsstiel. Genau, das habe ich im Augenwinkel gesehen!"

"Mein Höschen?", fragte der Dunkelelb.

"Du bist reichlich provokant!" Jurin hörte nicht auf zu lächeln, um ihrem
Gegenüber die Wahl zu lassen, es als Kritik aufzufassen oder als Beschreibung
eines Fakts.

"Stört's dich?", fragte die Person, das Lächeln frech erwidernd.

Sie standen sich in angemessenem Abstand für eine Alltags-Unterhaltung gegenüber, oder nicht? Jurin
fragte sich, ob die Person die Nähe bewusst eine Nuance darunter wählte. Sie schüttelte sachte
den Kopf. Und die Person trat noch einen Schritt näher auf sie zu. "Steht auf meiner Stirn irgendwie
geschrieben, dass ich auf Personen wie dich stehen könnte?", fragte Jurin.

Der Dunkelelb kicherte. "Ich hatte für einen kurzen Moment vergessen, dass es auf der Welt auch
nicht-lesbische Wesen geben könnte", sagte er und verfiel in einen geradezu faszinierten
Tonfall. "Damit will ich jetzt nichtmal behaupten, du wärest
lesbisch oder so. Ich habe es einfach gerade komplett vergessen! Das finde ich witzig. Verstehst du?"

Jurin kicherte mit und schüttelte den Kopf. "Ich hoffe, es war ein schöner Moment!"

"Ich bin Salın. Pronomen 'sie/ihr/ihr/sie'", sagte Salın. "Und ich finde dich durchaus heiß genug dafür, dass ich deine
freundlich verliehene Jacke wieder ausziehen müsste, wenn ich dir noch näher käme."

"Jurin. Auch 'sie'." Sie blickte Salın einen Moment in die Augen, die fast hinter
den vielen Lachfältchen verborgen waren. "Also fragst du mich nun nach einem One Night Stand? Oder einer kinky
Version davon?"

Ohne die Frage zu beantworten, trat Salın noch einen Schritt näher und streckte die Hand aus. Jurin
wusste noch nicht, was diese berühren wollte -- vielleicht die Mauer hinter ihr --, aber ihre eigene schloss
sich fast schon im Reflex um das Handgelenk der anderen. "Du fragst, bevor du mich anfasst!"

"Du bist dominant?"

"Meistens spiele ich dominant, ja."

"Kann mir gar nicht denken, wieso." Salın grinste und nahm dabei die Unterlippe zwischen
die Zähne. "Du wirkst so zurückhaltend und, naja, als könntest du dich nie
so richtig durchsetzen, weißt du?"

Jurin hob eine Augenbraue. Sie war gewohnt, dominant eingeschätzt zu werden. Man schrieb ihr
eine natürliche Dominanz zu, von der sie nicht so genau wusste, was das heißen sollte, aber
gerade war ihr noch schleierhafter als sonst, wie Salın das so schnell vermutete.

"Es war ein oller Scherz!" Salın lachte und bewegte das Handgelenk in Jurins Griff so
gut das ging. "Ich fand deinen Zopf interessant und wollte ihn mir genauer ansehen, wenn ich
darf."

Jurin ließ Salıns Hand los, löste den eigenen Rücken kurz von der Wand und zog den langen, schwarzen
Zopf hervor. Auf Salıns "Darf ich?" nickte sie dieses Mal.

Salın nahm den Zopf in die Hand und streichelte sanft hindurch.

Jurin konnte wahrlich nicht leugnen, das sanfte Ziepen auf der Kopfhaut zu mögen.

"Hier ist so ein Übergang!", stellte Salın richtig fest. "Der ist mir vorhin schon
aufgefallen. Bis hier, etwa auf Schulterhöhe
ist der Zopf dick, und ab da dünnt er massiv aus! Woran liegt das?"

"Ich hatte vor einem halben Jahr noch über die Hälfte meines Schädels
rasiert", gab Jurin zu.

"Welche? Nein, warte! Sag es nicht! Darf ich forschen?" Salın klang so aufgeregt, fast wie
ein Kind, das einen interessanten Stein aus dem Boden ausgebuddelt hatte.

Jurin lachte etwas verunsichert, aber zuckte schließlich mit den Schultern. "Warum nicht."

Salın trat noch dichter an sie heran, sodass sich ihre Körper fast berührten. Sie
war einen halben Kopf kleiner als Jurin, aber
vielleicht lag das nur daran, dass Jurins Stiefel eine recht hohe Sohle hatten. Sie löste Jurins
Pferdeschwanz auf, gab sich Mühe, dass es nicht zu sehr ziepte, als sie das Zopfband
aus dem Haar zog.

Jurin merkte, wie ihr Atem entspannte. Sie schloss die Augen und sog den Geruch von Salıns
Haar oder Gesicht ein, das ihrem so nahe war. Und die Nachtluft in Minzter, die erfrischende
Kühle des Abends, durchmengt mit Salz, weil das Nachtmeer nicht weit war.

Salın untersuchte ihr Haar gründlich. Die kürzeren Teile des Haars sortierte sie nach
vorn über Jurins Gesicht, wo es kitzelte und mit ihrem Atem sachte mitwehte, und
hinten, bis sie tatsächlich ungefähr ausmachen konnte, welcher Teil des Schädels
mal rasiert gewesen war. "Du bist Jurin Raute!"

"Gute Detektei-Arbeit", meinte Jurin trocken. "Die bin ich wohl."

"Ich hatte schon so ein Gefühl, dass du das bist, aber ich war mir nicht
sicher." Salın strich das ganze Haar wieder nach hinten, sodass es Jurins Rücken herabhing und
ihren Nacken streichelte. Sie ließ ihre Finger darin eingefädelt. "Warum trägst du
nicht mehr die Haarraute?"

"Ich werde damit immer mit meiner Persona Ærenik in Verbindung gebracht, die im Zusammenhang
mit dem Spiel berühmt war", erklärte Jurin. "Ich habe mich in ein paar Punkten weiterentwickelt." Sie
dachte wieder daran, wie sie Linoschka verletzt hatte. "Glaub ich zumindest. Und ich nehme nicht
mehr teil. Ich wollte Dinge hinter mir lassen und habe mich entschieden, das Haar deshalb im ganzen
lang wachsen zu lassen."

Salın strich zärtlich durch ihr Haar, die Finger bis auf die Kopfhaut eingefädelt. "Das verstehe
ich", sagte sie. "Es ist schön so! Du bist so schön!"

Jurin kicherte. "Du aber auch." Sie hatte es nicht erwidern wollen. Es war ihr so rausgerutscht. Es
war, was man dann so antwortet eben. Aber sie empfand es durchaus so. "Wenn du nicht
so schnell mit Komplimenten wärest, hätte ich es dir vielleicht auch direkt und nicht als
Erwiderung gesagt."

Salın lächelte und näherte sich den letzten Schritt, der sie trennte, sodass
ihre Körper aneinander lehnten. "Ich würde dich gern küssen. Einfach so!"

"Nur zu!", ludt Jurin ein.

Die kühlen Finger taten so gut auf der Haut. Salın strich ihr über die Seiten des
Gesichts. Sie küsste als erstes die weichen Stelle der Wangen, die so nachgiebiges
Gewebe hatten, mittig und direkt neben der Nase oberhalb des Mundes. Sie zupfte
daran und bewegte die Lippen darauf, die Weiche spürend. Als sie ihren
Mund auf Jurins legte, schloss Jurin die Arme um sie. Sie fragte meistens
lieber, bevor sie ein Spiel oder irgendeine Art Session anfing, wo die Grenzen
waren. Etwa, ob Leute streicheln überhaupt mochten. Aber in diesem Spiel leitete
bisher Salın auf eine so interessant forsche Weise, dass Jurin sie fast eher als
die dominante Person von ihnen eingeordnet hätte. Und dann wiederum auch gar nicht. Jurin hatte
die Stiefel, Salın stand auf nackten Zehenspitzen, um sie überhaupt zu erreichen. War
es vielleicht vanilla^[Vanilla heißt in diesem Zusammenhang, ohne kinky oder
BDSM-Komponente. Also hier zum Beispiel ohne Machtgefälle. Bei Computerspielen
beschreibt vanilla oft das Spiel ohne User-Addons oder -Additions. Diese
andere Bedeutung hat mir geholfen, ein Gefühl für den Begriff zu bekommen, deshalb
gebe ich sie mit an.]?

Jurin kehrte zurück ins Hierjetzt, als sie merkte, dass sie von sich aus eine
Zunge zwischen Salıns Lippen geschoben hatte. Sie hatte sich fallen gelassen. Das
war lange her. Sie löste die Lippen, küsste sie wieder, spürte die Hitze in sich
aufsteigen, kostete die Berührungen aus,
fühlte den Atem jener Person, die sie küsste und doch kaum kannte, auf ihrem
Gesicht. Es passte selten so gut für Jurin, was küssen betraf.

Jurin mochte es, fremde Leute zu küssen. Es machte alles einfacher. Keine
Bindung, keine oder kaum Erwartungen. Keine persönlichen Gefühle, die irgendwie
im Weg waren, das Ganze nur für die Sinnlichkeit zu machen, nur zum Fühlen. Und ja, wenn
Salın sich anbieten würde, Jurin würde zu einem One Night Stand sicher nicht
'nein' sagen.

Salın hörte zu küssen auf und hielt inne, als Jurins Hand sich beinahe in ihrem
Rücken in den Rockbund verirrt hatte. "Du willst mehr als küssen, oder?", fragte
sie.

Jurin erkannte, dass es eine halbe Absage war. Mindestens eine halbe. "Ich wäre
nicht abgeneigt gewesen, aber nur küssen ist für mich fein." Sie versuchte
ein Lächeln, und es war seltsam unsicher. Das gefiel ihr nicht. Es war nicht
unsicher, weil sie eine Grenze kommuniziert bekommen hatte, sondern weil sie
in einem Küssen-Mindset war, das sich nicht gut für versichernde Konversationen
eignete. "Streich das 'nur'."

"Ich möchte heute lieber nicht mehr als küssen", sagte Salın.

Jurin nahm die Hand aus ihrem Lendenwirbelbereich und sortierte sie stattdessen
in Salıns Nacken. "Aber weiterküssen schon?"

"Gleich." Eine Freude huschte über Salıns Gesicht, sie löste sich ganz aus der
Umarmung und tanzte über die Steine. "Ich hab geschafft, 'nein' zu sagen!"

"Das hast du!" Jurin lachte mit! Und machte sich eine innere Notiz, dass Salın gegebenenfalls
manchmal Probleme mit dem Nein-Sagen hatte. "Exzellent!"

Salın tappste zurück, sodass sie Jurin wieder gegenüberstand und sie sie
hätte leicht berühren können. "Aber du leidest darunter!"

"Nein?!" Jurin hob eine Braue.

Salın wirkte einen Moment, als würde all ihre Energie entweichen. Dann trat irgendwie
wieder ein Leuchten in ihr Gesicht, als wäre in ihr ein zartes neues Flämmchen entzündet
worden, das sich aber noch nicht ganz sicher in dieser Welt fühlte. Sie
trat auf Jurin zu und lehnte sich seitlich an ihren Körper, sodass
ihr Kopf in Jurins rechter Halsbeuge zum Liegen kam. "Darf ich das überhaupt?"

Jurin legte ihr den Arm um die Schultern und drehte den Kopf, sodass ihr Mund auf
Salıns Stirn zum Liegen kam. "Darfst du." Mauk hätte sie nun dafür gescholten und
es hätte ihnen beiden gefallen. Aber Salıns Vorzüge kannte sie noch so gar
nicht. Das war äußerst interessant!

"Dein Mund schmeckt nach Pfefferminze", bemerkte Salın. "Ich hab die Blätter
von meinem Stängel vorhin in meiner Pause alle aufgegessen. Es ist sehr
gute Pfefferminze! Obwohl, vielleicht fast ein bisschen mild."

Jurin gluckste. "Ich könnte dich mit mehr Blättern füttern."

"Wie so ein Schaf!" Salın stupste Jurins Kinnkante mit der Nase an. Sie
war schon wieder so verheißungsvoll dicht mit ihrem Mund an Jurins Gesicht. "Willst
du mit mir ein Minzblatt essen? Also so ... Kennst du, wie Paare Spagetti essen, bis
sie sich küssen?"

Jurin schnaubte. "Du findest die Minze im Beutel zu meinen Füßen." Sie widerstand
dem Drang, eine Hand in Salıns Nacken zu legen, um sie zum Bücken anzuleiten.

Salın sah sie mit einem so verschmitzten Grinsen an, dass Jurin sich fast sicher
war, dass Salın eine Ahnung hatte, was in Jurin vorging. Sie ging in die Hocke,
und strich dabei mit ihrem Körper an Jurins entlang, sodass es fast ein
erotisches Streicheln war. Vielleicht doch nicht nur fast. Sie stützte sich mit einer Hand auf Jurins linker
Stiefelspitze ab, als wäre es ein Versehen, als sie den Beutel durchsuchte und zügig die Minzblätter
fand. Sie sahen jetzt schon ein wenig schlapp aus. Auf die gleiche, streichelnd reibende
Art stand sie auch wieder auf, aber dabei rutschte ihr Rock etwas herunter,
der mit einem breiten Gummiband ihre Hüfte umschloss. Sie zuppelte ihn
zurecht, ohne die Körperberührung zu lösen, und versäumte dabei nicht,
den Saum des Rocks so weit hochgezogen zwischen ihren Körpern einzuklemmen,
dass ihr nackter Oberschenkel auf Jurins bestrumpfhosten oberhalb der Stiefel
lag.

"So viel zu nicht mehr als küssen", kommentierte Jurin. "Ich behalt meine Finger bei
mir, keine Sorge. Aber es ist schon arg provokativ und erotisch, was du da tust."

"Stört's dich?", wiederholte Salın die Frage von vorhin.

Jurin schüttelte abermals den Kopf und formte den Mund zu einem schmalen
Lächeln. "Interessante Dynamik. Sowas hatte ich noch nicht. Ich bin
immer offen für Neues."

Salın zupfte ein Blatt der Minze vom Stängel und klemmte den äußersten Teil des Blattes
zwischen den Lippen ein. Ihr Blick, mit dem sie Jurin fesselte, war fast bettelnd,
unterwürfig, und trotzdem voll bei sich, fand Jurin. Genau das meinte sie mit
'interessante Dynamik'. Und sie liebte dieses Spiel.

Sie schob nun doch ihre Hand in Salıns Nacken, wo sie das weiche Haar erfühlte. Salın
hatte etwa schulterlanges, hellgraues Haar, --  wobei, eigentlich verschiedenlanges. Es waren
sortierte Locken in einem Stufenschnitt mit Seitenscheitel, die mit Spangen in einem
Bonbon-Zeit-Look fixiert waren.

Jurin streichelte vorsichtig durch das Nackenhaar, in der Hoffnung, die Frisur nicht
zu sehr in Mitleidenschaft zu ziehen. Aber auf der anderen Seite hatte diese Frisur an
Salın auch auf der Bühne Räder geschlagen und war heile geblieben. Sie zog Salın noch ein wenig enger
an ihre Seite, bevor sie das Minzblatt von der anderen Seite in den Mund nahm. Der
Geschmack der Minze hätte sie nicht überraschen sollen, aber er war so intensiv und
fühlte sich gut an. Sie überlegte, ob sie Salın frech das Minzblatt wegzupfen sollte, aber
da hatte diese schon abgebissen und zog ein wenig mehr davon in ihren Mund. Und als Jurin
das selbe tat, berührten sich ihre Lippen erneut.

Jurin durchfloss ein heißes Gefühl. Sie schob den Gedanken weg, dass es interessant
war, dass so ein Minzblatt als Eröffnung diese Hitze in ihr auslösen konnte, und ließ sich einfach
wieder fallen. Sie küsste Salın gierig und verlangend und merkte, wie sich Salıns Atem
und Körperhaltung änderte. Wie das Küssen Atemzügen oder Wellen gleich wogte, und
das ganz passend war, weil sie in einem Meer aus diesen Empfindungen badeten.

Jurin dachte nichtmal daran, dass sie vorhin zu einem One Night Stand nicht
'nein' gesagt hätte. Und ihre Hände waren nichtmal gewillt, auf Wanderschaft
zu gehen. 'Nur' küssen war eh einfach das beste gerade.

Als sie aufhörte, fühlte sie in sich eine Art tiefer Befriedigung. Salın lehnte
in ihren Armen und wirkte ebenfalls zufrieden und ein wenig erschöpft. Sie
hatte die Augen geschlossen und atmete ruhig. Jurin hatte das Bedürfnis zu
fragen, wie es für sie war, aber vielleicht nicht jetzt, weil das Schweigen auch
so schön war.

Salın löste Jurins einen Arm von sich. Den anderen beließ sie um ihre Schulter gelegt. Den
nun gelösten streichelte sie aber sanft hinauf und hinab, bis ihr Blick auf dem kleinen Tattoo
auf Jurins Handgelenk haften blieb. Sie hob es an und küsste das Tattoo.

"Wenn eine Stelle meines Körpers wirklich oft geküsst worden ist, dann die", bemerkte
Jurin. "Das habe ich nicht einberechnet, als ich mir das hab stechen lassen."

"Magst du es nicht?", fragte Salın. "Also, dort geküsst werden. Obwohl, also, ich hoffe,
dass du das Tattoo magst, aber vielleicht sollte ich auch danach fragen."

"Ich mag das Tattoo. Es hat was mit meiner Herkunft zutun." Es waren zwei einfache
Bögen, so wie Kinder oft Vögel skizzierten. Jurin seufzte leicht. "Eigentlich
mag ich nicht gern dort geküsst werden. Zumindest nicht solange, wenn Leute die Bedeutung
nicht kennen. Ich kann nicht gut erklären, warum."

Salıns Körper verkrampfte sich kurz, wieder nur einen Moment. Dann atmete sie
langsam und kontrolliert ein und aus. "Es tut mir leid."

"Schon gut. Es ist auch nicht so schlimm", sagte Jurin. Salın hielt die
ganze Zeit weiter ihr Handgelenk in der Hand und betrachtete die Linien. "Mehr
so wie, hm, wenn ich halt ein Geräusch nicht mag, was kurz da war. Es schadet mir
nicht langfristig, ist nur eben mal kurz so meh."

Salın nickte und küsste stattdessen die oberen Gelenke von Jurins Fingern. "Ich
weiß, was das Tattoo bedeutet. Also grundsätzlich weiß ich das. Natürlich
weiß ich nicht, wie genau du dazu fühlst. Aber ich habe auch mal von dir ein
Interview zu deinem Windschwinge-Dasein gesehen."

Jurin lächelte und streichelte ihr mit der anderen Hand über die Schulter. "Damit
ich mich wohlfühle damit, dort geküsst zu werden, reicht mir das nicht. Aber es
freut mich trotzdem! Hattest du mit dem Spiel zu tun, oder wie bist du darauf
gekommen, das Interview von mir anzusehen?"

"Ich mag Retro-Spiele!" Salın blickte endlich vom Tattoo
weg und auf in Jurins Gesicht. "Du hattest
in einer bestimmten Runde teilgenommen, in der es um sowas in etwa ging."

"Sehr entfernt."

"Außerdem habe ich immer so Schmetterlinge im Bauch, wenn ich dir begegne oder dich
sehe." Salıns Gesicht nahm einen sehr unschuldig wirkenden Ausdruck an.

Jurin seufzte schwer und nur mit großer Mühe nicht äußerst genervt. "Ich bin aro. Ich
bin nicht zu haben für irgendwas Romantisches, zumindest nicht für Längerfristiges. Und
es ist echt anstrengend, dauernd Körbe verteilen zu müssen und zu enttäuschen."

Der Ausdruck verschwand aus Salıns Gesicht und es wurde stattdessen sachte ernst. "Stört
dich dann bereits das Schmetterlingsgefühl? Oder nur die Erwartungen zu Bindungen? Ich
habe nämlich nur ersteres. Wenn ich nicht dich ansehe, sondern eine andere umwerfende Person,
dann schwärme ich für die und hab dich völlig vergessen."

Jurin konnte nicht anders als zu kichern. "Komplett vergessen! Und das sagst du mir einfach
ins Gesicht!" Sie küsste Salın auf die Stirn. "Ich feiere dich sehr dafür."

"Also ist das okay für dich?", fragte Salın. "Also, dass ich unter den Umständen Schmetterlinge
fühle?"

Jurin zuckte mit den Schultern. "Ich habe kein Gefühl für Schmetterlinge. Aber wenn es nicht
mit Erwartungen verknüpft ist, dann ist das wohl okay."

"Hm", machte Salın. "Wahrscheinlich ist es ein schlechter Zeitpunkt, um zu fragen, ob du
Lust hättest, nächsten Morntag Abend wieder ins Antikuarium zu kommen. Es findet eine
Bonbon-Show statt, in der ich mittanze. Und ich hätte Lust, für dich zu tanzen."

"Als romantische Geste?" Jurin hob eine Augenbraue.

Salın aber schüttelte den Kopf. "Als kinky Geste. Ich wollte schon immer für eine
bestimmte Person eine Show tanzen und singen. Wie beschreibe ich das am besten? Ich
meine, ich möchte zu deiner Unterhaltung zur Verfügung stehen."

"Der Zeitpunkt der Frage macht mich tatsächlich skeptisch. Warum ich?", fragte Jurin.

"Jedenfalls nicht, weil ich mit dir eine romantische Beziehung will", antwortete Salın. "Der
Zeitpunkt ist, weil ich gleich rein muss. Ich mache Bühnenbeleuchtung für die nächste
Show. Ich frage dich, weil ... also, weil ich dich noch nicht ausgekostet habe."

Jurin schnaubte abermals. "Na gut, ich komme. Aber verbrenn dich nicht an mir. Klar?"

Salın rückte noch einmal sehr dicht an Jurin heran und küsste ihre Wange. "Wenn Dinge
vorbeigehen, gehen sie vorbei. Ich genieße den Augenblick. Manchmal plane ich einen
Augenblick, aber wenn ein anderer kommt, ist das genauso schön. Mach dir keine Sorgen."

---

Später in der Herberge dachte Jurin über den Tag nach. Sie konnte nicht leugnen, auch
Salın noch nicht ausgekostet zu haben. Zudem liebte sie das Abenteuer: Sie war noch
nie in der Rolle gewesen, dass eine andere Person ihr eine Show gewidmet hätte. Sie
fragte sich, was der Unterschied zur Minze war, die Linoschka ihr geschenkt hatte. Vielleicht
tatsächlich, dass sie Salın eigentlich ohne Zögern abkaufte, dass es eine kinky und keine
romantische oder Bindungs-Geste war. Sie fühlte sich bisher, als könnte sie sich bei Salın
auf Arten und Weisen fallen lassen, wie sie das von anderen Beziehungen bisher
nicht kannte. Das Gefühl könnte vergehen, wenn Salın doch anfinge, zu viel zu wollen. Aber Jurin mochte
das Risiko eingehen, mit Salın zu spielen, zu schauen, ob irgendwann eintrat, dass
sie sich ausgekostet hätten, und es dann vielleiht eine Freundschaft würde, oder
ob es irgendwann clashen würde.

Die Tür zu ihrem Doppelzimmer öffnete sich und Linoschka betrat den Raum. Sie zündete eine
Kerze an, ganz im Sinne des Retro-Themas. "Bist du noch wach?"

"Bin ich", antwortete Jurin.

"Es tut mir leid", sagte Linoschka. "Wegen des Blumenstrauß'. Ich wollte eigentlich einfach
einen Witz damit veranstalten. Weil das Thema Retro war, und früher oft Blumensträuße
die romantische Geste waren, und ich dachte, ich mache halt einen Minzstrauß draus, um
Regeln zu brechen." Linoschka setzte sich auf den Stuhl neben Jurins Bett. "Ich war
verletzt, weil du mir nicht einmal Gelegenheit gegeben hast, darüber zu reden. Und weil du etwas
gemacht hast, wovon ich mir recht sicher war, dass dir bewusst war, dass es Verletzungspotenzial
hat. Aber ich verstehe auch, dass ich dich verletzt habe. Ich dachte, mit Minze kann
ich nicht so viel falsch machen, aber ich weiß doch eigentlich auch, dass du
in dem Punkte verletzlich bist und hätte fragen sollen. Und das tut mir leid."

"Mir auch", sagte Jurin. "Ich mache das morgen so ausführlich wie du. Ich bin müde."

"Hat dich verletzt, dass ich mit Mauk gespielt habe?", fragte Linoschka.

"Nein, kein Stück." Jurin kicherte amüsiert. "Es war überraschend. Aber
ich fand es vorwiegend witzig. Hattet ihr Spaß?"

"Überwiegend", sagte Linoschka. "Ich würde mir erst die Zähne putzen wollen. Wenn du dann noch
wach bist, erzähle ich dir davon, wenn ich im Bett liege."

"Willst du mit in meinem liegen?", ludt Jurin ein.

"Oh, ich glaube, das fände ich schön!"

Es war schon seit Jahren so ein Spiel mit Gefühlen zwischen ihnen. Wenn Linoschka zu viel
Nähe zu Jurin hatte, kamen Verliebtheitsgefühle bei ihr auf und sie brauchte
erstmal wieder Abstand zum Abkühlen. Und Jurin war dieses Gefühlsgedusel in Hinblick
auf sich nicht so sehr angenehm. Aber sie hatten sich geeinigt, es trotzdem so zu halten,
weil es für sie beide von Zeit zu Zeit doch schön war und das Schöne überwog.
